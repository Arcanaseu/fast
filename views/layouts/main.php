<?php
use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php


            $items = [
                ['label' => 'Home', 'url' => ['/']],
            ];
            if(!Yii::$app->user->isGuest){
                $items[] = ['label' => 'Settings', 'url' => ['/admin/']];
                if(Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'developer')){
                    $items[] = ['label' => 'Admin', 'url' => ['/admin/admin-panel']];
                }
                if(Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'developer')){
                    $items[] = ['label' => 'API', 'url' => ['/site/about']];
                    $items[] = ['label' => 'Models', 'url' => ['/site/models']];
                }
                /** @var User $user */
                $user = User::findOne(Yii::$app->user->id);
                $ns =    ($user instanceof User)?$user->name.' '.$user->surname:'unknown';

                $name = (Yii::$app->user->identity->username)? Yii::$app->user->identity->username:$ns;
                $items[] = ['label' => 'Logout (' . $name . ')',
                    'url' => ['/auth/default/logout'],
                    'linkOptions' => ['data-method' => 'post']];
            } else {
                $items[] = ['label' => 'Login', 'url' => ['/auth/default/login']];
                $items[] = ['label' => 'Registration', 'url' => ['/auth/default/registration']];
            }
            NavBar::begin([
                'brandLabel' => 'Fast Beer',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $items,
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink'=>false
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Fast <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
