<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1>API</h1>
    <h3>urls</h3>
    <div style="margin-left: 50px;">
        <br>
        <h4>Collection</h4>
        <div style="margin-left: 20px;">
            GET &nbsp;&nbsp;   -    &nbsp;&nbsp; /api/place/?free=1&fields=title.description,text&expand=<br>
        </div>
        <h4>Model</h4>
        <div style="margin-left: 20px;">
             (id = 1 ) <br>
            &nbsp;&nbsp;&nbsp;  <b>view</b>    GET            -  &nbsp;&nbsp;   /api/place/view/?$id=1&free=1<br>
            &nbsp;&nbsp;&nbsp;  <b>create</b>  POST           -  &nbsp;&nbsp;   /api/place/create/&nbsp;&nbsp;&nbsp;free=1<br>
            &nbsp;&nbsp;&nbsp;  <b>update</b>  PUT,PATCH      -  &nbsp;&nbsp;   /api/place/update/&nbsp;&nbsp;&nbsp;free=1, id=1<br>
            &nbsp;&nbsp;&nbsp;  <b>delete</b>  DELETE         -  &nbsp;&nbsp;   /api/place/delete/&nbsp;&nbsp;&nbsp;free=1, id=1<br>

        </div>

    </div>


    <h3>params</h3>
    <br>
    <div style="margin-left: 50px;">
        <strong>free</strong> - give access to api without secret key. Any positive number(usually 1)(free=1)
        <br><br>
        <strong>access</strong> - secret key, defined in initial parameters of site(access=secret)
        <br><br>
        <strong>filters</strong> - filter models (filters={"id":1,"title":"mike","xen":9}) default mode 'like'
        <br><br>
        <strong>strict</strong> - if exist, change filter mode from 'like' to 'equal'. Any positive number(usually 1)(strict=1)
        <br><br>
        <strong>page</strong>  - current page
        <br><br>
        <strong>limit</strong> - limit of models per page (default 20)
        <br><br>
        <strong>sort</strong> - property for sorting
        <br><br>
        <strong>order</strong> - sort dimension (if order = 'desc' dimension 'desc' in other case - 'asc')
        <br><br>
        <strong>fields</strong> - selected fields (fields=id,created)
        <br><br>
        <strong>expand</strong> - related models  (expand=creatorCompany,avatar0)
        <br><br>
    </div>


</div>
