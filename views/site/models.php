<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-models">
    <h1>Models relations</h1>
    <h4>list of models</h4>
    <br>
    <div>
        <strong>AcceptedCompany</strong> events_id => Events, company_id => Company    (company accepted event)
        <br>
        {"id":"1","events_id":"1","company_id":"1"}
        <br><br>
        <strong>AcceptedUser</strong> events_id => Events, user_id => User (user accepted event)
        <br>
        {"id":"1","events_id":"1","user_id":"1"}
        <br><br>
        <strong>Additional</strong> image_id => Image
        <br>
        {"id":"1","title":"test 1","description":"test desc 1","image_id":"1"}
        <br><br>
        <strong>AdditionalCompany</strong> additional_id => Additional, company_id => Company
        <br>
        {"id":"1","additional_id":"1","company_id":"2"}
        <br><br>
        <strong>AdditionalProduct</strong> additional_id => Additional, product_id => Product
        <br>
        {"id":"1","additional_id":"1","product_id":"2"}
        <br><br>
        <strong>City</strong>
        <br><br>
        expand
        <br>
        <pre>
        ****************************************************
        *    models       ***********   fields          ****
        ****************************************************
        *    Company[]            companies
        ****************************************************
        </pre>
        <br><br>
        {"id":"1","city":"Moscow","lat":null,"lon":null}
        <br><br>
        <strong>Company</strong> avatar => Image, city_id => City
        <br><br>
        expand
        <br>
        <pre>
        ****************************************************
        *    models       ***********   fields          ****
        ****************************************************
        *  AcceptedCompany[]      acceptedCompanies
        *  AdditionalCompany[]    additionalCompanies
        *  City                   city
        *  Image                  avatar0
        *  CompanyKitchen[]       companyKitchens
        *  CompanyImage[]         companyImages
        *  CompanyMetro[]         companyMetros
        *  CompanyPhone[]         companyPhones
        *  CompanyVideo[]         companyVideos
        *  Events[]               events
        *  GroupCompany[]         groupCompanies
        *  Hidden[]               hiddens
        *  HiddenCompany[]        hiddenCompanies
        *  HiddenCompany[]        hiddenCompanies0
        *  HiddenGroup[]          hiddenGroups
        *  HiddenUser[]           hiddenUsers
        *  Like[]                 likes
        *  LikeCompany[]          likeCompanies  - company_id
        *  LikeCompany[]          likeCompanies0 - target_id
        *  LikeProduct[]          likeProducts
        *  MentionCompany[]       mentionCompanies
        *  Menu[]                 menus
        *  RatingCompany          ratingCompany
        *  Repost[]               reposts
        *  SubscribersCompany[]   subscribersCompanies
        *  UserCompany[]          userCompanies
        ****************************************************
            </pre>
        <br><br>
        {"id":"1","title":"F1","description":"description","text":"text","avatar":"1",<br>
        "address":"address","city_id":"1","average_score":"3","email":"ms@ww.pp",<br>
        "site":"f1.ru","lat":null,"lon":null}
        <br><br>
        <strong>CompanyKitchen</strong> company_id => Company, kitchen_id => Kitchen
        <br>
        {"id":"1","company_id":"1","kitchen_id":"1"}
        <br><br>
        <strong>CompanyImage</strong> company_id => Company, image_id => Image
        <br>
        {"id":"1","company_id":"1","image_id":"2"}
        <br><br>
        <strong>CompanyMetro</strong> company_id => Company, metro_id => Metro
        <br>
        {"id":"1","company_id":"1","metro_id":"1"}
        <br><br>
        <strong>CompanyPhone</strong> company_id => Company, phone_id => Phone
        <br>
        {"id":"1","company_id":"1","phone_id":"1"}
        <br><br>
        <strong>CompanyVideo</strong>  company_id => Company, video_id => Video
        <br>
        {"id":"1","company_id":"1","video_id":"2"},{"id":"2","company_id":"2","video_id":"1"}
        <br><br>
        <strong>Country</strong> currency_id => Currency
        <br>
        {"id":"1","title":"Lithuania","description":"country of eternal spring",<br>
        "phone_code":"370","flag":"1","currency_id":"1"}
        <br><br>
        <strong>Kitchen</strong>
        <br>
        {"id":"1","title":"china","description":null}
        <br><br>
        <strong>Currency</strong>
        <br>
        {"id":"1","title":"Euro"}
        <br><br>
        <strong>Events</strong>  avatar => Image
        <br><br>
        expand
        <br>
        <pre>
        ****************************************************
        *    models       ***********   fields          ****
        ****************************************************
        *  AcceptedCompany[]      acceptedCompanies
        *  AcceptedUser[]         acceptedUsers
        *  Image                  avatar0
        *  Company                creatorCompany
        *  User                   creatorUser
        *  EventsImage[]          eventsImages
        *  EventsVideo[]          eventsVideos
        *  GroupEvents[]          groupEvents
        *  Hidden[]               hiddens
        *  Like[]                 likes
        *  MentionEvents[]        mentionEvents
        *  RatingEvents           ratingEvents
        *  Repost[]               reposts
        ****************************************************
        </pre>
        <br><br>
        {"id":"1","type":"news","avatar":"3","title":"test event 1","description":"test event 1 desc",<br>
        "text":"test event 1 text","created":"2015-07-13 06:21:36","is_closed":0,"is_expired":0,<br>
        "date":"2015-07-31 00:00:00","creator_user_id":"1","creator_company_id":null,<br>
        "creator_type":"user","lat":0,"lon":0}
        <br><br>
        <strong>EventsImage</strong>  events_id => Events, image_id => Image
        <br>
        {"id":"1","events_id":"1","image_id":"3"}
        <br><br>
        <strong>EventsVideo</strong>  events_id => Events, video_id => Video
        <br>
        {"id":"1","events_id":"1","video_id":"1"}
        <br><br>
        <strong>Friends</strong> initiator_id => User, acceptor_id => User
        <br>
        {"id":"1","initiator_id":"1","acceptor_id":"2"}
        <br><br>
        <strong>Group</strong>
        <br><br>
        expand
        <br>
        <pre>
        ****************************************************
        *    models       ***********   fields          ****
        ****************************************************
        *  GroupAdministrator[]   groupAdministrators
        *  GroupCompany[]         groupCompanies
        *  GroupEvents[]          groupEvents
        *  GroupImage[]           groupImages
        *  GroupUser[]            groupUsers
        *  GroupVideo[]           groupVideos
        *  HiddenGroup[]          hiddenGroups
        ****************************************************
        </pre>
        <br><br>
        {"id":"1","title":"test group 1","description":"test group 1 desc","text":"test group 1 text","is_open":1,"created":"2015-07-13 06:19:32"}
        <br><br>
        <strong>GroupAdministrator</strong>   group_id => Group,  user_id => User
        <br>
        {"id":"1","group_id":"1","user_id":"1"}
        <br><br>
        <strong>GroupCompany</strong>   group_id => Group, company_id => Company
        <br>
        {"id":"1","group_id":"1","company_id":"1"}
        <br><br>
        <strong>GroupEvents</strong> group_id => Group, events_id => Events
        <br>
        {"id":"1","group_id":"1","events_id":"2"}
        <br><br>
        <strong>GroupImage</strong> group_id => Group,  image_id => Image
        <br>
        {"id":"1","group_id":"1","image_id":"1"}
        <br><br>
        <strong>GroupUser</strong> group_id => Group,  user_id => User
        <br>
        {"id":"1","group_id":"1","user_id":"1"}
        <br><br>
        <strong>GroupVideo</strong> group_id => Group, video_id => Video
        <br>
        {"id":"2","group_id":"1","video_id":"1"}
        <br><br>
        <strong>Hidden</strong> company_id => Company,  user_id => User,  events_id => Events (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"1","company_id":null,"created":"2015-07-13 06:38:55","events_id":"1"}
        <br><br>
        <strong>HiddenCompany</strong> company_id => Company,  user_id => User,  hidden_company_id => Company (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"1","company_id":null,"created":"2015-07-13 06:39:28","hidden_company_id":"1"}
        <br><br>
        <strong>HiddenGroup</strong> company_id => Company,  user_id => User,  hidden_group_id => Group (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"1","company_id":null,"created":"2015-07-13 06:40:35","hidden_group_id":"2"}
        <br><br>
        <strong>HiddenUser</strong> company_id => Company,  user_id => User,  hidden_user_id => User (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"1","company_id":null,"created":"2015-07-13 06:41:08","hidden_user_id":"2"}
        <br><br>
        <strong>Image</strong>
        <br>
        {"id":"3","path":"uploads/1436757224.png","created":"2015-07-13 05:13:44","title":"third image","description":"third test image"}
        <br><br>
        <strong>Like</strong> company_id => Company,  user_id => User,  events_id => Events (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"1","company_id":null,"created":"2015-07-13 06:42:07","events_id":"1"}
        <br><br>
        <strong>LikeCompany</strong> company_id => Company,  user_id => User,  target_id => Company (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"3","company_id":null,"created":"2015-07-14 19:57:14","target_id":"2"}
        <br><br>
        <strong>LikeProduct</strong> company_id => Company,  user_id => User,  target_id => Product (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"3","company_id":null,"created":"2015-07-14 19:57:48","target_id":"1"}
        <br><br>
        <strong>MentionCompany</strong>  creator_id => User, target_id => Company
        <br>
        {"id":"1","creator_id":"1","target_id":"2","created":"2015-07-13 06:43:18","rate":2,"text":"mention test text 1 "}
        <br><br>
        <strong>MentionEvents</strong>  creator_id => User, target_id => Events
        <br>
        {"id":"1","creator_id":"1","target_id":"2","created":"2015-07-14 19:58:38","rate":2,"text":"test 1"}
        <br><br>
        <strong>MentionProduct</strong>  creator_id => User, target_id => Product
        <br>
        {"id":"1","creator_id":"2","target_id":"2","created":"2015-07-14 19:59:17","rate":5,"text":"test 1"}
        <br><br>
        <strong>MentionUser</strong> creator_id =>User, target_id =>User
        <br>
        {"id":"1","creator_id":"2","target_id":"1","created":"2015-07-13 06:44:18","rate":7,"text":"mention user test text 1 "}
        <br><br>
        <strong>Menu</strong> company_id => Company
        <br><br>
        expand
        <br>
        <pre>
        ****************************************************
        *    models       ***********   fields          ****
        ****************************************************
        *  Company                company
        *  MenuCategory[]         menuCategories
        *  MenuImage[]            menuImages
        *  MenuVideo[]            menuVideos
        ****************************************************
        </pre>
        <br><br>
        {"id":"1","company_id":"1","title":"menu test 1","description":"menu test 1 desc"}
        <br><br>
        <strong>MenuCategory</strong> menu_id => Menu
        <br>
        {"id":"1","title":"menu categoty 1","description":"menu categoty 1 desc","menu_id":"1"}
        <br><br>
        <strong>MenuCategoryItem</strong> product_id => Product, category_id => MenuCategory
        <br>
        {"id":"1","product_id":"1","category_id":"2","price":"5","discount":"2","discount_type":"percent"}
        <br><br>
        <strong>MenuImage</strong> menu_id => Menu,  image_id => Image
        <br>
        {"id":"1","menu_id":"1","image_id":"2"}
        <br><br>
        <strong>MenuVideo</strong> menu_id => Menu, video_id => Video
        <br>
        {"id":"1","menu_id":"1","video_id":"2"}
        <br><br>
        <strong>Message</strong> creator_id =>User, target_id =>User
        <br>
        {"id":"1","creator_id":"1","target_id":"2","created":"2015-07-13 06:51:44","text":"test message 1 text"}
        <br><br>
        <strong>MessageModerator</strong> creator_id =>User
        <br>
        {"id":"1","creator_id":"1","created":"2015-07-17 18:54:51","text":"tst 1"}
        <br><br>
        <strong>Meta</strong>
        <br>
        {"id":"1","title":"title","key":"key","description":"description"}
        <br><br>
        <strong>Metro</strong>
        <br>
        {"id":"1","metro":"VDNH","lat":null,"lon":null}
        <br><br>
        <strong>Phone</strong>
        <br>
        {"id":"1","country_id":"3","number":"9199804190"}
        <br><br>
        <strong>Product</strong>  avatar => Image, category_id => ProductCategory, type_id => ProductType, <br>
        brand_id => ProductBrand, sort_id => ProductSort, packing_id => ProductPacking, country_id => Country
        <br><br>
        expand
        <br>
        <pre>
        ****************************************************
        *    models       ***********   fields          ****
        ****************************************************
        *  AdditionalProduct[]    additionalProducts
        *  LikeProduct[]          likeProducts
        *  MentionProduct[]       mentionProducts
        *  MenuCategoryItem[]     menuCategoryItems
        *  Country                country
        *  Image                  avatar0
        *  ProductBrand           brand
        *  ProductCategory        category
        *  ProductPacking         packing
        *  ProductSort            sort
        *  ProductType            type
        *  ProductImage[]         productImages
        *  ProductVideo[]         productVideos
        *  RatingProduct          ratingProduct
        *  SubscribersProduct[]   subscribersProducts
        ***************************************************
        </pre>
        <br><br>
        {"id":"1","title":"product test 1","description":"product test 1 desc","text":"product test 1 text","avatar":"1",<br>
        "category_id":"1","type_id":"1","brand_id":"1","sort_id":"1","packing_id":"1","country_id":"1"}
        <br><br>
        <strong>ProductBrand</strong>
        <br>
        {"id":"1","title":"test 1","description":"test 1 desc"}
        <br><br>
        <strong>ProductCategory</strong>
        <br>
        {"id":"1","title":"test 1","description":"test 1 desc"}
        <br><br>
        <strong>ProductKitchen</strong> product_id => Product, kitchen_id => Kitchen
        <br>
        {"id":"1","product_id":"1","kitchen_id":"1"}
        <br><br>
        <strong>ProductImage</strong> product_id => Product, image_id => Image
        <br>
        {"id":"1","product_id":"1","image_id":"3"}
        <br><br>
        <strong>ProductPacking</strong>
        <br>
        {"id":"1","title":"test 1","description":"test 1 desc"}
        <br><br>
        <strong>ProductSort</strong>  brand_id  => ProductBrand
        <br>
        {"id":"1","title":"test 1","description":"test 1 desc","brand_id":null}
        <br><br>
        <strong>ProductType</strong>
        <br>
        {"id":"1","title":"test 1","description":"test 1 desc"}
        <br><br>
        <strong>ProductVideo</strong> product_id => Product, video_id => Video
        <br>
        {"id":"1","product_id":"1","video_id":"1"}
        <br><br>
        <strong>RatingCompany</strong> company_id => Company
        <br>
        {"id":"1","company_id":"1","score":"8","voted":"2"}
        <br><br>
        <strong>RatingEvents</strong>  events_id => Events
        <br>
        {"id":"1","events_id":"2","score":"2","voted":"1"}
        <br><br>
        <strong>RatingMentionCompany</strong> creator_id => User, target_id => MentionCompany
        <br>
        {"id":"1","creator_id":"3","target_id":"2","rate":3}
        <br><br>
        <strong>RatingMentionEvents</strong> creator_id => User, target_id => MentionEvents
        <br>
        {"id":"1","creator_id":"3","target_id":"2","rate":3}
        <br><br>
        <strong>RatingMentionProduct</strong> creator_id => User, target_id => MentionProduct
        <br>
        {"id":"1","creator_id":"3","target_id":"2","rate":3}
        <br><br>
        <strong>RatingMentionUser</strong> creator_id => User, target_id => MentionUser
        <br>
        {"id":"1","creator_id":"3","target_id":"2","rate":3}
        <br><br>
        <strong>RatingProduct</strong> product_id => Product
        <br>
        {"id":"1","product_id":"1","score":"5","voted":"2"}
        <br><br>
        <strong>RatingUser</strong> user_id => User
        <br>
        {"id":"1","user_id":"1","score":"2","voted":"2"}
        <br><br>
        <strong>Repost</strong> company_id => Company,  user_id => User,  events_id => Events (company_id/user_id - creator)
        <br>
        {"id":"1","creator_type":"user","user_id":"1","company_id":null,"created":"2015-07-13 06:53:50","events_id":"1"}
        <br><br>
        <strong>Request</strong> initiator_id => User, acceptor_id => User
        <br>
        {"id":"1","initiator_id":"1","acceptor_id":"2","created":"2015-07-13 06:54:36","type":"request 1","is_realized":0}
        <br><br>
        <strong>Subscribers</strong> subscriber_id => User, target_id => User
        <br>
        {"id":"1","subscriber_id":"1","target_id":"2"}
        <br><br>
        <strong>SubscribersCompany</strong> subscriber_id => User, target_id => Company
        <br>
        {"id":"1","subscriber_id":"1","target_id":"2"}
        <br><br>
        <strong>SubscribersProduct</strong> subscriber_id => User, target_id => Product
        <br>
        {"id":"1","subscriber_id":"1","target_id":"2"}
        <br><br>
        <strong>User</strong>  avatar => Image
        <br><br>
        expand
        <br>
        <pre>
        ****************************************************
        *    models       ***********   fields          ****
        ****************************************************
        *  AcceptedUser[]         acceptedUsers
        *  Events[]               events
        *  Friends[]              friends
        *  Friends[]              friends0
        *  GroupAdministrator[]   groupAdministrators
        *  GroupUser[]            groupUsers
        *  Hidden[]               hiddens
        *  HiddenCompany[]        hiddenCompanies
        *  HiddenGroup[]          hiddenGroups
        *  HiddenUser[]           hiddenUsers
        *  HiddenUser[]           hiddenUsers0
        *  Like[]                 likes
        *  LikeCompany[]          likeCompanies
        *  LikeProduct[]          likeProducts
        *  MentionCompany[]       mentionCompanies
        *  MentionEvents[]        mentionEvents
        *  MentionProduct[]       mentionProducts
        *  MentionUser[]          mentionUsers
        *  MentionUser[]          mentionUsers0
        *  Message[]              messages
        *  Message[]              messages0
        *  Message[]              messages1
        *  MessageModerator[]     messageModerators
        *  RatingMentionCompany[] ratingMentionCompanies
        *  RatingMentionEvents[]  ratingMentionEvents
        *  RatingMentionProduct[] ratingMentionProducts
        *  RatingMentionUser[]    ratingMentionUsers
        *  RatingUser             ratingUser
        *  Repost[]               reposts
        *  Request[]              requests
        *  Request[]              requests0
        *  Subscribers[]          subscribers
        *  Subscribers[]          subscribers0
        *  SubscribersCompany[]   subscribersCompanies
        *  SubscribersProduct[]   subscribersProducts
        *  City                   city0
        *  Phone                  phone0
        *  Image                  avatar0
        *  UserCompany[]          userCompanies
        *  UserImage[]            userImages
        *  UserPhone[]            userPhones
        *  UserVideo[]            userVideos
        ***************************************************
        </pre>
        <br><br>
        {"id":null,"name":"user 1","surname":"user 1 surname","city":"manila","address":"kokos str 123","avatar":"1","phone":"12345","email":"rr@tt.ll","lat":null,"lon":null}
        <br><br>
        <strong>UserImage</strong> user_id => User, image_id => Image
        <br>
        {"id":"1","user_id":"1","image_id":"3"}
        <br><br>
        <strong>UserCompany</strong> user_id => User, company_id => Company
        <br>
        {"id":"4","user_id":"2","company_id":"1"}
        <br><br>
        <strong>UserPhone</strong> user_id => User, phone_id => Phone
        <br>
        {"id":"1","user_id":"1","phone_id":"1"}
        <br><br>
        <strong>UserVideo</strong> user_id => User, video_id => Video
        <br>
        {"id":"2","user_id":"1","video_id":"1"}
        <br><br>
        <strong>Video</strong>
        <br>
        {"id":"1","path":"video path 1","created":"2015-07-13 06:27:57","title":"video 1 title","description":"video 1 desc"}
        <br><br>
    </div>
</div>