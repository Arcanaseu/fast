<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 07.08.2015
 * Time: 17:42
 */

namespace app\commands;




use app\rbac\CompanyAdminRule;
use app\rbac\EventsAdminRule;
use app\rbac\GroupAdminRule;
use app\rbac\MenuAdminRule;
use Yii;
use yii\console\Controller;
use yii\rbac\DbManager;


class RbacController extends Controller
{
    public function actionInit()
    {
        /** @var DbManager $auth */
        $auth = Yii::$app->authManager;

        /*
        // add "default" permission
        $default = $auth->createRole('default');
        $default->description = 'Create default user';
        $auth->add($default);

        // add "admin" permission
        $admin = $auth->createRole('admin');
        $admin->description = 'Create company admin';
        $auth->add($admin);
        $auth->addChild($admin, $default);

        // add "superAdmin" permission
        $superAdmin = $auth->createRole('superAdmin');
        $superAdmin->description = 'Create SuperAdmin';
        $auth->add($superAdmin);
        $auth->addChild($superAdmin, $admin);


        $superAdmin = $auth->createRole('companyAdmin');
        $rule = new CompanyAdminRule();
        $superAdmin->description = 'Create Company Admin';
        $superAdmin->data = serialize($rule);
        $superAdmin->ruleName = $rule->name;
        $auth->add($rule);
        $auth->add($superAdmin);
        $auth->assign($superAdmin, 2);




        $superAdmin = $auth->createRole('eventsAdmin');
        $rule = new EventsAdminRule();
        $superAdmin->description = 'Create Events Admin';
        $superAdmin->data = serialize($rule);
        $superAdmin->ruleName = $rule->name;
        $auth->add($rule);
        $auth->add($superAdmin);
        $auth->assign($superAdmin, 2);

        $superAdmin = $auth->createRole('groupAdmin');
        $rule = new GroupAdminRule();
        $superAdmin->description = 'Create Group Admin';
        $superAdmin->data = serialize($rule);
        $superAdmin->ruleName = $rule->name;
        $auth->add($rule);
        $auth->add($superAdmin);
        $auth->assign($superAdmin, 2);


        $superAdmin = $auth->createRole('menuAdmin');
        $rule = new MenuAdminRule();
        $superAdmin->description = 'Create Menu Admin';
        $superAdmin->data = serialize($rule);
        $superAdmin->ruleName = $rule->name;
        $auth->add($rule);
        $auth->add($superAdmin);
        $auth->assign($superAdmin, 2);
        */

        // add "developer" permission
        $superAdmin = $auth->createRole('developer');
        $superAdmin->description = 'Create Developer';
        $auth->add($superAdmin);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        //$auth->assign($admin, 2);
        //$auth->assign($superAdmin, 2);
    }
}