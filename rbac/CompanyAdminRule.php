<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 10.08.2015
 * Time: 12:27
 */

namespace app\rbac;


use app\models\AcceptedCompany;
use app\models\AdditionalCompany;
use app\models\Company;
use app\models\CompanyImage;
use app\models\CompanyKitchen;
use app\models\CompanyMetro;
use app\models\CompanyPhone;
use app\models\CompanyVideo;
use app\models\LikeCompany;
use app\models\MentionCompany;
use app\models\RatingCompany;
use app\models\SubscribersCompany;
use app\models\User;
use yii\db\ActiveRecord;
use yii\rbac\Item;
use yii\rbac\Rule;

class CompanyAdminRule extends  Rule
{
    public $name = 'companyAdmin';

    /**
     * Executes the rule.
     *
     * @param string|integer $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[ManagerInterface::checkAccess()]].
     * @return boolean a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if(\Yii::$app->authManager->checkAccess($user, 'superAdmin'))
            return true;
        if(!isset($params['modelId']))
            return false;
        $class = \Yii::$app->controller->className();
        $class = str_replace('Controller', '', $class);
        $ex = class_exists($class);

        $class = ($ex)?(new $class()):null;
        $classFromParams = (isset($params['class']))?'app\models\\'.$params['class']:null;
        $exfp = class_exists($classFromParams);
        $classFromParams = ($exfp)?new $classFromParams():null;
        if(!($exfp || $ex))
            return false;
        $model = null;
        if($this->checkClass($classFromParams)){
            $model = $classFromParams;

        } elseif ($this->checkClass($class)){
            $model = $class;
        }

        if($model instanceof ActiveRecord){
            /** @var User $user */
             $user = User::findOne((int) $user);
            if(!$user)
                return false;
            $model = $model::findOne((int) $params['modelId']);
            if(!$model)
                return false;
            $company = $user->getCompany();
            if($company instanceof Company){
                if($model instanceof Company){
                    return $company->id == $model->id;

                } elseif(
                    $model instanceof MentionCompany ||
                    $model instanceof SubscribersCompany
                ){
                    return $model->target->id == $company->id;
                    //target
                } else {
                    return $model->company->id - $company->id;
                    //company
                }
            }




        }

        return false;
    }

    private function checkClass($model)
    {

        if(
            $model instanceof AcceptedCompany       ||
            $model instanceof AdditionalCompany     ||
            $model instanceof Company               ||
            $model instanceof CompanyImage          ||
            $model instanceof CompanyKitchen        ||
            $model instanceof CompanyMetro          ||
            $model instanceof CompanyPhone          ||
            $model instanceof CompanyVideo          ||
            $model instanceof LikeCompany           ||
            $model instanceof MentionCompany        ||
            $model instanceof RatingCompany         ||
            $model instanceof SubscribersCompany
        )
            return true;
        else
            return false;
    }
}