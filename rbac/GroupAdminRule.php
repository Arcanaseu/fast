<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 10.08.2015
 * Time: 12:27
 */

namespace app\rbac;



use app\models\Group;
use app\models\GroupCompany;
use app\models\GroupEvents;
use app\models\GroupImage;
use app\models\GroupUser;
use app\models\GroupVideo;
use app\models\User;
use ReflectionClass;
use yii\db\ActiveRecord;
use yii\rbac\Item;
use yii\rbac\Rule;

class GroupAdminRule extends  Rule
{
    public $name = 'groupAdmin';

    /**
     * Executes the rule.
     *
     * @param string|integer $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[ManagerInterface::checkAccess()]].
     * @return boolean a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if(\Yii::$app->authManager->checkAccess($user, 'superAdmin'))
            return true;
        if(!isset($params['modelId']))
            return false;
        $class = \Yii::$app->controller->className();
        $class = (new ReflectionClass($class))->getShortName();
        $class = str_replace('Controller', '', $class);
        $class = 'app\models\\'.$class;
        $ex = class_exists($class);
        $classFromParams = (isset($params['class']))?'app\models\\'.$params['class']:null;
        $exfp = class_exists($classFromParams);
        if($ex && (new $class()) instanceof ActiveRecord)
            $model = new $class();
        elseif($exfp && (new $classFromParams()) instanceof ActiveRecord)
            $model = new $classFromParams();
        else
            return false;
        /** @var User $user */
        $user = User::findOne((int) $user);
        if(!$user)
            return false;

        /** @var ActiveRecord $model */
        $model = $model::findOne((int) $params['modelId']);
        if(
            $model instanceof GroupCompany ||
            $model instanceof GroupEvents  ||
            $model instanceof GroupImage   ||
            $model instanceof GroupVideo   ||
            $model instanceof GroupUser
        ){
            $model = $model->group;
        }
        if($model instanceof Group){
            /** @var Group $model */
            foreach($model->groupAdministrators as $admin){
                if($admin->user->id = $user->id)
                    return true;

            }
        }
    }


}