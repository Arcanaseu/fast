<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'gii' => [
            'class' =>  'yii\gii\Module',
            'allowedIPs' => ['*'],
        ],
        'auth' => [
            'class' => 'app\modules\auth\Module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
        'ios' => [
            'class' => 'app\modules\ios\Module',
        ],
        /*
        'auth' => [
            'class' => 'auth\Module',
            'layout' => 'homepage', // Layout when not logged in yet
            'layoutLogged' => 'main', // Layout for logged in users
            'attemptsBeforeCaptcha' => 3, // Optional
            'supportEmail' => 'support@mydomain.com', // Email for notifications
            'passwordResetTokenExpire' => 3600, // Seconds for token expiration
            'superAdmins' => ['superAdmin'], // SuperAdmin users
            'signupWithEmailOnly' => false, // false = signup with username + email, true = only email signup
            'tableMap' => [ // Optional, but if defined, all must be declared
                'User' => 'user',
                'UserStatus' => 'user_status',
                'ProfileFieldValue' => 'profile_field_value',
                'ProfileField' => 'profile_field',
                'ProfileFieldType' => 'profile_field_type',
            ],
        ],
        */
        'permit' => [
            'class' => 'developeruz\db_rbac\Yii2DbRbac',
            'params' => [
                'userClass' => 'app\models\User'
            ]
            //'layout' => 'admin'
        ],

    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'V_01-9W-2BnwdJVbtowcgk751RFhJBkK',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'application/json; charset=UTF-8' => 'yii\web\JsonParser',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            //'enableStrictParsing' => true,
            'showScriptName' => false,

            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'api/accepted-company',
                        'api/accepted-user',
                        'api/additional',
                        'api/additional-product',
                        'api/place',
                        'api/company-image',
                        'api/company-video',
                        'api/country',
                        'api/events',
                        'api/events-image',
                        'api/events-video',
                        'api/friends',
                        'api/group',
                        'api/group-administrator',
                        'api/group-company',
                        'api/group-events',
                        'api/group-image',
                        'api/group-user',
                        'api/group-video',
                        'api/hidden',
                        'api/hidden-company',
                        'api/hidden-group',
                        'api/hidden-user',
                        'api/image',
                        'api/like',
                        'api/like-company',
                        'api/like-product',
                        'api/mention-company',
                        'api/mention-events',
                        'api/mention-product',
                        'api/mention-user',
                        'api/menu',
                        'api/menu-category',
                        'api/menu-category-item',
                        'api/menu-image',
                        'api/menu-video',
                        'api/message',
                        'api/meta',
                        'api/product',
                        'api/product-brand',
                        'api/product-category',
                        'api/product-image',
                        'api/product-packing',
                        'api/product-sort',
                        'api/product-type',
                        'api/product-video',
                        'api/rating-company',
                        'api/rating-events',
                        'api/rating-mention-company',
                        'api/rating-mention-events',
                        'api/rating-mention-product',
                        'api/rating-mention-user',
                        'api/rating-product',
                        'api/rating-user',
                        'api/repost',
                        'api/request',
                        'api/subscribers',
                        'api/subscribers-company',
                        'api/subscribers-product',
                        'api/user',
                        'api/user-image',
                        'api/user-video',
                        'api/video',
                    ]
                ],
            ],


        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/default/login']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
