<?php

/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 11.07.2015
 * Time: 13:08
 */



namespace app\modules\api;


use yii\web\Response;

class Helper
{
    public static function params(){
        $params = \Yii::$app->getRequest()->getBodyParams();
        if(!$params)
            $params = \Yii::$app->getRequest()->getQueryParams();
        return $params;
    }
    public static function setFormat(){
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;
    }
}