<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 10.07.2015
 * Time: 18:34
 */

namespace app\modules\api\actions;

use app\modules\api\Helper;
use Yii;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;
class UpdateAction extends \yii\rest\UpdateAction
{
    /**
     * Updates an existing model.
     * @param string $id the primary key of the model.
     * @return \yii\db\ActiveRecordInterface the model being updated
     * @throws ServerErrorHttpException if there is any error when updating the model
     */
    public function run($id)
    {
        /* @var $model ActiveRecord */
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $model->scenario = $this->scenario;
        $model->load(Helper::params(), '');
        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }

}