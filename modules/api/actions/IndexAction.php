<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 10.07.2015
 * Time: 10:56
 */

namespace app\modules\api\actions;


use app\modules\api\controllers\BaseController;
use app\modules\api\Helper;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\web\Response;

class IndexAction extends \yii\rest\IndexAction
{

    /**
     * @return ActiveDataProvider
     */
    public function run()
    {


        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        return $this->prepareDataProvider();

    }

    /**
     * Prepares the data provider that should return the requested collection of the models.
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider()
    {

        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this);
        }




        $params = Helper::params();

        $page = (isset($params['page']))?$params['page']:1;
        $limit = (isset($params['limit']))?$params['limit']:20;
        $offset = $limit*($page-1);
        $sort = (isset($params['sort']))?$params['sort']:'';
        $strict = (isset($params['strict']))?1:0;
        if(isset($params['sort'])){
            if(isset($params['order'])){
                if($params['order'] == "desc")
                    $sort.=" desc";
                else
                    $sort.=" asc";
            }
        }

        $model = new $this->modelClass();

        $attr = ($model instanceof ActiveRecord)?$model->attributes():[];
        $filters = (isset($params['filters']))?json_decode($params['filters'], true):[];
        /*
        $fields = (isset($params['fields']))?($params['fields']):'';
        $fields = explode(',',$fields);
        if(!is_array($fields) || count($fields) == 0)
           $fields = $attr;
        $fields = array_intersect($fields, $attr);
        */
        $filters = array_flip($filters);
        $filters = array_intersect($filters, $attr);
        $filters = array_flip($filters);


        //$fields = implode(',', $fields);




        $query = new ActiveQuery($this->modelClass);
        $query->offset($offset)
            ->limit($limit);

        foreach($filters as $key => $filter){
            if($strict)
                $query->andFilterWhere([ $key => $filter]);
            else
                $query->andFilterWhere(['like', $key, $filter]);
        }

        $query
            ->orderBy($sort);
            //->select($fields);

        $dataProvider = new ActiveDataProvider();
        $dataProvider->query = $query;
        $dataProvider->init();
        $dataProvider->prepare();
        return $dataProvider;
    }


}