<?php

namespace app\modules\api\controllers;

use app\models\EventsImage;

class EventsImageController extends BaseController
{
    public $modelClass = 'app\models\EventsImage';
}
