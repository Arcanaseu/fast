<?php

namespace app\modules\api\controllers;


use app\models\Product;

class ProductController extends BaseController
{
    public $modelClass = 'app\models\Product';
}
