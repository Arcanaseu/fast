<?php

namespace app\modules\api\controllers;

class ProductTypeController extends BaseController
{
    public $modelClass = 'app\models\ProductType';
}
