<?php

namespace app\modules\api\controllers;

class KitchenController extends BaseController
{
    public $modelClass = 'app\models\Kitchen';
}
