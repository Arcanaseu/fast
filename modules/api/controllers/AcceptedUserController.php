<?php

namespace app\modules\api\controllers;

use app\models\AcceptedUser;

class AcceptedUserController extends BaseController
{
    public $modelClass = 'app\models\AcceptedUser';

}
