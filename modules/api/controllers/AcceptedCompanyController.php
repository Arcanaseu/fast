<?php

namespace app\modules\api\controllers;

use app\models\AcceptedCompany;

class AcceptedCompanyController extends BaseController
{
    public $modelClass = 'app\models\AcceptedCompany';

}
