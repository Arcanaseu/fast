<?php

namespace app\modules\api\controllers;

class ProductBrandController extends BaseController
{
    public $modelClass = 'app\models\ProductBrand';
}
