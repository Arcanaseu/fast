<?php

namespace app\modules\api\controllers;

class ProductCategoryController extends BaseController
{
    public $modelClass = 'app\models\ProductCategory';
}
