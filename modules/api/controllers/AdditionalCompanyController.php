<?php

namespace app\modules\api\controllers;

class AdditionalCompanyController extends BaseController
{
    public $modelClass = 'app\models\AdditionalCompany';
}
