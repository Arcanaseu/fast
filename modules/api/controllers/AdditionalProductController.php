<?php

namespace app\modules\api\controllers;

class AdditionalProductController extends BaseController
{
    public $modelClass = 'app\models\AdditionalProduct';
}
