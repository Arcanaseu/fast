<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 11.08.2015
 * Time: 20:20
 */

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */


$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-remind">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>


        <?= $form->field($model, 'username') ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'phone') ?>

        <?= $form->field($model, 'country')->dropDownList(AdminHelper::getCountryList()) ?>

        <?= $form->field($model, 'password') ?>



        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'remind-button']) ?>
            </div>
        </div>





    <?php ActiveForm::end(); ?>
</div>