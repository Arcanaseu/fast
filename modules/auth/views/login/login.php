<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
/** @var string $ok */
/** @var string $vk */
/** @var string $fb */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php        if(Yii::$app->session->hasFlash('ok_error')): ?>
        <div class="alert alert-danger col-lg-12" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign">
                <?=Yii::$app->session->getFlash('ok_error') ?>
            </span>
        </div>
    <?php endif; ?>
    <?php        if(Yii::$app->session->hasFlash('vk_error')): ?>
        <div class="alert alert-danger col-lg-12" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign">
                <?=Yii::$app->session->getFlash('vk_error') ?>
            </span>
        </div>
    <?php endif; ?>
    <?php        if(Yii::$app->session->hasFlash('fb_error')): ?>
        <div class="alert alert-danger col-lg-12" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign">
                <?=Yii::$app->session->getFlash('fb_error') ?>
            </span>
        </div>
    <?php endif; ?>



    <p>Please fill out the following fields to login:</p>


    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe', [
        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
    ])->checkbox() ?>



    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            <span style="margin-left: 50px;"><a href="/admin/default/remind/">remind password</a></span>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <br>
    <br>
    <div>
        <h4>Login with social networks </h4>
        <br>
        <div>
            <a href="<?=$fb; ?>">
                <img src="/image/facebook.png" height="80">
            </a>
            <a href="<?=$ok; ?>">
                <img src="/image/ok.png" height="80">
            </a>
            <a href="<?=$vk; ?>">
                <img src="/image/vk.jpg" height="80">
            </a>
        </div>
    </div>


</div>
