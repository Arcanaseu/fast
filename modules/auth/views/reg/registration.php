<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 11.08.2015
 * Time: 20:20
 */

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
/* @var $check boolean */


$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-remind">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(!$check): ?>
        <br>
        <p>Check code will be sent to your phone</p>
        <p style="color: darkgoldenrod">Please insert phone without country code!!!</p>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([
        'id' => 'reg-form',
        'options' => ['class' => 'form-horizontal','enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?php if($check): ?>
        <p>Enter check code </p>
        <?= $form->field($model, 'code') ?>
        <?php  echo   $form->field($model, 'username')->hiddenInput()->label(false); ?>
        <?php  echo   $form->field($model, 'email')->hiddenInput()->label(false); ?>
        <?php  echo   $form->field($model, 'gender')->hiddenInput()->label(false); ?>
        <?php  echo   $form->field($model, 'phone')->hiddenInput()->label(false); ?>
        <?php  echo   $form->field($model, 'country')->hiddenInput()->label(false); ?>
        <?php  echo   $form->field($model, 'password')->hiddenInput()->label(false); ?>

    <?php else: ?>


        <?= $form->field($model, 'username') ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'gender')->dropDownList(['m' =>'male', 'f' => 'female']) ?>

        <?= $form->field($model, 'phone') ?>

        <?= $form->field($model, 'country')->dropDownList(AdminHelper::getCountryCodeList()) ?>

        <?= $form->field($model, 'password') ?>
        <?= $form->field($model, 'file')->fileInput() ?>
    <?php endif; ?>


        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'remind-button']) ?>
            </div>
        </div>





    <?php ActiveForm::end(); ?>
    <?php if($check): ?>
    <br>
    <div id="resend-block"  class="hidden">
        <?php $form = ActiveForm::begin([
            'id' => 'resend-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

            <input type="hidden" name="resend" value="1">
            <?= Html::submitButton('Resend code', ['class' => 'btn btn-link', 'name' => 'resend-button']) ?>

        <?php ActiveForm::end(); ?>
    </div>
    <?php endif; ?>

</div>