<?php

namespace app\modules\auth\controllers;

use app\models\Image;
use app\models\Phone;
use app\models\User;
use Yii;
use yii\web\Controller;

class VkController extends Controller
{

    public $params;


    public static  $countries = [
        'n/a',
        3,//'������',
        5,//'�������',
        6,//'��������',
        '���������',
        '�����������',
        '�������',
        '������',
        '�������',
        '���',
        '��������',
        '����������',
        '������',
        1,//'�����',
        '�������',
        '�������',
        '�����������',
        '������������',
        '����������'
    ];

    public function actionIndex()
    {

        $client_id = '5032216'; // ID ����������
        $client_secret = 'gBgtLR7EzIqcmw5W0hqq'; // ���������� ����
        $redirect_uri = 'http://ms.arcanas.eu/auth/vk/'; // ����� �����


        if (isset($_GET['code'])) {

            $this->params = array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'code' => $_GET['code'],
                'redirect_uri' => $redirect_uri
            );

            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($this->params))), true);

            if (isset($token['access_token'])) {
                $this->params = array(
                    'uids'         => $token['user_id'],
                    'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big,contacts',
                    'access_token' => $token['access_token']
                );

                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($this->params))), true);

                if (isset($userInfo['response'][0]['uid'])) {
                    $userInfo = $userInfo['response'][0];
                    $this->vkLogin($userInfo);

                }
            }
        }

        $message = 'Something went wrong with VK authorization';
        \Yii::$app->session->setFlash('vk_error', $message);


        return $this->redirect('/auth/login/');
    }

    private function vkLogin($userInfo)
    {
        $name = (isset($userInfo['first_name']))?$userInfo['first_name']:null;
        $surname = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $uid = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $phone = (isset($userInfo['home_phone']))?$userInfo['home_phone']:null;
        $phone = ($phone)?preg_replace('/\D/','',$phone):null;
        if(!$name && $surname)
            $name = $surname;
        elseif(!$name)
            $name = $uid;


        if(!$uid)
            $this->goHome();
        $access = md5(md5($uid).sha1($uid));
        $user = User::findIdentityByAccessToken($access);
        if($user){
            Yii::$app->user->login($user,  0);
            return $this->goHome();
        } else {
            $user = new User();
            $user->access_token = $access;
            $user->name = $name;
            $user->surname = $surname;
            if($user->save(false)){
                if(isset($userInfo['photo_big'])){
                    $url = $userInfo['photo_big'];
                    $this->createAvatar($user, $url);
                }elseif(isset($userInfo['photo_50'])){
                    $url = $userInfo['photo_50'];
                    $this->createAvatar($user, $url);
                }
                if($phone){
                    $country = (isset($userInfo['country']))?self::$countries[$userInfo['country']]:null;
                    $ph = new Phone();
                    if($country && is_numeric($country))
                        $ph->country_id = $country;
                    $ph->number = $phone;
                    $ph->save();
                }
                Yii::$app->user->login($user,  0);
                return $this->goHome();


            }

        }
        return false;
    }

    private function imageFromUrl($url, $path)
    {
        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $resource = curl_exec($ch);
        curl_close ($ch);
        $img = imagecreatefromstring($resource);
        return imagepng($img, $path);

    }

    private function createAvatar(User $user, $url)
    {
        $path = 'uploads/' . time() . '.png';
        if($this->imageFromUrl($url, $path)){
            $image = new Image();
            $image->path = $path;
            $image->title = $user->name;
            $image->description = 'This is image from VK website';
            if($image->save()){
                $user->avatar = $image->id;
                $user->save(false);
            }
        }
    }

}
