<?php

namespace app\modules\auth\controllers;

use app\models\Country;
use app\models\Image;
use app\models\Phone;
use app\models\RegUser;
use app\models\ResendForm;
use app\models\User;
use app\sms\Transport;
use Yii;
use yii\web\Controller;
use yii\web\JqueryAsset;
use yii\web\UploadedFile;

class RegController extends Controller
{

    public $layout = 'main';

    private $countryPhoneLength = [
        7 => 10,
        370 => 8,
        371 => 8,
        38 => 10,
    ];

    /**
     * @return string
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $sid = session_id();
        $resend = new ResendForm();
        $post = Yii::$app->request->post();
        if(isset($post['RegUser']['code'])){
            $model = RegUser::find()->where(['session' => session_id()])->one();
            if($model && $model->checking == trim($post['RegUser']['code'])){
                $this->registration($model);

            }
            $this->getView()->registerJsFile('@web/assets/js/resend.js', ['depends' => [JqueryAsset::className()]]);

            return $this->render('registration', ['model' => $model, 'resend' => $resend, 'check' => true]);
        }
        if(isset($post['resend'])){
            $model = RegUser::find()->where(['session' => session_id()])->one();
            $this->setCheck($model);
            $this->getView()->registerJsFile('@web/assets/js/resend.js', ['depends' => [JqueryAsset::className()]]);

            return $this->render('registration', ['model' => $model, 'resend' => $resend, 'check' => true]);
        }
        $this->cleanTemporaryTables();
        $model = new RegUser();

        if(isset($_FILES['RegUser']) && $_FILES['RegUser']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->phone = $this->checkPhone($model);
                $model->session = session_id();
                $model->expired_at = time() + 20*60;
                if(!$model->username || $model->username == ''){
                    $model->username = $model->phone;
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $model->path = $fileName;
                if($model->validate())
                    $model->file->saveAs($fileName,false);
                $model->validate();
                if ($model->save()) {
                    $this->setCheck($model);
                    $this->getView()->registerJsFile('@web/assets/js/resend.js', ['depends' => [JqueryAsset::className()]]);

                    return $this->render('registration', ['model' => $model, 'resend' => $resend, 'check' => true]);
                } else {
                    return $this->render('registration', ['model' => $model, 'resend' => $resend, 'check' => false]);
                }

            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $model->phone = $this->checkPhone($model);
                $model->expired_at = time() + 20*60;
                $model->session = session_id();
                if(!$model->username || $model->username == ''){
                    $model->username = $model->phone;
                }
                $model->validate();
                if ($model->save()) {
                    $this->setCheck($model);
                    $this->getView()->registerJsFile('@web/assets/js/resend.js', ['depends' => [JqueryAsset::className()]]);

                    return $this->render('registration', ['model' => $model,  'resend' => $resend,'check' => true]);
                } else {
                    return $this->render('registration', ['model' => $model, 'resend' => $resend, 'check' => false]);
                }
            }
        }

        return $this->render('registration', ['model' => $model, 'resend' => $resend, 'check' => false]);
    }


    protected function registration(RegUser $model){



        $user = new User();
        if($model->path){
            $image = new Image();
            $image->setAttribute('path',$model->path);
            $image->setAttribute('title','User Image  - '.$model->username);
            if($image->save())
                $user->setAttribute('avatar',$image->id);
        }

        $user->setAttribute('username',$model->username);
        $user->setAttribute('password', md5($model->password));
        $user->setAttribute('gender', md5($model->gender));

        $phone = new Phone();
        $ph =  (float)$model->phone;
        $phone->setAttribute('number', $ph);
        /** @var Country $country */
        $country = Country::find()->where(['phone_code' => $model->country])->one();
        $phone->setAttribute('country_id',$country->id);
        $phone->validate();
        $phone->save();
        $user->setAttribute('phone', $phone->id);
        $user->setAttribute('email',$model->email);
        $user->validate();
        if($user->save()){
            Yii::$app->user->login($user,  0);

            $num =  $model->getPhone();
            $model->delete();
            $text = 'You are registered at beermap';
            $res = (new Transport())->send(['text' => $text, 'source' => 'beermap'], [$num]);

            $this->redirect('/');
        }
    }

    private function cleanTemporaryTables()
    {
        $t = time();
        $criteria = "'expired_at' < {$t}";
        $users = RegUser::find()->where($criteria)->all();
        /** @var RegUser $user */
        foreach($users as $user){
            if($path = $user->path)
                unlink($path);
            $user->delete();
        }
    }

    private function setCheck($user)
    {

        $code = ($user instanceof RegUser && !$user->checking)?(substr(md5(time()),0,4)):$user->checking;
        $user->checking = $code;
        $user->save();
        $phone = $user->getPhone();
        $res = (new Transport())->send(['text' => $code, 'source' => 'beermap'], [$phone]);
        return true;
    }

    private function checkPhone(RegUser $model)
    {
        $model->phone = preg_replace("/[^0-9]/", "", $model->phone );
        $len = $this->countryPhoneLength[$model->country];
        if(strlen($model->phone) > $len)
            $model->phone = substr($model->phone, -$len);
        elseif(strlen($model->phone) < $len)
            return null;

        return (float) $model->phone;
    }

}
