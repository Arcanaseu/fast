<?php

namespace app\modules\auth\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class DefaultController extends Controller
{

    public $layout = 'main';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->redirect('/auth/login/');
    }
    public function actionRemind()
    {
        return $this->redirect('/auth/remind/');
    }

    public function actionRegistration(){
        return $this->redirect('/auth/reg/');
    }

    public function actionLogin()
    {
        return $this->redirect('/auth/login/');
    }

    public function actionLogout()
    {
        return $this->redirect('/auth/logout/');
    }



}
