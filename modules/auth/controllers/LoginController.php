<?php

namespace app\modules\auth\controllers;

use app\models\LoginForm;
use Facebook\Facebook;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class LoginController extends Controller
{

    private $ok_client_id = '1150074368'; // Application ID
    private $ok_redirect_uri = 'http://ms.arcanas.eu/auth/ok/';
    private $vk_client_id = '5032216';
    private $vk_redirect_uri = 'http://ms.arcanas.eu/auth/vk/';

    public $layout = 'main';

    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest  && !Yii::$app->request->isAjax) {
            return $this->goHome();
        }

        if(session_id() == '')
            session_start();

        $paramsOk = array(
            'client_id' => $this->ok_client_id,
            'response_type' => 'code',
            'redirect_uri' => $this->ok_redirect_uri
        );

        $paramsVk = array(
            'client_id'     => $this->vk_client_id,
            'redirect_uri'  => $this->vk_redirect_uri,
            'response_type' => 'code',
            'v' => 5.37,
            'scope'  =>  'friends,email',
            'display' =>'popup',
        );

        $facebook = new Facebook([
            'app_id' => '156629614671462',
            'app_secret' => '48d6b092bdd909e9228dc2f1f0fb0d01',
            'default_graph_version' => 'v2.2',
        ]);


        $helper = $facebook->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $fb = $helper->getLoginUrl('http://ms.arcanas.eu/auth/fb/', $permissions);

        $ok = 'http://www.odnoklassniki.ru/oauth/authorize?'. urldecode(http_build_query($paramsOk));

        $vk = 'http://oauth.vk.com/authorize?' . urldecode(http_build_query($paramsVk));

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();



        } else {


            return $this->render('login', [
                'model' => $model,
                'ok'  => $ok,
                'vk'  => $vk,
                'fb'  => $fb,
            ]);
        }
    }

}
