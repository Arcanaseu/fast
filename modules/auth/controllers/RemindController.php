<?php

namespace app\modules\auth\controllers;

use app\models\RemindForm;
use Yii;
use yii\web\Controller;

class RemindController extends Controller
{

    public $layout = 'main';
    public function actionIndex()
    {
        $model = new RemindForm();
        $message = false;
        if ($model->load(Yii::$app->request->post())){
            $message = $model->getMessage();
        }
        return $this->render('remind', ['model' => $model, 'message' =>$message]);
    }

}
