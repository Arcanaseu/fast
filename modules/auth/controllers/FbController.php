<?php

namespace app\modules\auth\controllers;

use app\models\Image;
use app\models\User;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\GraphNodes\GraphPicture;
use Yii;
use yii\web\Controller;

class FbController extends Controller
{
    public function actionIndex()
    {
        $appId = '156629614671462';
        if(session_id() == '')
            session_start();
        $fb = new Facebook([
            'app_id' => '156629614671462',
            'app_secret' => '48d6b092bdd909e9228dc2f1f0fb0d01',
            'default_graph_version' => 'v2.2',
        ]);


        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {

                $message = 'Something went wrong with Fb authorization'.
                    "Error: " . $helper->getError() . "\n".
                    "Error Code: " . $helper->getErrorCode() . "\n".
                    "Error Reason: " . $helper->getErrorReason() . "\n".
                    "Error Description: " . $helper->getErrorDescription() . "\n";
                \Yii::$app->session->setFlash('fb_error', $message);

                return $this->redirect('/auth/login/');

            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }


// The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);


// Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($appId);
// If you know the user ID this access token belongs to, you can validate it here
//$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();


        $_SESSION['fb_access_token'] = (string) $accessToken;


        $fb->setDefaultAccessToken($_SESSION['fb_access_token']);


        try {
            $response = $fb->get('/me?fields=email,first_name,last_name,name,picture');
            $userNode = $response->getGraphUser();
            if($this->fbLogin($userNode))
                return $this->goHome();
            else{
                $message = 'Something went wrong with OK authorization';
                \Yii::$app->session->setFlash('vk_error', $message);


                return $this->redirect('/auth/login/');
            }



        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }


    }

    private function fbLogin($userInfo)
    {
        $name = (isset($userInfo['first_name']))?$userInfo['first_name']:null;
        $surname = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $uid = (isset($userInfo['id']))?$userInfo['id']:null;
        $picture = (isset($userInfo['picture']))?$userInfo['picture']:null;

        if(!$name && $surname)
            $name = $surname;
        elseif(!$name)
            $name = $uid;


        if(!$uid)
            $this->goHome();
        $access = md5(md5($uid).sha1($uid));
        $user = User::findIdentityByAccessToken($access);
        if($user){
            Yii::$app->user->login($user,  0);
            return true;
        } else {
            $user = new User();
            $user->access_token = $access;
            $user->name = $name;
            $user->surname = $surname;
            if($user->save(false)){
                if($picture  instanceof GraphPicture){
                    $url = $picture->getUrl();
                    $this->createAvatar($user, $url);
                }

                Yii::$app->user->login($user,  0);
                return true;
            }

        }
        return false;
    }

    private function imageFromUrl($url, $path)
    {
        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $resource = curl_exec($ch);
        curl_close ($ch);
        $img = imagecreatefromstring($resource);
        return imagepng($img, $path);

    }

    private function createAvatar(User $user, $url)
    {
        $path = 'uploads/' . time() . '.png';
        if($this->imageFromUrl($url, $path)){
            $image = new Image();
            $image->path = $path;
            $image->title = $user->name;
            $image->description = 'This is image from VK website';
            if($image->save()){
                $user->avatar = $image->id;
                $user->save(false);
            }
        }
    }

}
