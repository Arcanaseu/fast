<?php

namespace app\modules\auth\controllers;

use app\models\Image;
use app\models\User;
use Yii;
use yii\web\Controller;

class OkController extends Controller
{
    public function actionIndex()
    {
        $client_id = '1150074368'; // Application ID
        $public_key = 'CBAPLNIFEBABABABA'; // ��������� ���� ����������
        $client_secret = '56670772AA5458E8C742107B'; // ��������� ���� ����������
        $redirect_uri = 'http://ms.arcanas.eu/auth/ok/'; // ������ �� ����������

        if (isset($_GET['code'])) {

            $params = array(
                'code' => $_GET['code'],
                'redirect_uri' => $redirect_uri,
                'grant_type' => 'authorization_code',
                'client_id' => $client_id,
                'client_secret' => $client_secret
            );

            $url = 'http://api.odnoklassniki.ru/oauth/token.do';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            curl_close($curl);

            $tokenInfo = json_decode($result, true);

            if (isset($tokenInfo['access_token']) && isset($public_key)) {
                $sign = md5("application_key={$public_key}format=jsonmethod=users.getCurrentUser" . md5("{$tokenInfo['access_token']}{$client_secret}"));

                $params = array(
                    'method' => 'users.getCurrentUser',
                    'access_token' => $tokenInfo['access_token'],
                    'application_key' => $public_key,
                    'format' => 'json',
                    'sig' => $sign
                );

                $userInfo = json_decode(file_get_contents('http://api.odnoklassniki.ru/fb.do' . '?' . urldecode(http_build_query($params))), true);
                if (isset($userInfo['uid'])) {

                    $this->okLogin($userInfo);

                }
            }
        }

        $message = 'Something went wrong with OK authorization';
        \Yii::$app->session->setFlash('ok_error', $message);

         return $this->redirect('/auth/login/');
    }

    private function okLogin($userInfo)
    {

        $name = (isset($userInfo['first_name']))?$userInfo['first_name']:null;
        $surname = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $uid = (isset($userInfo['uid']))?$userInfo['uid']:null;
        if(!$name && $surname)
            $name = $surname;
        elseif(!$name)
            $name = $uid;


        if(!$uid)
            $this->goHome();
        $access = md5(md5($uid).sha1($uid));
        $user = User::findIdentityByAccessToken($access);
        if($user){
            Yii::$app->user->login($user,  0);
            return $this->goHome();
        } else {
            $user = new User();
            $user->access_token = $access;
            $user->name = $name;
            $user->surname = $surname;
            if($user->save(false)){
                if(isset($userInfo['pic_2'])){
                    $url = $userInfo['pic_2'];
                    $this->createAvatar($user, $url);
                }elseif(isset($userInfo['pic_1'])){
                    $url = $userInfo['pic_1'];
                    $this->createAvatar($user, $url);
                }
                Yii::$app->user->login($user,  0);
                return $this->goHome();

            }

        }



    }

    private function imageFromUrl($url, $path)
    {
        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $resource = curl_exec($ch);
        curl_close ($ch);
        $img = imagecreatefromstring($resource);
         return imagepng($img, $path);

    }

    private function createAvatar(User $user, $url)
    {
        $path = 'uploads/' . time() . '.png';
        if($this->imageFromUrl($url, $path)){
            $image = new Image();
            $image->path = $path;
            $image->title = $user->name;
            $image->description = 'This is image from Odnoklassniki website';
            if($image->save()){
                $user->avatar = $image->id;
                $user->save(false);
            }
        }
    }

}
