<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MentionProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mention-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'creator_id')->dropDownList(AdminHelper::getUsersList()) ?>

    <?= $form->field($model, 'target_id')->dropDownList(AdminHelper::getProductList()) ?>

    <?= $form->field($model, 'rate')->textInput() ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
