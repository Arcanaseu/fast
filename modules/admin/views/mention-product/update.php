<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MentionProduct */

$this->title = 'Update Mention Product: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mention Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mention-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
