<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MentionProduct */

$this->title = 'Create Mention Product';
$this->params['breadcrumbs'][] = ['label' => 'Mention Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mention-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
