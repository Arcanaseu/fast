<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AcceptedCompany */

$this->title = 'Create Accepted Company';
$this->params['breadcrumbs'][] = ['label' => 'Accepted Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accepted-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
