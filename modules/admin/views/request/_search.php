<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\RequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'initiator_id')->dropDownList(AdminHelper::getUsersList()) ?>

    <?= $form->field($model, 'acceptor_id')->dropDownList(AdminHelper::getUsersList()) ?>

    <?= $form->field($model, 'created') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'is_realized') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
