<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserImage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-image-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList(AdminHelper::getUsersList()) ?>

    <?= $form->field($model, 'image_id')->dropDownList(AdminHelper::getImagesList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
