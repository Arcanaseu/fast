<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserCompany */
/* @var $form yii\widgets\ActiveForm */
/** @var \app\models\Company $company */
/** @var mixed $companyId */
?>

<div class="user-company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList(AdminHelper::getUsersList()) ?>

    <?php
    if(!($company instanceof \app\models\Company)){
        echo $form->field($model, 'company_id')->dropDownList(AdminHelper::getCompanyList());
    } else {
        echo   $form->field($model, 'company_id',
            ['options' => ['value'=> $company->id] ])->hiddenInput(['value'=> $company->id])->label(false);
    }


    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
