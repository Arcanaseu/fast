<?php

use yii\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserCompany */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var mixed $route */
/** @var \app\models\Company $company */
/** @var mixed $companyId */
/** @var mixed $back */
if(!isset($route))
    $route = 'index';

if(!isset($back))
    $back = '';

$this->title = 'Create User Company';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route, 'back' => $back]];
$this->params['breadcrumbs'][] = $this->title;
if(true){
    $columns = [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        'surname',
        'city',
        //'address',
        // 'avatar',
        // 'phone',
        'email:email',
                // 'lat',
                // 'lon',

                //['class' => 'yii\grid\ActionColumn']
    ];
} else {
    $columns =[
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        'surname',
        'city',
        //'address',
        // 'avatar',
        // 'phone',
        'email:email',
                // 'lat',
                // 'lon',

                ['class' => 'yii\grid\ActionColumn']
        ];
}

?>
<div class="user-company-create">



    <div style="width: 70%;margin-left: 150px;">
        <h2>Search user</h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $columns ,

        ]); ?>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'companyId' => $companyId
    ]) ?>

</div>
