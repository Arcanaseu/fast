<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserCompany */
/** @var mixed $route */
/** @var \app\models\Company $company */
/** @var mixed $companyId */
/** @var mixed $back */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Update User Company: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route, 'back' => $back]];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => [
        'view',
        'id' => $model->id,
        'companyId' => $companyId,
        'route' => $route,
        'back' => $back
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
    ]) ?>

</div>
