<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductVideo */

$this->title = 'Create Product Video';
$this->params['breadcrumbs'][] = ['label' => 'Product Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
