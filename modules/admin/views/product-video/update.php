<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductVideo */

$this->title = 'Update Product Video: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-video-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
