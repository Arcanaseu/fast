<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MentionUser */

$this->title = 'Create Mention User';
$this->params['breadcrumbs'][] = ['label' => 'Mention Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mention-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
