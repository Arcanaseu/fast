<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\GroupCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Group Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Group Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'group_id',
            'company_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
