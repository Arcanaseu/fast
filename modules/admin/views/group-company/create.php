<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GroupCompany */

$this->title = 'Create Group Company';
$this->params['breadcrumbs'][] = ['label' => 'Group Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
