<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventsImage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-image-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'events_id')->dropDownList(AdminHelper::getEventsList()) ?>

    <?= $form->field($model, 'image_id')->dropDownList(AdminHelper::getImagesList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
