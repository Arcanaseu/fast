<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\EventsImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-image-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Events Image', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'events_id',
            'image_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
