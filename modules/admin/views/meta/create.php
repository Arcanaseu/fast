<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Meta */
/** @var mixed $route */
/** @var  $back  mixed */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Create Meta';
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'route' => $route,
        'back' => $back,
    ]) ?>

</div>
