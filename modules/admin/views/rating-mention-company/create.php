<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RatingMentionCompany */

$this->title = 'Create Rating Mention Company';
$this->params['breadcrumbs'][] = ['label' => 'Rating Mention Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-mention-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
