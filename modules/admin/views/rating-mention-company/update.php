<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RatingMentionCompany */

$this->title = 'Update Rating Mention Company: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rating Mention Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rating-mention-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
