<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RatingProduct */

$this->title = 'Update Rating Product: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rating Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rating-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
