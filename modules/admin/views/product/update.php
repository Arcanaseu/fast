<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/** @var  mixed $companyId */
/** @var  mixed $categoryId */
/** @var \app\models\MenuCategory $category */
/** @var mixed $route */
/** @var  $back  mixed */
/** @var  $backRoute  mixed */
/** @var string $images */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Update Product: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $backRoute,
        'route' => $route,
        'companyId' => $companyId,
        'categoryId' => $categoryId,
        'back' => $back,
    ]];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        'view',
        'id' => $model->id,
        'backRoute' => $backRoute,
        'route' => $route,
        'companyId' => $companyId,
        'categoryId' => $categoryId,
        'back' => $back,
        'productId' => $model->id
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'update' => true,
        'images' => $images,
        'route' => $route,
        'companyId' => $companyId,

    ]) ?>

</div>
