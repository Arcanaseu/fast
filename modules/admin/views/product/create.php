<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Product */
/** @var  mixed $companyId */
/** @var  mixed $categoryId */
/** @var \app\models\MenuCategory $category */
/** @var mixed $route */
/** @var  $back  mixed */
/** @var  $backRoute  mixed */
/** @var string $images */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';


$this->title = 'Create Product';
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $backRoute,
        'route' => $route,
        'companyId' => $companyId,
        'categoryId' => $categoryId,
        'back' => $back,
    ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'update' => false,
        'back' => $back,
        'images' => '',
    ]) ?>

</div>
