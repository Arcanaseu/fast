<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuVideo */

$this->title = 'Create Menu Video';
$this->params['breadcrumbs'][] = ['label' => 'Menu Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
