<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalProduct */
/** @var mixed $route */
/** @var \app\models\Product $product */
/** @var mixed $productId */

if(!isset($route))
    $route = 'index';

$this->title = 'Update Additional Product: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="additional-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'productId' => $productId,
        'product'  => $product
    ]) ?>

</div>
