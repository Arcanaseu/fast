<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AdditionalProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="additional-product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'additional_id')->dropDownList(AdminHelper::getAdditionalList()) ?>

    <?= $form->field($model, 'product_id') ->dropDownList(AdminHelper::getProductList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
