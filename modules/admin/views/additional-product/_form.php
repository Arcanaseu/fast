<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalProduct */
/* @var $form yii\widgets\ActiveForm */
/** @var \app\models\Product $product */
/** @var mixed $productId */
?>

<div class="additional-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'additional_id')->dropDownList(AdminHelper::getAdditionalList()) ?>

    <?php
    if(!($product instanceof \app\models\Product)){
        echo $form->field($model, 'product_id')->dropDownList(AdminHelper::getProductList());
    } else {
        echo   $form->field($model, 'product_id',
            ['options' => ['value'=> $product->id] ])->hiddenInput(['value'=> $product->id])->label(false);
    }


    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
