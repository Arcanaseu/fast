<?php
use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 17.08.2015
 * Time: 11:34
 */
/** @var User $model */
/** @var integer $index */
$modelRequest = new \app\models\FriendsRequestForm();


?>

<div class="people-item">
    <span  class="people-item-name" >
sdfghjkjhgvfxsdfghjklkjhgfd
    </span>
    <span  class="people-item-surname" >
sdfhjkjhgfdfghjkjgfd
    </span>
    <div  class="people-item-send-request" >
        <?php $form = ActiveForm::begin([
            'id' => 'people-search-form',
            'action' => '/admin/admin-friends/send',
            'method' => 'post',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],

        ]); ?>
        <?php  echo   $form->field($modelRequest, 'friendId',
            ['options' => ['value'=> $model->id] ])
            ->hiddenInput(['value'=> $model->id])->label(false); ?>
        <?php  echo   $form->field($modelRequest, 'userId',
            ['options' => ['value'=> Yii::$app->user->id] ])
            ->hiddenInput(['value'=> Yii::$app->user->id])->label(false); ?>
        <div class="form-group">
            <?= Html::submitButton('Send Request', ['class' => 'btn btn-warning']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

