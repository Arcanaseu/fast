<?php
/* @var $this yii\web\View */
/** @var $model PeopleSearchForm */
/** @var $dataProvider ActiveDataProvider */
use app\models\Image;
use app\models\PeopleSearchForm;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ListView;



?>
<style>
    .people-item-name{
        width: 250px;
        overflow: hidden;
        text-overflow: ellipsis;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 18px;
        color: darkslateblue;
        border: 1px solid #262626;
        border-left-width: 2px;
    }
    .people-item{
        height: 100%;
        min-height: 28px;
        background-color: lightgrey;
    }
    .people-item-surname{
        width: 250px;
        overflow: hidden;
        text-overflow: ellipsis;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 18px;
        color: darkslateblue;
        border: 1px solid #262626;
    }
    .people-item-send-request{
        display: inline;
        width: 250px;
        overflow: hidden;
        text-overflow: ellipsis;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 18px;
        color: darkslateblue;
        border: 1px solid #262626;
        border-right-width: 2px;
    }
    .button-mg-left-50{
        margin-left: 50px;
    }
</style>
<br>
<br>
<br>
<br>
<center><h2>Friends</h2></center>

<div class="peoples search">
    <h4>peoples search</h4>
    <?php $form = ActiveForm::begin([
        'id' => 'people-search-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    <?= $form->field($model, 'pattern')->textInput()->label(false) ?>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary col-md-offset-1']) ?>
    </div>
    <?php ActiveForm::end(); ?>

    <br>
    <br>
    <br>
    <?php if ($dataProvider): ?>
        <div>
            <br>
            <center><h3>Users List</h3></center>
            <div>
                <?php /** @var \app\models\User[] $users */ ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th>
                            <div class="table-banner-id" style="color: #ffffff"><b>Name</b></div>
                        </th>
                        <th>
                            <div class="table-banner-name" style="color: #ffffff"><b>Surname</b></div>
                        </th>
                        <th>
                            <div class="table-banner-name" style="color: #ffffff"><b>Avatar</b></div>
                        </th>
                        <th>
                            <div style="color: red"><b><i>Request</i></b></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\User $user */ ?>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?= $user->name; ?></td>
                            <td><?= $user->surname; ?></td>
                            <?php if ($user->avatar0 instanceof Image): ?>
                                <td><img src="/<?= $user->avatar0->path; ?>" width="100" height="70"></td>
                            <?php else: ?>
                                <td>No Image</td>
                            <?php endif; ?>

                            <td>
                                <?php
                                $modelRequest = new \app\models\FriendsRequestForm();
                                $form = ActiveForm::begin([
                                    'action' => '/admin/admin-friends/send',
                                    'method' => 'post',
                                    'options' => ['class' => 'form-horizontal'],
                                    'fieldConfig' => [
                                        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                        'labelOptions' => ['class' => 'col-lg-1 control-label'],
                                    ],

                                ]); ?>
                                <?php  echo   $form->field($modelRequest, 'friendId',
                                    ['options' => ['value'=> $user->id] ])
                                    ->hiddenInput(['value'=> $user->id])->label(false); ?>
                                <?php  echo   $form->field($modelRequest, 'userId',
                                    ['options' => ['value'=> Yii::$app->user->id] ])
                                    ->hiddenInput(['value'=> Yii::$app->user->id])->label(false); ?>
                                <div class="form-group">
                                    <?= Html::submitButton('Send Request', ['class' => 'btn btn-warning button-mg-left-50']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($user); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $pages */
            echo LinkPager::widget([
                'pagination' => $pages,
                //'registerLinkTags' => true
            ]);
            ?>
            <br>
            <?php
            /** @var mixed $userPages */
            if ($userPages) {
                echo LinkPager::widget([
                    'pagination' => $userPages,
                    //'registerLinkTags' => true
                ]);
            }

            ?>
        </div>
    <?php endif; ?>
</div>
<div class="friends-request">
    <br>
    <br>
    <center><h3>Requests</h3></center>
    <div>
        <?php /** @var \app\models\Request[] $requests */ ?>
        <table class="table table-striped table-bordered table-condensed">
            <thead style="background-color: #4f4f4f">
            <tr>
                <th>
                    <div class="table-banner-id" style="color: #ffffff"><b>Name</b></div>
                </th>
                <th>
                    <div class="table-banner-name" style="color: #ffffff"><b>Surname</b></div>
                </th>
                <th>
                    <div class="table-banner-name" style="color: #ffffff"><b>Avatar</b></div>
                </th>
                <th>
                    <div style="color: green; margin-left: 30px;"><b><i>Adopt</i></b></div>
                </th>
                <th>
                    <div style="color: red; margin-left: 30px;"><b><i>Refuse</i></b></div>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php /** @var \app\models\Request $request */ ?>
            <?php foreach ($requests as $request): ?>
                <tr>
                    <td><?= $request->initiator->name; ?></td>
                    <td><?= $request->initiator->surname; ?></td>
                    <?php if ($request->initiator->avatar0 instanceof Image): ?>
                        <td><img src="/<?= $request->initiator->avatar0->path; ?>" width="100" height="70"></td>
                    <?php else: ?>
                        <td>No Image</td>
                    <?php endif; ?>

                    <td>
                        <?php
                        $modelRequest = new \app\models\HandleRequestForm();
                        $form = ActiveForm::begin([
                            'action' => '/admin/admin-friends/handle',
                            'method' => 'post',
                            'options' => ['class' => 'form-horizontal'],
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                'labelOptions' => ['class' => 'col-lg-1 control-label'],
                            ],

                        ]); ?>
                        <?php  echo   $form->field($modelRequest, 'requestId',
                            ['options' => ['value'=> $request->id] ])
                            ->hiddenInput(['value'=> $request->id])->label(false); ?>
                        <?php  echo   $form->field($modelRequest, 'userId',
                            ['options' => ['value'=> Yii::$app->user->id] ])
                            ->hiddenInput(['value'=> Yii::$app->user->id])->label(false); ?>
                        <?php  echo   $form->field($modelRequest, 'decision',
                            ['options' => ['value'=> 1] ])
                            ->hiddenInput(['value'=> 1])->label(false); ?>
                        <div class="form-group">
                            <?= Html::submitButton('Adopt', ['class' => 'btn btn-success button-mg-left-50']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </td>
                    <td>
                        <?php
                        $modelRequest = new \app\models\HandleRequestForm();
                        $form = ActiveForm::begin([
                            'action' => '/admin/admin-friends/handle',
                            'method' => 'post',
                            'options' => ['class' => 'form-horizontal'],
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                'labelOptions' => ['class' => 'col-lg-1 control-label'],
                            ],

                        ]); ?>
                        <?php  echo   $form->field($modelRequest, 'requestId',
                            ['options' => ['value'=> $request->id] ])
                            ->hiddenInput(['value'=> $request->id])->label(false); ?>
                        <?php  echo   $form->field($modelRequest, 'userId',
                            ['options' => ['value'=> Yii::$app->user->id] ])
                            ->hiddenInput(['value'=> Yii::$app->user->id])->label(false); ?>
                        <?php  echo   $form->field($modelRequest, 'decision',
                            ['options' => ['value'=> 0] ])
                            ->hiddenInput(['value'=> 0])->label(false); ?>
                        <div class="form-group">
                            <?= Html::submitButton('Refuse', ['class' => 'btn btn-danger button-mg-left-50']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset ($request); ?>
            </tbody>
        </table>
    </div>
    <?php
    /** @var mixed $pages */
    echo LinkPager::widget([
        'pagination' => $pages,
        //'registerLinkTags' => true
    ]);
    ?>
    <br>
</div>
<div class="friends-list">
    <br>
    <br>
    <center><h3>Friend List</h3></center>
    <div>
        <?php /** @var \app\models\User[] $friends */ ?>
        <table class="table table-striped table-bordered table-condensed">
            <thead style="background-color: #4f4f4f">
            <tr>
                <th>
                    <div class="table-banner-id" style="color: #ffffff"><b>Name</b></div>
                </th>
                <th>
                    <div class="table-banner-name" style="color: #ffffff"><b>Surname</b></div>
                </th>
                <th>
                    <div class="table-banner-name" style="color: #ffffff"><b>Avatar</b></div>
                </th>
                <th>
                    <div style="color: red"><b><i>Unfriend</i></b></div>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php /** @var \app\models\User $friend */ ?>
            <?php foreach ($friends as $friend): ?>
                <tr>
                    <td><?= $friend->name; ?></td>
                    <td><?= $friend->surname; ?></td>
                    <?php if ($friend->avatar0 instanceof Image): ?>
                        <td><img src="/<?= $friend->avatar0->path; ?>" width="100" height="70"></td>
                    <?php else: ?>
                        <td>No Image</td>
                    <?php endif; ?>

                    <td>
                        <?php
                        $modelRequest = new \app\models\UnfriendForm();
                        $form = ActiveForm::begin([
                            'action' => '/admin/admin-friends/unfriend',
                            'method' => 'post',
                            'options' => ['class' => 'form-horizontal'],
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                'labelOptions' => ['class' => 'col-lg-1 control-label'],
                            ],

                        ]); ?>
                        <?php  echo   $form->field($modelRequest, 'friendId',
                            ['options' => ['value'=> $friend->id] ])
                            ->hiddenInput(['value'=> $friend->id])->label(false); ?>
                        <?php  echo   $form->field($modelRequest, 'userId',
                            ['options' => ['value'=> Yii::$app->user->id] ])
                            ->hiddenInput(['value'=> Yii::$app->user->id])->label(false); ?>
                        <div class="form-group">
                            <?= Html::submitButton('Unfriend', ['class' => 'btn btn-danger button-mg-left-50']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset ($friend); ?>
            </tbody>
        </table>
    </div>
    <?php
    /** @var mixed $pages */
    echo LinkPager::widget([
        'pagination' => $pages,
        //'registerLinkTags' => true
    ]);
    ?>
    <br>
</div>


