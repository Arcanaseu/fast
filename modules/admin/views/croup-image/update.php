<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GroupImage */


if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';


$this->title = 'Update Group Image: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Group Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="group-image-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
