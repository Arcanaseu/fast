<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GroupImage */


if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';


$this->title = 'Create Group Image';
$this->params['breadcrumbs'][] = ['label' => 'Group Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
