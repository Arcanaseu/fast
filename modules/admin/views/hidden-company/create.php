<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HiddenCompany */

$this->title = 'Create Hidden Company';
$this->params['breadcrumbs'][] = ['label' => 'Hidden Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hidden-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
