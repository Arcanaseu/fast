<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HiddenCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hidden-company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hidden_company_id')->dropDownList(AdminHelper::getNotHiddenCompaniesList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
