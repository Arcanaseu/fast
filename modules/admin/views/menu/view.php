<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/** @var mixed $route */
/** @var  $back  mixed */
/** @var string $images */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = $model->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', [

            'update',
            'model' => $model,
            'id' => $model->id,
            'route' => $route,
            'back' => $back,

        ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'route' => $route], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'title',
            'description',
        ],
    ]) ?>

    <br>
    <div style="font-size: large">
        Images&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <br><br>
    <div id="admin-image-container" style="width: 1000px;">
        <?=$images; ?>
    </div>

</div>
