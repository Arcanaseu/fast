<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/** @var  mixed $companyId */
/** @var  mixed $userId  */
/** @var mixed $route */
/** @var string $images */
/** @var  $back  mixed */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-view" ng-app="">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', [

            'update',
            'id' => $model->id,
            'route' => $route,
            'companyId' => $companyId,
            'userId' => $userId,
            'back'  => $back,
        ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'route' => $route], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'avatar',
            'title',
            'description',
            'text:ntext',
            'created',
            'is_closed',
            'is_expired',
            'date',
            'creator_user_id',
            'creator_company_id',
            'creator_type',
        ],
    ]) ?>



    <br>
    <div style="font-size: large">
        Images&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <br><br>
    <div id="admin-image-container" style="width: 1000px;">
        <?=$images; ?>
    </div>



</div>
