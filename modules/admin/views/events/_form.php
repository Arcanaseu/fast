<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */
/** @var string $route */
/** @var string $images */

$imgId = intval($model->avatar);
if(!isset($update))
    $update = false;
?>


<script>
    var imageField = '#events-image-drop';
    var imageTarget = '#events-create-image';
</script>
<div class="events-form" ng-app="">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'news' => 'News', 'action' => 'Action', 'event' => 'Event', ], ['prompt' => '']) ?>
    <?php if($update): ?>
    <label>
        <input type="checkbox" ng-model="change">   change the avatar
    </label>
    <?php else: ?>
        <span ng-init="change=true"></span>
    <?php endif; ?>
    <div ng-hide="change">
        <?php  echo   $form->field($model, 'avatar',
            ['options' => ['value'=> $imgId] ])->hiddenInput(['value'=> $imgId])->label(false); ?>
    </div>
    <div ng-show="change">
        <label>
            <input type="checkbox" ng-model="var"> <span ng-hide="var"> new image</span>
        </label>
        <span ng-hide="var">
            <div id="events-create-image" style="float: right;"></div>
            <br><br>
            <?= $form->field($model, 'avatar')
                ->dropDownList(AdminHelper::getImagesList(),  ['id' => 'events-image-drop']) ?>

        </span>
        <span ng-show="var">
            <?= $form->field($model, 'file')->fileInput() ?>
        </span>

    </div>


    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_closed')->checkbox() ?>


    <?php echo DatePicker::widget([
    'model' => $model,
    'attribute' => 'date',
    'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
    ]); ?>

    <?= $form->field($model, 'lat')->textInput() ?>

    <?= $form->field($model, 'lon')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if($update): ?>
        <br>
        <center>
            <div style="font-size: large">
                Image&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeImage">
            </div>
        </center>
        <br>
        <div ng-show="changeImage">
            <div>
                <div id="image-form-block" style="display: none;margin-bottom: 50px;">
                    <button id="close-add-image" class="btn" style="font-size: 24px;float:right; color: white">X</button>
                    <div style="padding-left: 400px">
                        <form action="/admin/default/image-create/" method="post" id="add-image-form" enctype="multipart/form-data">
                            <label>Image Name</label>
                            <br>
                            <input id='formTitle' type="text" name="title">
                            <br>
                            <label>Image Description</label>
                            <br>
                            <textarea id='formDesc'  name="description"></textarea>
                            <br>
                            <label>Image</label>
                            <input id='formFile' type="file" name="img">
                            <br>
                            <input id='formClass' type="hidden" name="class" value="Events">
                            <input id='formClassId' type="hidden" name="classId" value="<?=$model->id; ?>">
                            <input id='formBackRoute' type="hidden" name="backRoute" value="place-edit/index">

                            <input type="submit" name="submitted" value="submitted" >
                        </form>

                    </div>
                </div>
                <div id="add-image" >
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </div>
            </div>

            <br><br>
            <div id="admin-image-container" style="width: 1000px;">
                <?=$images; ?>
            </div>
        </div>

    <?php endif; ?>

</div>
