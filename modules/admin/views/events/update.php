<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/** @var  mixed $companyId */
/** @var  mixed $userId  */
/** @var string $images */
/** @var mixed $route */


if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';



$this->title = 'Update Events: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'companyId' => $companyId,
        'userId' => $userId,
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="events-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'update' => true,
        'images' => $images,
    ]) ?>

</div>
