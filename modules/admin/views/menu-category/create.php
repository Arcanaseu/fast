<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuCategory */

/** @var  mixed $companyId */
/** @var \app\models\Company $company */


/** @var mixed $route */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';


$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'companyId' => $companyId,
    ]];
$this->title = $model->title;
?>
<div class="menu-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'company'  => $company,
    ]) ?>

</div>
