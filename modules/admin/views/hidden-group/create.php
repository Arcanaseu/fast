<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HiddenGroup */

$this->title = 'Create Hidden Group';
$this->params['breadcrumbs'][] = ['label' => 'Hidden Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hidden-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
