<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HiddenGroup */

$this->title = 'Update Hidden Group: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hidden Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hidden-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
