<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\HiddenGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hidden Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hidden-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Hidden Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            'hidden_group_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
