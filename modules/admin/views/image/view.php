<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Image */
/** @var mixed $route */
if(!isset($route))
    $route = 'index';

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'path',
            'created',
            'title',
            'description',
        ],
    ]) ?>
    <br>
    <br>
    <div><img src="/<?=$model->path; ?>" width="250" height="200"></div>
    <br>
    <br>

</div>
