<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RatingMentionEvents */

$this->title = 'Create Rating Mention Events';
$this->params['breadcrumbs'][] = ['label' => 'Rating Mention Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-mention-events-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
