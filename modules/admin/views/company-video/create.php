<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanyVideo */

$this->title = 'Create Company Video';
$this->params['breadcrumbs'][] = ['label' => 'Company Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
