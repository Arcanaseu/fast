<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/** @var mixed $route */
/** @var  $back  mixed */
/** @var string $images */
if(!isset($update))
    $update = false;
if(!isset($back))
    $back = '';
if(!isset($route))
    $route = 'index';


$this->title = $model->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', [

            'update',
            'model' => $model,
            'id' => $model->id,
            'route' => $route,
            'back' => $back,

        ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'route' => $route], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'text:ntext',
            'avatar',
            'address',
            'city_id',
            'average_score',
            'email:email',
            'site',
            'lat',
            'lon',
        ],
    ]) ?>


    <br><br><br>
    <div>
        <table class="table table-striped table-bordered table-condensed">
            <thead style="background-color: #4f4f4f">
            <tr>
                <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                <th><div class="table-banner-show"  style="color: #ffffff"><b>Image</b></div></th>
                <th><div  style="color: red"><b><i>Controls</i></b></div></th>
            </tr>
            </thead>
            <tbody>
            <?php

            /** @var \app\models\AdditionalCompany $a */
            foreach($model->additionalCompanies as $a ): ?>
                <?php $end = (strlen($a->additional->description) > 151)?'...':''; ?>
                <tr>
                    <td><?=$a->additional->title ?></td>
                    <td><?=substr($a->additional->description,0,150).$end ?></td>
                    <td><img src="/<?=$a->additional->image->path ?>" width="100" height="70"></td>
                    <td>
                        <a href='/admin/additional/update?id=<?=$a->additional->id; ?>&route=<?=$route; ?>&companyId=<?=$model->id; ?>&back=additional' target='_blank'>
                            <span class='glyphicon glyphicon-pencil' ></span>
                        </a>
                        <a href='/admin/additional/view?id=<?=$a->additional->id; ?>&route=<?=$route; ?>&companyId=<?=$model->id; ?>&back=additional' target='_blank'>
                            <span class='glyphicon glyphicon-eye-open' ></span>
                        </a>
                        <a href='/admin/place-edit/delete?id=<?=$a->id; ?>&class=<?=get_class($a); ?>&back=additional'>
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset ($a); ?>
            </tbody>
        </table>
    </div>



    <br>
    <div style="font-size: large">
        Images&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <br><br>
    <div id="admin-image-container" style="width: 1000px;">
        <?=$images; ?>
    </div>

</div>
