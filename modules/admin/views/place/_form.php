<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
/** @var string $images */
/** @var string $route */
$imgId = $model->avatar;
if(!isset($update))
    $update = false;
if(!isset($panel))
    $panel = false;
?>
<script>
    var imageField = '#company-image-drop';
    var imageTarget = '#company-create-avatar';
</script>
<div class="company-form" >

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php if($update || $panel): ?>
        <label>
            <input type="checkbox" ng-model="change">   change the avatar
        </label>
    <?php else: ?>
        <span ng-init="change=true"></span>
    <?php endif; ?>
    <div ng-hide="change">
        <?php  echo   $form->field($model, 'avatar',
            ['options' => ['value'=> $imgId] ])->hiddenInput(['value'=> $imgId])->label(false); ?>
    </div>
    <div ng-show="change">
        <label>
            <input type="checkbox" ng-model="var"> <span ng-hide="var"> new image</span>
        </label>
        <span ng-hide="var">
            <div id="company-create-avatar" style="float: right;"></div>
            <br><br>
            <?= $form->field($model, 'avatar')
                ->dropDownList(AdminHelper::getImagesList(),  ['id' => 'company-image-drop']) ?>

        </span>
        <span ng-show="var">
            <?= $form->field($model, 'file')->fileInput() ?>
        </span>

    </div>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opened')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->dropDownList(AdminHelper::getCityList()) ?>

    <?= $form->field($model, 'average_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lat')->textInput() ?>

    <?= $form->field($model, 'lon')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


    <?php if($update): ?>
        <!--   Additional  -->
        <br>
        <center>
            <div style="font-size: large">
                Additional&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeAdditional">
            </div>
        </center>
        <br>
        <div ng-show="changeAdditional">
            <div id="add-additional" >
                <a href="/admin/additional/create/?route=<?=$route; ?>&companyId=<?=$model->id; ?>&back=additional" target="_blank">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <div id="add-additional-company" >
                <a href="/admin/additional-company/create/?route=<?=$route; ?>&companyId=<?=$model->id; ?>&back=additional" target="_blank">
                    <button class="btn btn-success" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;Create&nbsp;&nbsp;</button>
                </a>
            </div>
            <br><br><br>
            <div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-show"  style="color: #ffffff"><b>Image</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    /** @var \app\models\AdditionalCompany $a */
                    foreach($model->additionalCompanies as $a ): ?>
                        <?php $end = (strlen($a->additional->description) > 151)?'...':''; ?>
                        <tr>
                            <td><?=$a->additional->title ?></td>
                            <td><?=substr($a->additional->description,0,150).$end ?></td>
                            <td><img src="/<?=$a->additional->image->path ?>" width="100" height="70"></td>
                            <td>
                                <a href='/admin/additional/update?id=<?=$a->additional->id; ?>&route=<?=$route; ?>&companyId=<?=$model->id; ?>&back=additional' target='_blank'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/additional/view?id=<?=$a->additional->id; ?>&route=<?=$route; ?>&companyId=<?=$model->id; ?>&back=additional' target='_blank'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/place-edit/delete?id=<?=$a->id; ?>&class=<?=get_class($a); ?>&back=additional'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($a); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <br>
        <center>
            <div style="font-size: large">
                Image&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeImage">
            </div>
        </center>
        <br>
        <div ng-show="changeImage">
            <div>
                <div id="image-form-block" style="display: none;margin-bottom: 50px;">
                    <button id="close-add-image" class="btn" style="font-size: 24px;float:right; color: white">X</button>
                    <div style="padding-left: 400px">
                        <form action="/admin/default/image-create/" method="post" id="add-image-form" enctype="multipart/form-data">
                            <label>Image Name</label>
                            <br>
                            <input id='formTitle' type="text" name="title">
                            <br>
                            <label>Image Description</label>
                            <br>
                            <textarea id='formDesc'  name="description"></textarea>
                            <br>
                            <label>Image</label>
                            <input id='formFile' type="file" name="img">
                            <br>
                            <input id='formClass' type="hidden" name="class" value="Company">
                            <input id='formClassId' type="hidden" name="classId" value="<?=$model->id; ?>">
                            <input id='formBackRoute' type="hidden" name="backRoute" value="admin-panel/index">

                            <input type="submit" name="submitted" value="submitted" >
                        </form>

                    </div>
                </div>
                <div id="add-image" >
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </div>
            </div>

            <br><br>
            <div id="admin-image-container" style="width: 1000px;">
                <?=$images; ?>
            </div>
        </div>
        <br>
        <br>

    <?php endif; ?>


</div>
