<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kitchen */
/** @var mixed $route */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Create Kitchen';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitchen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,

    ]) ?>

</div>
