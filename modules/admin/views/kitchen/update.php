<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kitchen */
/** @var  mixed $companyId */
/** @var  mixed $productId  */
/** @var \app\models\Company $company */

/** @var mixed $route */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Update Kitchen: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'companyId' => $companyId,
        'productId' => $productId,
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kitchen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'update' => true,
    ]) ?>

</div>
