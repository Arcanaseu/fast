<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AcceptedUser */

$this->title = 'Create Accepted User';
$this->params['breadcrumbs'][] = ['label' => 'Accepted Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accepted-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
