<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AcceptedUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accepted-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'events_id')->dropDownList(AdminHelper::getEventsList()) ?>

    <?= $form->field($model, 'user_id')->dropDownList(AdminHelper::getUsersList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
