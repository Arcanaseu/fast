<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HiddenUser */

$this->title = 'Create Hidden User';
$this->params['breadcrumbs'][] = ['label' => 'Hidden Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hidden-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
