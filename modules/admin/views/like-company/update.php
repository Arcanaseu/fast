<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LikeCompany */

$this->title = 'Update Like Company: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Like Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="like-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
