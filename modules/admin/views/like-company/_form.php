<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LikeCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="like-company-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'target_id')->dropDownList(AdminHelper::getCompanyList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
