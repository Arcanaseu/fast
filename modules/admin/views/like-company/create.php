<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LikeCompany */

$this->title = 'Create Like Company';
$this->params['breadcrumbs'][] = ['label' => 'Like Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="like-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
