<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MentionEventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mention Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mention-events-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mention Events', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'creator_id',
            'target_id',
            'created',
            'rate',
            // 'text',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
