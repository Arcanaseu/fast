<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductCategory */
/** @var mixed $route */
/** @var  $back  mixed */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Update Product Category: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'route' => $route,
        'back' => $back,
        'update' => true,
    ]) ?>

</div>
