<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\RatingCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rating Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Rating Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'company_id',
            'score',
            'voted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
