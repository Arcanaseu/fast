<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RatingCompany */

$this->title = 'Create Rating Company';
$this->params['breadcrumbs'][] = ['label' => 'Rating Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
