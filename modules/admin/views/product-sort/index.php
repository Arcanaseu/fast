<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProductSortSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Sorts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-sort-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Sort', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description',
            'brand_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
