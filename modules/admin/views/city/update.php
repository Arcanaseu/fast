<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\City */
/** @var mixed $route */
/** @var  $back  mixed */


if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';


$this->title = 'Update City: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = [
    'label' => $model->city,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
