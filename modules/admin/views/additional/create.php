<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Additional */
/** @var mixed $route */
if(!isset($route))
    $route = 'index';

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="additional-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

