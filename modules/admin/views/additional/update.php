<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Additional */
/** @var  mixed $companyId */
/** @var  mixed $productId  */
if(!isset($route))
    $route = 'index';

/** @var mixed $route */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'companyId' => $companyId,
        'productId' => $productId,
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="additional-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'update' => true,
    ]) ?>

</div>
