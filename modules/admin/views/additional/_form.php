<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Additional */
/* @var $form yii\widgets\ActiveForm */

$imgId = $model->image_id;
if(!isset($update))
    $update = false;
?>
<script>
    var imageField = '#additional-image-drop';
    var imageTarget = '#additional-create-image';
</script>

<div class="additional-form" ng-app="" >

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php if($update): ?>

    <label>
        <input type="checkbox" ng-model="change">   change the image
    </label>
     <?php else: ?>
        <span ng-init="change=true"></span>
    <?php endif; ?>
    <div ng-hide="change">
        <?php  echo   $form->field($model, 'image_id',
            ['options' => ['value'=> $imgId] ])->hiddenInput(['value'=> $imgId])->label(false); ?>
    </div>
    <div ng-show="change">
        <label>
            <input type="checkbox" ng-model="var"> <span ng-hide="var"> new image</span>
        </label>
        <span ng-hide="var">
            <div id="additional-create-image" style="float: right;"></div>
            <br><br>
            <?= $form->field($model, 'image_id')
                ->dropDownList(AdminHelper::getImagesList(),  ['id' => 'additional-image-drop']) ?>

        </span>
        <span ng-show="var">
            <?= $form->field($model, 'file')->fileInput() ?>
        </span>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
