<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductKitchen */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Create Product Kitchen';
$this->params['breadcrumbs'][] = ['label' => 'Product Kitchens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-kitchen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
