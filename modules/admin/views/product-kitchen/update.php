<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductKitchen */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Update Product Kitchen: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Kitchens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-kitchen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
