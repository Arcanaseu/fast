<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanyMetro */
/** @var mixed $route */
/** @var mixed $companyId */
/** @var \app\models\Company $company */

if(!isset($route))
    $route = 'index';



$this->title = 'Create Company Metro';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-metro-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'companyId' => $companyId,
        'company' => $company,
    ]) ?>

</div>
