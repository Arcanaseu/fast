<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuCategoryItem */

/** @var  mixed $companyId */
/** @var  mixed $categoryId */
/** @var \app\models\MenuCategory $category */
/** @var mixed $route */
/** @var  $back  mixed */
/** @var \app\models\Product $product */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'companyId' => $companyId,
        'categoryId'  => $categoryId,
        'category'  => $category,
        'back'  => $back
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="menu-category-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'route' => $route,
        'companyId' => $companyId,
        'category'  => $category,
        'back'  => $back,
        'product'  => $product

    ]) ?>

</div>
