<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuCategoryItem */
/** @var  mixed $companyId */
/** @var  mixed $categoryId */
/** @var \app\models\MenuCategory $category */
/** @var \app\models\Product $product */
/** @var mixed $route */
/** @var  $back  mixed */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-category-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'route' => $route,
        'companyId' => $companyId,
        'category'  => $category,
        'categoryId'  => $categoryId,
        'back'  => $back,
        'product' => $product

    ]) ?>

</div>
