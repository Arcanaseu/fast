<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MenuCategoryItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu Category Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-category-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Menu Category Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'product_id',
            'category_id',
            'price',
            'discount',
            // 'discount_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
