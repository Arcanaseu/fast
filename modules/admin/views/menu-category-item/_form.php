<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MenuCategoryItem */
/* @var $form yii\widgets\ActiveForm */
/** @var \app\models\MenuCategory $category */
/** @var \app\models\Product $product */
/** @var mixed $route */
/** @var $back mixed */
/** @var $companyId */
/** @var $categoryId */


?>

<div class="menu-category-item-form" ng-app="">
    <?php if($product instanceof \app\models\Product): ?>

        <h3>Product</h3>
        <br>
    <div><b><?=$product->title ?></b></div><br>
    <div><img src="/<?=$product->avatar0->path; ?>" width="350" height="250"></div>
        <br>
        <br>
    <a href="/admin/product/update/?id=<?=$product->id; ?>&route=place-edit/index&backRoute=menu-category-item/create&companyId=<?=$companyId; ?>&categoryId=<?=$categoryId; ?>&back=menu" target="_blank">
        <button class="btn btn-info">Update Product</button>
    </a>
        <?php $form = ActiveForm::begin();
        echo   $form->field($model, 'product_id',
            ['options' => ['value'=> $product->id] ])
            ->hiddenInput(['value'=> $product->id])
            ->label(false); ?>
    <?php     else: ?>
    <label>
        <input type="checkbox" ng-model="var">   create new product
    </label>
    <span ng-show="var">
        <br>
        <a href="/admin/product/create/?route=place-edit/index&backRoute=menu-category-item/create&companyId=<?=$companyId; ?>&categoryId=<?=$categoryId; ?>&back=menu" target="_blank">
            <button  id="create-new-product" class="btn btn-warning">Create new Product</button>
        </a>



    </span>

    <?php $form = ActiveForm::begin(); ?>

    <span ng-hide="var">
        <?= $form->field($model, 'product_id')->dropDownList(AdminHelper::getProductList()) ?>
    </span>
    <?php endif; ?>





    <?php
    if(!$category){
        echo $form->field($model, 'category_id')->dropDownList(AdminHelper::getMenuList());
    } else {
        echo   $form->field($model, 'category_id',
            ['options' => ['value'=> $category->id] ])->hiddenInput(['value'=> $category->id])->label(false);
    }


    ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount_type')->dropDownList([ 'percent' => 'Percent', 'num' => 'Num', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

