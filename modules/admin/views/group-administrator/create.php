<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GroupAdministrator */

$this->title = 'Create Group Administrator';
$this->params['breadcrumbs'][] = ['label' => 'Group Administrators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-administrator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
