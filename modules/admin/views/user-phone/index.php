<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserPhoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Phones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-phone-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Phone', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'phone_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
