<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Hidden */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hidden-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'events_id')->dropDownList(AdminHelper::getNotHiddenEventsList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
