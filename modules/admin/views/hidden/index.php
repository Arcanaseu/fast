<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\HiddenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hiddens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hidden-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Hidden', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'creator_type',
            'user_id',
            'company_id',
            'created',
            // 'events_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
