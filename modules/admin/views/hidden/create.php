<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Hidden */

$this->title = 'Create Hidden';
$this->params['breadcrumbs'][] = ['label' => 'Hiddens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hidden-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
