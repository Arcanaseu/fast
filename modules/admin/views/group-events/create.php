<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GroupEvents */

$this->title = 'Create Group Events';
$this->params['breadcrumbs'][] = ['label' => 'Group Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-events-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
