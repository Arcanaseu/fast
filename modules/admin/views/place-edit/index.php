<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 20.07.2015
 * Time: 12:16
 */
use app\models\Product;
use app\models\ProductCategory;
use yii\bootstrap\Nav;

/** @var integer $id */
/** @var \app\models\AdditionalCompany[] $additional */
/** @var string $images */
/** @var string $class */
/** @var string $backRoute */
/** @var \app\models\CompanyKitchen[] $kitchens */
/** @var \app\models\CompanyMetro[] $metro */
/** @var \app\models\CompanyPhone[] $phone */
/** @var \app\models\Events[] $events */
/** @var \app\models\Menu $menu */
/** @var \app\models\User[] $users */
/** @var \app\models\Company $company */

?>

<script>
    var backRoute = 'place-edit/index'
</script>
<div ng-app="">
    <div class="index-container-right">
        <?php if(Yii::$app->session->hasFlash('not_deleted')): ?>
            <div class="alert alert-danger" role="alert">
                <?= Yii::$app->session->getFlash('not_deleted') ?>
            </div>
        <?php endif; ?>
        <br>
        <center><span style="font-size: x-large; color:darkolivegreen">Company &nbsp;&nbsp;-&nbsp;&nbsp;<?=$company->title ?></span></center>
        <br>
        <!-- Company Edit -->
        <br>
        <center>
            <div style="font-size: large">
                Edit&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeEdit">
            </div>
        </center>
        <br>
        <div ng-show="changeEdit">
            <?= $this->render('@app/modules/admin/views/place/_form', [
                'model' => $company,
                'update' => false,
                'panel'  => true
            ]) ?>
        </div>
        <br>
        <hr>
        <!-- Menu -->
        <br>
        <center>
            <div style="font-size: large">
                Menu&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeMenu">
            </div>
        </center>

        <br>
        <div ng-show="changeMenu">
            <center>
                |&nbsp;&nbsp;&nbsp;&nbsp;
                <a href='/admin/menu/update?id=<?=$menu->id; ?>&route=<?=$backRoute; ?>&back=menu' target='_blank'>
                    <span class='glyphicon glyphicon-pencil' ></span>
                </a>
                &nbsp;&nbsp;
                <a href='/admin/menu/view?id=<?=$menu->id; ?>&route=<?=$backRoute; ?>&back=menu' target='_blank'>
                    <span class='glyphicon glyphicon-eye-open' ></span>
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;|
            </center>
            <div id="add-menu-category" >
                <a href="/admin/menu-category/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=menu" target="_blank">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;Category&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\MenuCategory $category */  ?>
                <?php foreach($menu->menuCategories as $category): ?>

                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Menu Category Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Product Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Product Description</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Price</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Discount</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Discount Type</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Avatar</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\MenuCategoryItem $item */  ?>
                    <?php foreach($category->menuCategoryItems as $item): ?>
                        <tr>
                            <td><?=$category->title; ?></td>
                            <td><?=$item->product->title; ?></td>
                            <td><?=$item->product->description; ?></td>
                            <td><?=$item->price; ?></td>
                            <td><?=$item->discount; ?></td>
                            <td><?=$item->discount_type; ?></td>
                            <td><img src="/<?=$item->product->avatar0->path; ?>" width="100" height="70"></td>

                            <td>
                                <a href='/admin/menu-category-item/update?id=<?=$item->id; ?>&route=<?=$backRoute; ?>&categoryId=<?=$item->id; ?>&back=menu' target='_blank'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/menu-category-item/view?id=<?=$item->id; ?>&route=<?=$backRoute; ?>&categoryId=<?=$category->id; ?>&back=menu' target='_blank'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/place-edit/delete?id=<?=$item->id; ?>&class=<?=get_class($item); ?>&back=menu'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <br>
                    <div style="text-align: center; color: darkolivegreen;font-weight: bold;">
                        <?=$category->title ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;
                        <a href="/admin/menu-category/update/?id=<?=$category->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=menu" target="_blank">
                            <span class='glyphicon glyphicon-pencil' ></span>
                        </a>
                        &nbsp;&nbsp;
                        <a href='/admin/menu-category/view?id=<?=$category->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$category->id; ?>&back=menu' target='_blank'>
                            <span class='glyphicon glyphicon-eye-open' ></span>
                        </a>
                        &nbsp;&nbsp;
                        <a href='/admin/place-edit/delete?id=<?=$category->id; ?>&class=<?=get_class($category); ?>&back=menu'>
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </div>
                    <div id="add-product" >
                        <a href="/admin/menu-category-item/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&categoryId=<?=$category->id; ?>&back=menu" target="_blank">
                            <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;Product&nbsp;</button>
                        </a>
                    </div>
                    <br>
                    <br>
                    <br>
                    <?php endforeach; ?>
                    <?php unset ($prod, $category); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!-- Events -->
        <br>
        <center>
            <div style="font-size: large">
                Events&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeEvents">
            </div>
        </center>

        <br>
        <div ng-show="changeEvents">
            <div id="add-events" >
                <a href="/admin/events/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=events" target="_blank">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>

                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Avatar</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Date</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var \app\models\Events $ev */
                    foreach($events as $ev ): ?>
                        <tr>
                            <td><?=$ev->title; ?></td>
                            <td><?=$ev->description; ?></td>
                            <td><img src="/<?=($ev->avatar0)?$ev->avatar0->path:'image/def.jpg'; ?>" width="100" height="70"></td>
                            <td><?=$ev->date; ?></td>
                            <td>
                                <a href='/admin/events/update?id=<?=$ev->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=events' target='_blank'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/events/view?id=<?=$ev->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=events' target='_blank'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/place-edit/delete?id=<?=$ev->id; ?>&class=<?=get_class($ev); ?>$back=events'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($ph); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!-- User -->
        <br>
        <center>
            <div style="font-size: large">
                Users&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeUser">
            </div>
        </center>

        <br>
        <div ng-show="changeUser">
            <div id="add-user" >
                <a href="/admin/user-company/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=user">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>

                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Name</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Surname</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Email</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Avatar</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    /** @var \app\models\UserCompany $user */
                    foreach($company->userCompanies as $uc ): ?>
                        <tr>
                            <td><?=$uc->user->name; ?></td>
                            <td><?=$uc->user->surname; ?></td>
                            <td><?=$uc->user->email; ?></td>
                            <td><img src="/<?=$uc->user->avatar0->path; ?>" width="100" height="70"></td>
                            <td>
                                <a href='/admin/place-edit/delete?id=<?=$uc->id; ?>&class=<?=get_class($uc); ?>&back=phone'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($uc); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!-- Group -->
        <br>
        <center>
            <div style="font-size: large">
                Group&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeGroup">
            </div>
        </center>

        <br>
        <div ng-show="changeGroup">
            <div id="add-group" >
                <a href="/admin/group/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=events" target="_blank">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\GroupCompany[] $groups */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\GroupCompany $gc */  ?>
                    <?php foreach($groups as $gc): ?>
                        <tr>
                            <td><?=$gc->group->title; ?></td>
                            <td><?=$gc->group->description; ?></td>

                            <td>
                                <a href='/admin/group/update/?id=<?=$gc->group->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=events' target="_blank">
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/group/view/?id=<?=$gc->group->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=events' target="_blank">
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/admin-panel/delete?id=<?=$gc->id; ?>&class=<?=get_class($gc); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($gc); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!-- Phone -->
        <br>
        <center>
            <div style="font-size: large">
                Phone&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changePhone">
            </div>
        </center>

        <br>
        <div ng-show="changePhone">
            <div id="add-phone" >
                <a href="/admin/phone/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=phone">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>

                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Country Code</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Number</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var \app\models\CompanyPhone $ph */
                    foreach($phone as $ph ): ?>
                        <tr>
                            <td><?=$ph->phone->country->phone_code; ?></td>
                            <td><?=$ph->phone->number ?></td>
                            <td>
                                <a href='/admin/place-edit/delete?id=<?=$ph->id; ?>&class=<?=get_class($ph); ?>&back=phone'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($ph); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!-- Metro -->
        <br>
        <center>
            <div style="font-size: large">
                Metro&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeMetro">
            </div>
        </center>
        <br>
        <div ng-show="changeMetro">
            <div id="add-metro" >
                <a href="/admin/company-metro/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=metro">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>

                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Metro</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>latitude</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>longitude</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var \app\models\CompanyMetro $m */
                    foreach($metro as $m ): ?>
                        <tr>
                            <td><?=$m->metro->metro; ?></td>
                            <td><?=$m->metro->lat; ?></td>
                            <td><?=$m->metro->lon; ?></td>
                            <td>
                                <a href='/admin/place-edit/delete?id=<?=$m->id; ?>&class=<?=get_class($m); ?>&back=metro'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($m); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!-- Kitchen -->
        <br>
        <center>
            <div style="font-size: large">
                Kitchen&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeKitchen">
            </div>
        </center>
        <br>
        <div ng-show="changeKitchen">
            <div id="add-kitchen" >
                <a href="/admin/company-kitchen/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=kitchen">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>

                </a>
            </div>
            <div id="add-company-kitchen" >
                <a href="/admin/kitchen/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=kitchen">
                    <button class="btn btn-success" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;Create&nbsp;&nbsp;</button>

                </a>
            </div>
            <br>
            <br>
            <br>
            <div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var \app\models\CompanyKitchen $cc */
                    foreach($kitchens as $cc ): ?>
                        <?php $end = (strlen($cc->kitchen->description) > 131)?'...':''; ?>
                        <tr>
                            <td><?=$cc->kitchen->title ?></td>
                            <td><?=substr($cc->kitchen->description,0,130).$end ?></td>
                            <td>
                                <!-- todo only for administrators  -->
                                <a href='/admin/kitchen/update?id=<?=$cc->kitchen->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=kitchen' target='_blank'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/kitchen/view?id=<?=$cc->kitchen->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=kitchen' target='_blank'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/place-edit/delete?id=<?=$cc->id; ?>&class=<?=get_class($cc); ?>&back=kitchen'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($cc); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!--   Additional  -->
        <br>
        <center>
            <div style="font-size: large">
                Additional&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeAdditional">
            </div>
        </center>
        <br>
        <div ng-show="changeAdditional">
            <div id="add-additional" >
                <a href="/admin/additional-company/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=additional">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <div id="add-additional-company" >
                <a href="/admin/additional/create/?route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=additional">
                    <button class="btn btn-success" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;Create&nbsp;&nbsp;</button>
                </a>
            </div>
            <br><br><br>
            <div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-show"  style="color: #ffffff"><b>Image</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var \app\models\AdditionalCompany $a */
                    foreach($additional as $a ): ?>
                        <?php $end = (strlen($a->additional->description) > 151)?'...':''; ?>
                        <tr>
                            <td><?=$a->additional->title ?></td>
                            <td><?=substr($a->additional->description,0,150).$end ?></td>
                            <td><img src="/<?=$a->additional->image->path ?>" width="100" height="70"></td>
                            <td>
                                <a href='/admin/additional/update?id=<?=$a->additional->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=additional' target='_blank'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/additional/view?id=<?=$a->additional->id; ?>&route=<?=$backRoute; ?>&companyId=<?=$id; ?>&back=additional' target='_blank'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                &nbsp;&nbsp;
                                <a href='/admin/place-edit/delete?id=<?=$a->id; ?>&class=<?=get_class($a); ?>&relId=<?=$a->additional->id; ?>&relClass=<?=get_class($a->additional); ?>&back=additional'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($a); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
        <hr>
        <!--   Image  -->
        <br>
        <center>
            <div style="font-size: large">
                Image&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeImage">
            </div>
        </center>
        <br>
        <div ng-show="changeImage">
            <div>
                <div id="image-form-block" style="display: none;margin-bottom: 50px;">
                    <button id="close-add-image" class="btn" style="font-size: 24px;float:right; color: white">X</button>
                    <div style="padding-left: 400px">
                        <form action="/admin/default/image-create/" method="post" id="add-image-form" enctype="multipart/form-data">
                            <label>Image Name</label>
                            <br>
                            <input id='formTitle' type="text" name="title">
                            <br>
                            <label>Image Description</label>
                            <br>
                            <textarea id='formDesc'  name="description"></textarea>
                            <br>
                            <label>Image</label>
                            <input id='formFile' type="file" name="img">
                            <br>
                            <input id='formClass' type="hidden" name="class" value="<?=$class; ?>">
                            <input id='formClassId' type="hidden" name="classId" value="<?=$id; ?>">
                            <input id='formBackRoute' type="hidden" name="backRoute" value="place-edit/index">

                            <input type="submit" name="submitted" value="submitted" >
                        </form>

                    </div>
                </div>
                <div id="add-image" >
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </div>
            </div>

            <br><br>
            <div id="admin-image-container" style="width: 1000px;">
                <?=$images; ?>
            </div>
        </div>
    </div>
</div>





