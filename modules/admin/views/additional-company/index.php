<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\AdditionalCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Additional Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="additional-company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Additional Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'additional_id',
            'company_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
