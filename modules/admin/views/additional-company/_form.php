<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalCompany */
/* @var $form yii\widgets\ActiveForm */
/** @var \app\models\Company $company */
/** @var mixed $companyId */

$ci = $companyId;
?>

<div class="additional-company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'additional_id')->dropDownList(AdminHelper::getAdditionalList()) ?>

    <?php
        if(!$company){
            echo $form->field($model, 'company_id')->dropDownList(AdminHelper::getCompanyList());
        } else {
            echo   $form->field($model, 'company_id',
                ['options' => ['value'=> $ci] ])->hiddenInput(['value'=> $ci])->label(false);
        }


    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
