<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdditionalCompany */

/** @var mixed $route */
/** @var \app\models\Company $company */
/** @var mixed $companyId */

if(!isset($route))
    $route = 'index';

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="additional-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'companyId' => $companyId

    ]) ?>

</div>
