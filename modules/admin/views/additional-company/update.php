<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdditionalCompany */
/** @var mixed $route */
/** @var \app\models\Company $company */
/** @var mixed $companyId */

if(!isset($route))
    $route = 'index';

$this->title = 'Update Additional Company: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => [
        'view',
        'id' => $model->id,
        'companyId' => $companyId,
        'route' => $route
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="additional-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'companyId' => $companyId,
        'company' => $company
    ]) ?>

</div>
