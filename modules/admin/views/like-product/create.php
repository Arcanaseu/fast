<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LikeProduct */

$this->title = 'Create Like Product';
$this->params['breadcrumbs'][] = ['label' => 'Like Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="like-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
