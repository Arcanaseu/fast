<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GroupVideo */

$this->title = 'Create Group Video';
$this->params['breadcrumbs'][] = ['label' => 'Group Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
