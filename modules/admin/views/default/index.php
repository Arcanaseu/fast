<?php
/** @var $model \app\models\User */
use app\models\City;

/** @var mixed $route */
/** @var  $back  mixed */
/** @var string $images */
/** @var string $access */
?>

<div ng-app="">
    <div class="index-container-right">
        <div style="width: 350px; height: 250px;border-radius: 50px; float:left;">
            <?php  if($model->avatar0 instanceof \app\models\Image): ?>
            <img src="/<?=$model->avatar0->path; ?>" width="350" height="250">
            <?php endif; ?>
        </div>
        <div class="person-box">
            <div>
                <div class="person-title">name</div>
                <span class="person-description"><?=$model->name ?></span>
            </div>
            <div>
                <div class="person-title">surname</div>
                <span class="person-description"><?=$model->surname ?></span>
            </div>
            <div>
                <div class="person-title">Address</div>
                <span class="person-description"><?=$model->address ?></span>
            </div>
            <div>
                <div class="person-title">City</div>
                <span class="person-description"><?=($model->city0 instanceof City)?$model->city0->city:''; ?></span>
            </div>
            <div>
                <div class="person-title">Email</div>
                <span class="person-description"><?=$model->email ?></span>
            </div>
            <div>
                <div class="person-title">Phone</div>
                <span class="person-description">
                    <?php if($model->phone0 instanceof \app\models\Phone): ?>
                        <?php if($model->phone0->country instanceof \app\models\Country):  ?>
                    <span>+</span>&nbsp;
                    <span>(<?=$model->phone0->country->phone_code ?>)</span>&nbsp;
                    <?php endif; ?>
                    <span><?=$model->phone0->number ?></span>
                    <? endif ?>
                </span>
            </div>

        </div>
        <hr style="width: 100%;">
        <div class="person-edit"  ng-app="">
            <div style="font-size: large; float:right;margin-bottom: 35px;">
                Edit&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="edit">
            </div>
            <div ng-show="edit">
                <?= $this->render('@app/modules/admin/views/user/_form', [
                    'model' => $model,
                    'images' => $images,
                    'route' => $route,
                    'back' => $back,
                    'access' => $access,
                    'update' => true,
                ]) ?>
            </div>
            <div ng-hide="edit">
                <?php if(trim($images) != ''): ?>
                <br>
                <div style="font-size: large">
                    Images&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <br><br>
                <div id="admin-image-container" >
                    <?=$images; ?>
                </div>
                <?php endif; ?>
            </div>

        </div>


    </div>
</div>
