<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Repost */

$this->title = 'Create Repost';
$this->params['breadcrumbs'][] = ['label' => 'Reposts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repost-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
