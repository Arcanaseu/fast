<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RatingEvents */

$this->title = 'Create Rating Events';
$this->params['breadcrumbs'][] = ['label' => 'Rating Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-events-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
