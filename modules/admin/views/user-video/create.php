<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserVideo */

$this->title = 'Create User Video';
$this->params['breadcrumbs'][] = ['label' => 'User Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-video-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
