<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserVideo */

$this->title = 'Update User Video: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-video-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
