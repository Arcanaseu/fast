<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserVideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Videos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-video-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Video', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'video_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
