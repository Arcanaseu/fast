<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyKitchen */
/** @var mixed $route */
/** @var mixed $companyId */

if(!isset($route))
    $route = 'index';



$this->title = 'Update Company Kitchen: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => [$route]];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => [
        'view',
        'id' => $model->id,
        'companyId' => $companyId,
        'route' => $route
    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-kitchen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
