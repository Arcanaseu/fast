<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyKitchen */
/* @var $form yii\widgets\ActiveForm */
/** @var \app\models\Company $company */
/** @var  mixed $companyId */

?>

<div class="company-kitchen-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'kitchen_id')->dropDownList(AdminHelper::getKitchenList()) ?>

    <?php
    if(!$company){
        echo $form->field($model, 'company_id')->dropDownList(AdminHelper::getCompanyList());
    } else {
        echo   $form->field($model, 'company_id',
            ['options' => ['value'=> $companyId] ])->hiddenInput(['value'=> $companyId])->label(false);
    }


    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
