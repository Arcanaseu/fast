<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\SubscribersProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscribers Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribers-product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subscribers Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'subscriber_id',
            'target_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
