<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubscribersProduct */

$this->title = 'Create Subscribers Product';
$this->params['breadcrumbs'][] = ['label' => 'Subscribers Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribers-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
