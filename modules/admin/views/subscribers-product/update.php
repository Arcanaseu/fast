<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubscribersProduct */

$this->title = 'Update Subscribers Product: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Subscribers Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subscribers-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
