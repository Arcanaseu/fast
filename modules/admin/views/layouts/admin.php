<?php
use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>

    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => 'Beer Map',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        /** @var User $user */
        $user = User::findOne(Yii::$app->user->id);
        $nn = ($user instanceof User)?$user->name.' '.$user->surname:'';
        $name = (Yii::$app->user->identity->username)? Yii::$app->user->identity->username:$nn;

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'Logout (' . $name . ')',
                    'url' => ['/auth/default/logout'],
                    'linkOptions' => ['data-method' => 'post']]


            ],
        ]);

        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="index-container-left">
                <?php
                /** @var \app\models\User $user */
                $user = \app\models\User::findOne(Yii::$app->user->id);
                $items = [
                    ['label' => 'User profile', 'url' => ['/admin/']],
                    ['label' => 'Friends', 'url' => ['/admin/admin-friends/']],

                ];
                if(Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'admin')){
                    $items[] = ['label' => 'Company', 'url' => ['/admin/place-edit/index']];
                }
                if(Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'developer')){
                    $items[] = ['label' => 'API doc', 'url' => ['/admin/api-doc']];
                    $items[] = ['label' => 'Role', 'url' => ['/permit/access/role']];
                    $items[] = ['label' => 'Permission', 'url' => ['/permit/access/permission']];
                }
                if(Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'superAdmin')){
                    $items[] = ['label' => 'Admin panel', 'url' => ['/admin/admin-panel']];
                    $items[] = ['label' => 'Full Admin panel', 'url' => ['/admin/admin-full-panel']];
                }

                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-left'],
                    'items' => $items,
                ]);
                ?>
                <div class="index-menu-avatar">
                    <?php if($user->avatar0): ?>
                    <img src="/<?=$user->avatar0->path; ?>" width="70" height="70">
                    <?php endif; ?>
                </div>
            </div>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Fast <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>