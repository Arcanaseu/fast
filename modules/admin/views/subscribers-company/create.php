<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubscribersCompany */

$this->title = 'Create Subscribers Company';
$this->params['breadcrumbs'][] = ['label' => 'Subscribers Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribers-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
