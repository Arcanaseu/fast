<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuImage */

$this->title = 'Create Menu Image';
$this->params['breadcrumbs'][] = ['label' => 'Menu Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
