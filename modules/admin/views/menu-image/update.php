<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuImage */

$this->title = 'Update Menu Image: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Menu Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="menu-image-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
