<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;

?>
<script>
    var backRoute = 'admin-panel/index'
</script>
<div ng-app="">

    <div class="index-container-right">
        <?php if(Yii::$app->session->hasFlash('not_deleted')): ?>
        <div class="alert alert-danger" role="alert">
            <?= Yii::$app->session->getFlash('not_deleted') ?>
        </div>
        <?php endif; ?>
        <!-- Company -->
        <br>
        <center>
            <div style="font-size: large">
                Company&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeCompany">
            </div>
        </center>

        <br>
        <div ng-show="changeCompany">
            <?= $this->render('@app/modules/admin/form/company', [
                'model' => new \app\models\Company(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-company" >
                <a href="/admin/place/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>

                <?php /** @var \app\models\Company[] $companys */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Opened</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Site</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Avatar</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Company $company */  ?>
                    <?php foreach($companys as $company): ?>
                        <tr>
                            <td><?=$company->title; ?></td>
                            <td><?=$company->description; ?></td>
                            <td><?=$company->opened; ?></td>
                            <td><?=$company->site; ?></td>
                            {
                            <td><img src="/<?=$company->avatar0->path; ?>" width="100" height="70"></td>

                            <td>
                                <a href='/admin/place-edit/?placeId=<?=$company->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$company->id; ?>&class=<?=get_class($company); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($company); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorCompany */
            echo LinkPager::widget([
                'pagination' => $paginatorCompany,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- User -->
        <br>
        <center>
            <div style="font-size: large">
                User&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeUser">
            </div>
        </center>

        <br>
        <div ng-show="changeUser">
            <?= $this->render('@app/modules/admin/form/user', [
                'model' => new \app\models\User(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-user" >
                <a href="/admin/user/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\User[] $users */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Name</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Surname</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Email</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Phone</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Avatar</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\User $user */  ?>
                    <?php foreach($users as $user): ?>
                        <tr>
                            <td><?=$user->name; ?></td>
                            <td><?=$user->surname; ?></td>
                            <td><?=$user->email; ?></td>
                            <td><?=($user->phone0 instanceof \app\models\Phone)?$user->phone0->number:'' ; ?></td>
                            <?php
                            if($user->avatar0 && $user->avatar0 instanceof \app\models\Image){
                                $src = $user->avatar0->path;
                            }else{
                                $src = 'image/def.jpg';
                            }
                            ?>
                            <td><img src="/<?=$src; ?>" width="100" height="70"></td>

                            <td>
                                <a href='/admin/?id=<?=$user->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$user->id; ?>&class=<?=get_class($user); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($user); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorUser */
            echo LinkPager::widget([
                'pagination' => $paginatorUser,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Group -->
        <br>
        <center>
            <div style="font-size: large">
                Group&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeGroup">
            </div>
        </center>

        <br>
        <div ng-show="changeGroup">
            <?= $this->render('@app/modules/admin/form/group', [
                'model' => new \app\models\Group(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-group" >
                <a href="/admin/group/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\Group[] $groups */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Group $group */  ?>
                    <?php foreach($groups as $group): ?>
                        <tr>
                            <td><?=$group->title; ?></td>
                            <td><?=$group->description; ?></td>

                            <td>
                                <a href='/admin/group/update/?id=<?=$group->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/group/view/?id=<?=$group->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$group->id; ?>&class=<?=get_class($group); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($group); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorGroup */
            echo LinkPager::widget([
                'pagination' => $paginatorGroup,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Events -->
        <br>
        <center>
            <div style="font-size: large">
                Events&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeEvents">
            </div>
        </center>

        <br>
        <div ng-show="changeEvents">
            <?= $this->render('@app/modules/admin/form/events', [
                'model' => new \app\models\Events(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-events" >
                <a href="/admin/events/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\Events[] $eventss */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-name"  style="color: #ffffff"><b>Type</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Events $events */  ?>
                    <?php foreach($eventss as $events): ?>
                        <tr>
                            <td><?=$events->title; ?></td>
                            <td><?=$events->description; ?></td>
                            <td><?=$events->type; ?></td>
                            <td>
                                <a href='/admin/events/update/?id=<?=$events->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/events/view/?id=<?=$events->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$events->id; ?>&class=<?=get_class($events); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($events); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorEvents */
            echo LinkPager::widget([
                'pagination' => $paginatorEvents,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- City -->
        <br>
        <center>
            <div style="font-size: large">
                City&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeCity">
            </div>
        </center>

        <br>
        <div ng-show="changeCity">
            <?= $this->render('@app/modules/admin/form/city', [
                'model' => new \app\models\City(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-city" >
                <a href="/admin/city/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\City[] $citys */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>City</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\City $city */  ?>
                    <?php foreach($citys as $city): ?>
                        <tr>
                            <td><?=$city->city ?></td>
                            <td>
                                <a href='/admin/city/update/?id=<?=$city->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/city/view/?id=<?=$city->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$city->id; ?>&class=<?=get_class($city); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($city); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorCity */
            echo LinkPager::widget([
                'pagination' => $paginatorCity,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Country -->
        <br>
        <center>
            <div style="font-size: large">
                Country&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeCountry">
            </div>
        </center>

        <br>
        <div ng-show="changeCountry">
            <?= $this->render('@app/modules/admin/form/country', [
                'model' => new \app\models\Country(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-country" >
                <a href="/admin/country/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\Country[] $countrys */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Currency</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Country $country */  ?>
                    <?php foreach($countrys as $country): ?>
                        <tr>
                            <td><?=$country->title ?></td>
                            <td><?=$country->description ?></td>
                            <td><?=($country->currency)?$country->currency->title:'' ?></td>
                            <td>
                                <a href='/admin/country/update/?id=<?=$country->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/country/view/?id=<?=$country->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$country->id; ?>&class=<?=get_class($country); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($country); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorCountry */
            echo LinkPager::widget([
                'pagination' => $paginatorCountry,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Currency -->
        <br>
        <center>
            <div style="font-size: large">
                Currency&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeCurrency">
            </div>
        </center>

        <br>
        <div ng-show="changeCurrency">
            <?= $this->render('@app/modules/admin/form/currency', [
                'model' => new \app\models\Currency(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-currency" >
                <a href="/admin/currency/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\Currency[] $currencys */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Currency $currency */  ?>
                    <?php foreach($currencys as $currency): ?>
                        <tr>
                            <td><?=$currency->title ?></td>
                            <td>
                                <a href='/admin/admin-panel/delete?id=<?=$currency->id; ?>&class=<?=get_class($currency); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($currency); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorCurrency */
            echo LinkPager::widget([
                'pagination' => $paginatorCurrency,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Metro -->
        <br>
        <center>
            <div style="font-size: large">
                Metro&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeMetro">
            </div>
        </center>

        <br>
        <div ng-show="changeMetro">
            <?= $this->render('@app/modules/admin/form/metro', [
                'model' => new \app\models\Metro(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-metro" >
                <a href="/admin/metro/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\Metro[] $metros */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Metro $m */  ?>
                    <?php foreach($metros as $m): ?>
                        <tr>
                            <td><?=$m->metro ?></td>
                            <td>
                                <a href='/admin/metro/update/?id=<?=$m->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/metro/view/?id=<?=$m->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$m->id; ?>&class=<?=get_class($m); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($m); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorMetro */
            echo LinkPager::widget([
                'pagination' => $paginatorMetro,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Meta -->
        <br>
        <center>
            <div style="font-size: large">
                Meta&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeMeta">
            </div>
        </center>

        <br>
        <div ng-show="changeMeta">
            <div id="add-meta" >
                <a href="/admin/meta/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\Meta[] $metas */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Key</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Meta $m */  ?>
                    <?php foreach($metas as $m): ?>
                        <tr>
                            <td><?=$m->title ?></td>
                            <td><?=$m->key ?></td>
                            <td><?=$m->description ?></td>
                            <td>
                                <a href='/admin/meta/update/?id=<?=$m->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/meta/view/?id=<?=$m->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$m->id; ?>&class=<?=get_class($m); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($m); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorMeta */
            echo LinkPager::widget([
                'pagination' => $paginatorMeta,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Phone -->
<!--        <br>-->
<!--        <center>-->
<!--            <div style="font-size: large">-->
<!--                Phone&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changePhone">-->
<!--            </div>-->
<!--        </center>-->
<!---->
<!--        <br>-->
<!--        <div ng-show="changePhone">-->
<!--            <div id="add-phone" >-->
<!--                <a href="/admin/phone/create/?route=admin-panel/index">-->
<!--                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>-->
<!--                </a>-->
<!--            </div>-->
<!---->
<!--            <br>-->
<!--            <br>-->
<!--            <br>-->
<!--            <div>-->
<!--                --><?php ///** @var \app\models\Phone[] $phones */  ?>
<!--                <table class="table table-striped table-bordered table-condensed">-->
<!--                    <thead style="background-color: #4f4f4f">-->
<!--                    <tr>-->
<!--                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Country</b></div></th>-->
<!--                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Code</b></div></th>-->
<!--                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Number</b></div></th>-->
<!--                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    --><?php ///** @var \app\models\Phone $p */  ?>
<!--                    --><?php //foreach($phones as $p): ?>
<!--                        <tr>-->
<!--                            <td>--><?//=$p->country->title ?><!--</td>-->
<!--                            <td>--><?//=$p->country->phone_code ?><!--</td>-->
<!--                            <td>--><?//=$p->number ?><!--</td>-->
<!--                            <td>-->
<!--                                <a href='/admin/phone/update/?id=--><?//=$p->id; ?><!--&route=admin-panel/index'>-->
<!--                                    <span class='glyphicon glyphicon-pencil' ></span>-->
<!--                                </a>-->
<!--                                <a href='/admin/phone/view/?id=--><?//=$p->id; ?><!--&route=admin-panel/index'>-->
<!--                                    <span class='glyphicon glyphicon-eye-open' ></span>-->
<!--                                </a>-->
<!--                                <a href='/admin/admin-panel/delete?id=--><?//=$p->id; ?><!--&class=--><?//=get_class($p); ?><!--'>-->
<!--                                    <span class="glyphicon glyphicon-trash"></span>-->
<!--                                </a>-->
<!--                            </td>-->
<!--                        </tr>-->
<!--                    --><?php //endforeach; ?>
<!--                    --><?php //unset ($p); ?>
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->
<!--            --><?php
//            /** @var mixed $paginatorPhone */
//            echo LinkPager::widget([
//                'pagination' => $paginatorPhone,
//                //'registerLinkTags' => true
//            ]);
//            ?>
<!--        </div>-->
<!--        <br>-->
<!--        <hr>-->

        <!-- Product -->
        <br>
        <center>
            <div style="font-size: large">
                Product&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProduct">
            </div>
        </center>

        <br>
        <div ng-show="changeProduct">
            <?= $this->render('@app/modules/admin/form/product', [
                'model' => new \app\models\Product(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product" >
                <a href="/admin/product/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\Product[] $products */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Avatar</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\Product $p */  ?>
                    <?php foreach($products as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td><img src="/<?=$p->avatar0->path; ?>" width="100" height="70"></td>
                            <td>
                                <a href='/admin/product/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProduct */
            echo LinkPager::widget([
                'pagination' => $paginatorProduct,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Product Brand -->
        <br>
        <center>
            <div style="font-size: large">
                ProductBrand&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProductBrand">
            </div>
        </center>

        <br>
        <div ng-show="changeProductBrand">
            <?= $this->render('@app/modules/admin/form/product-brand', [
                'model' => new \app\models\ProductBrand(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product-brand" >
                <a href="/admin/product-brand/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\ProductBrand[] $productbrands */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\ProductBrand $p */  ?>
                    <?php foreach($productbrands as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td>
                                <a href='/admin/product-brand/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product-brand/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProductBrand */
            echo LinkPager::widget([
                'pagination' => $paginatorProductBrand,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Product Category -->
        <br>
        <center>
            <div style="font-size: large">
                ProductCategory&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProductCategory">
            </div>
        </center>

        <br>
        <div ng-show="changeProductCategory">
            <?= $this->render('@app/modules/admin/form/product-category', [
                'model' => new \app\models\ProductCategory(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product-category" >
                <a href="/admin/product-category/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\ProductCategory[] $productcategorys */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\ProductCategory $p */  ?>
                    <?php foreach($productcategorys as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td>
                                <a href='/admin/product-category/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product-category/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProductCategory */
            echo LinkPager::widget([
                'pagination' => $paginatorProductCategory,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Product Type -->
        <br>
        <center>
            <div style="font-size: large">
                ProductType&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProductType">
            </div>
        </center>

        <br>
        <div ng-show="changeProductType">
            <?= $this->render('@app/modules/admin/form/product-type', [
                'model' => new \app\models\ProductType(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product-category" >
                <a href="/admin/product-type/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\ProductType[] $producttypes */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\ProductType $p */  ?>
                    <?php foreach($producttypes as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td>
                                <a href='/admin/product-type/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product-type/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProductType */
            echo LinkPager::widget([
                'pagination' => $paginatorProductType,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Product Packing -->
        <br>
        <center>
            <div style="font-size: large">
                ProductPacking&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProductPacking">
            </div>
        </center>

        <br>
        <div ng-show="changeProductPacking">
            <?= $this->render('@app/modules/admin/form/product-packing', [
                'model' => new \app\models\ProductPacking(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product-packing" >
                <a href="/admin/product-packing/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\ProductPacking[] $productpackings */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\ProductPacking $p */  ?>
                    <?php foreach($productpackings as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td>
                                <a href='/admin/product-packing/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product-packing/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProductPacking */
            echo LinkPager::widget([
                'pagination' => $paginatorProductPacking,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Product Sort -->
        <br>
        <center>
            <div style="font-size: large">
                ProductSort&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProductSort">
            </div>
        </center>

        <br>
        <div ng-show="changeProductSort">
            <?= $this->render('@app/modules/admin/form/product-sort', [
                'model' => new \app\models\ProductSort(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product-sort" >
                <a href="/admin/product-sort/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\ProductSort[] $productsorts */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Brand</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\ProductSort $p */  ?>
                    <?php foreach($productsorts as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td><?=($p->brand instanceof \app\models\ProductBrand)?$p->brand->title:'' ?></td>
                            <td>
                                <a href='/admin/product-sort/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product-sort/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProductSort */
            echo LinkPager::widget([
                'pagination' => $paginatorProductSort,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Product Fortress -->
        <br>
        <center>
            <div style="font-size: large">
                ProductFortress&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProductFortress">
            </div>
        </center>

        <br>
        <div ng-show="changeProductFortress">
            <?= $this->render('@app/modules/admin/form/product-fortress', [
                'model' => new \app\models\ProductFortress(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product-fortress" >
                <a href="/admin/product-fortress/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\ProductFortress[] $productfortresss */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\ProductFortress $p */  ?>
                    <?php foreach($productfortresss as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td>
                                <a href='/admin/product-fortress/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product-fortress/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProductFortress */
            echo LinkPager::widget([
                'pagination' => $paginatorProductFortress,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>
        <!-- Product Brewery -->
        <br>
        <center>
            <div style="font-size: large">
                ProductBrewery&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeProductBrewery">
            </div>
        </center>

        <br>
        <div ng-show="changeProductBrewery">
            <?= $this->render('@app/modules/admin/form/product-brewery', [
                'model' => new \app\models\ProductBrewery(['scenario' => 'search']),
            ]) ?>
            <br>
            <div id="add-product-brewery" >
                <a href="/admin/product-brewery/create/?route=admin-panel/index">
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white;margin-left: 25px;">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </a>
            </div>

            <br>
            <br>
            <br>
            <div>
                <?php /** @var \app\models\ProductBrewery[] $productbrewerys */  ?>
                <table class="table table-striped table-bordered table-condensed">
                    <thead style="background-color: #4f4f4f">
                    <tr>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Title</b></div></th>
                        <th><div class="table-banner-id"  style="color: #ffffff"><b>Description</b></div></th>
                        <th><div  style="color: red"><b><i>Controls</i></b></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \app\models\ProductBrewery $p */  ?>
                    <?php foreach($productbrewerys as $p): ?>
                        <tr>
                            <td><?=$p->title ?></td>
                            <td><?=$p->description ?></td>
                            <td>
                                <a href='/admin/product-brewery/update/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-pencil' ></span>
                                </a>
                                <a href='/admin/product-brewery/view/?id=<?=$p->id; ?>&route=admin-panel/index'>
                                    <span class='glyphicon glyphicon-eye-open' ></span>
                                </a>
                                <a href='/admin/admin-panel/delete?id=<?=$p->id; ?>&class=<?=get_class($p); ?>'>
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset ($p); ?>
                    </tbody>
                </table>
            </div>
            <?php
            /** @var mixed $paginatorProductBrewery */
            echo LinkPager::widget([
                'pagination' => $paginatorProductBrewery,
                //'registerLinkTags' => true
            ]);
            ?>
        </div>
        <br>
        <hr>

    </div>
</div>