<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $form yii\widgets\ActiveForm */
/** @var string $images */
/** @var string $route */
if(!isset($update))
    $update = false;
?>


<div class="group-form" ng-app="">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_open')->checkbox() ?>

    <?php //echo $form->field($model, 'created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if($update): ?>

        <br>
        <center>
            <div style="font-size: large">
                Image&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeImage">
            </div>
        </center>
        <br>
        <div ng-show="changeImage">
            <div>
                <div id="image-form-block" style="display: none;margin-bottom: 50px;">
                    <button id="close-add-image" class="btn" style="font-size: 24px;float:right; color: white">X</button>
                    <div style="padding-left: 400px">
                        <form action="/admin/default/image-create/" method="post" id="add-image-form" enctype="multipart/form-data">
                            <label>Image Name</label>
                            <br>
                            <input id='formTitle' type="text" name="title">
                            <br>
                            <label>Image Description</label>
                            <br>
                            <textarea id='formDesc'  name="description"></textarea>
                            <br>
                            <label>Image</label>
                            <input id='formFile' type="file" name="img">
                            <br>
                            <input id='formClass' type="hidden" name="class" value="Group">
                            <input id='formClassId' type="hidden" name="classId" value="<?=$model->id; ?>">
                            <input id='formBackRoute' type="hidden" name="backRoute" value="admin-panel/index">

                            <input type="submit" name="submitted" value="submitted" >
                        </form>

                    </div>
                </div>
                <div id="add-image" >
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </div>
            </div>

            <br><br>
            <div id="admin-image-container" style="width: 1000px;">
                <?=$images; ?>
            </div>
        </div>
        <br>
        <br>

    <?php endif; ?>

</div>
