<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/** @var mixed $route */
/** @var  $back  mixed */
/** @var string $images */


if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';



$this->title = 'Update Group: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'update' => true,
        'images' => $images,
        'back' => $back,
        'route' => $route,
    ]) ?>

</div>
