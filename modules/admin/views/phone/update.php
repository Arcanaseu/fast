<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Phone */
/** @var mixed $route */
/** @var  $back  mixed */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Update Phone: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = [
    'label' => $model->number,
    'url' => [
        'view',
        'id' => $model->id,
        'route' => $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="phone-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'route' => $route,
        'back' => $back,
        'update' => true,
    ]) ?>

</div>
