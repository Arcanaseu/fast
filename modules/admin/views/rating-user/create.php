<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RatingUser */

$this->title = 'Create Rating User';
$this->params['breadcrumbs'][] = ['label' => 'Rating Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
