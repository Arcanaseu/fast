<?php

use app\modules\admin\AdminHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
/** @var string $images */
/** @var string $route */
/** @var string $access */

$imgId = $model->avatar;
if(!isset($update))
    $update = false;
?>
<script>
    var imageField = '#user-image-drop';
    var imageTarget = '#user-create-avatar';
</script>
<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->dropDownList(AdminHelper::getCityList()) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?php if($update): ?>
        <label>
            <input type="checkbox" ng-model="change">   change the avatar
        </label>
    <?php else: ?>
        <span ng-init="change=true"></span>
    <?php endif; ?>
    <div ng-hide="change">
        <?php  echo   $form->field($model, 'avatar',
            ['options' => ['value'=> $imgId] ])->hiddenInput(['value'=> $imgId])->label(false); ?>
    </div>
    <div ng-show="change">
        <label>
            <input type="checkbox" ng-model="var"> <span ng-hide="var"> new image</span>
        </label>
        <span ng-hide="var">
            <div id="user-create-avatar" style="float: right;"></div>
            <br><br>
            <?= $form->field($model, 'avatar')
                ->dropDownList(AdminHelper::getImagesList(),  ['id' => 'user-image-drop']) ?>

        </span>
        <span ng-show="var">
            <?= $form->field($model, 'file')->fileInput() ?>
        </span>

    </div>
    <?php $selected = ($model->phone0 instanceof \app\models\Phone)?$model->phone0->country_id:null; ?>
    <?= Html::dropDownList('country', $selected, AdminHelper::getCountryList(),['class'=>'form-control']); ?>

    <?= $form->field($model, 'phone')->dropDownList(AdminHelper::getPhoneList()) ?>

    <?= $form->field($model, 'email')->input('email') ?>

    <?= $form->field($model, 'lat')->textInput() ?>

    <?= $form->field($model, 'lon')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


    <?php if($update): ?>
        <?php if(!Yii::$app->user->isGuest && Yii::$app->authManager->checkAccess(Yii::$app->user->id,'superUser')): ?>
        <br>
        <span style="font-size: 18px; font-weight: bolder;">Access Role - <?=$access; ?></span>
        <br>
        <br>
        <div id="update-access" >
            <a href="/permit/user/view/?id=<?=$model->id; ?>">
            <button class="btn btn-warning" style="font-size: 20px;float:left;">change permission</button>
            </a>
        </div>
        <br>
        <?php  endif; ?>
        <br>
        <center>
            <div style="font-size: large">
                Image&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ng-model="changeImage">
            </div>
        </center>
        <br>
        <div ng-show="changeImage">
            <div>
                <div id="image-form-block" style="display: none;margin-bottom: 50px;">
                    <button id="close-add-image" class="btn" style="font-size: 24px;float:right; color: white">X</button>
                    <div style="padding-left: 400px">
                        <form action="/admin/default/image-create/" method="post" id="add-image-form" enctype="multipart/form-data">
                            <label>Image Name</label>
                            <br>
                            <input id='formTitle' type="text" name="title">
                            <br>
                            <label>Image Description</label>
                            <br>
                            <textarea id='formDesc'  name="description"></textarea>
                            <br>
                            <label>Image</label>
                            <input id='formFile' type="file" name="img">
                            <br>
                            <input id='formClass' type="hidden" name="class" value="User">
                            <input id='formClassId' type="hidden" name="classId" value="<?=$model->id; ?>">
                            <input id='formBackRoute' type="hidden" name="backRoute" value="admin-panel/index">

                            <input type="submit" name="submitted" value="submitted" >
                        </form>

                    </div>
                </div>
                <div id="add-image" >
                    <button class="btn btn-info" style="font-size: 24px;float:right; color: white">&nbsp;&nbsp;+&nbsp;&nbsp;</button>
                </div>
            </div>

            <br><br>
            <div id="admin-image-container" style="width: 1000px;">
                <?=$images; ?>
            </div>
        </div>
        <br>
        <br>

    <?php endif; ?>

</div>
