<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

/** @var mixed $route */
/** @var  $back  mixed */
/** @var string $images */
if(!isset($route))
    $route = 'index';
if(!isset($back))
    $back = '';

$this->title = 'Create User';
$this->params['breadcrumbs'][] = [
    'label' => 'Back',
    'url' => [
        $route,
        'back' => $back,

    ]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create"  ng-app="">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'route' => $route,
        'back' => $back,
        'images' => '',
    ]) ?>

</div>
