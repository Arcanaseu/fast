<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductType */
/* @var $form ActiveForm */
?>
<div class="product-type-search-form">

    <?php $form = ActiveForm::begin([
        'id' => 'product-type-search-form',
        'enableClientValidation' => false,
        'options' => ['class' => 'form-inline'],
    ]); ?>

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'description') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- product-type -->
