<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Metro */
/* @var $form ActiveForm */
?>
<div class="metro-search-form">

    <?php $form = ActiveForm::begin([
        'id' => 'metro-search-form',
        'enableClientValidation' => false,
        'options' => ['class' => 'form-inline'],
    ]); ?>

        <?= $form->field($model, 'metro') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- metro -->
