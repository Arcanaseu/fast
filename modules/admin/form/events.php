<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form ActiveForm */
?>
<div class="events-search-form">

    <?php $form = ActiveForm::begin([
        'id' => 'events-search-form',
        'enableClientValidation' => false,
        'options' => ['class' => 'form-inline'],
    ]); ?>

    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'description') ?>
    <?= $form->field($model, 'type') ?>


    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- events -->
