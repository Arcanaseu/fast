<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\City */
/* @var $form ActiveForm */
?>
<div class="city-search-form">

    <?php $form = ActiveForm::begin([
        'id' => 'city-search-form',
        'enableClientValidation' => false,
        'options' => ['class' => 'form-inline'],
    ]); ?>

        <?= $form->field($model, 'city') ?>

    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- city -->
