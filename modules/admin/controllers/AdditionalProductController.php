<?php

namespace app\modules\admin\controllers;

use app\models\Product;
use Yii;
use app\models\AdditionalProduct;
use app\modules\admin\models\AdditionalProductSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdditionalProductController implements the CRUD actions for AdditionalProduct model.
 */
class AdditionalProductController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdditionalProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdditionalProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdditionalProduct model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        $product = Product::findOne($productId);
        return $this->render([
            'view', [
                'model' => $model,
                'id' => $model->id,
                'product' => $product,
                'route'   => $route,
                'productId' => $productId,
            ]
        ]);
    }

    /**
     * Creates a new AdditionalProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdditionalProduct();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        $product = Product::findOne($productId);
        if ($model->load(Yii::$app->request->post()) && @$model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'product' => $product,
                'route'   => $route,
                'productId' => $productId,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'product' => $product,
                'route'   => $route,
                'productId' => $productId,
            ]);
        }
    }

    /**
     * Updates an existing AdditionalProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        $product = Product::findOne($productId);
        if ($model->load(Yii::$app->request->post()) && @$model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'product' => $product,
                'route'   => $route,
                'productId' => $productId,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'product' => $product,
                'route'   => $route,
                'productId' => $productId,
            ]);
        }
    }

    /**
     * Deletes an existing AdditionalProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the AdditionalProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AdditionalProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdditionalProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
