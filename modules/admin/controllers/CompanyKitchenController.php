<?php

namespace app\modules\admin\controllers;

use app\models\Company;
use Yii;
use app\models\CompanyKitchen;
use app\modules\admin\models\CompanyKitchenSearch;
use app\modules\admin\controllers\AdminBaseController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyKitchenController implements the CRUD actions for CompanyKitchen model.
 */
class CompanyKitchenController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyKitchen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyKitchenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanyKitchen model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'companyAdmin', ['modelId' => $id])
        )) $this->goBack();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $company = Company::findOne($companyId);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route'   => $route,
            'companyId' => $companyId,
            'company' => $company,
        ]);
    }

    /**
     * Creates a new CompanyKitchen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyKitchen();


        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(
                Yii::$app->user->id,
                'companyAdmin',
                ['modelId' => $companyId, 'class' => 'Company'])
        )) $this->goBack();

        $company = Company::findOne($companyId);
        if ($model->load(Yii::$app->request->post()) && @$model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route'   => $route,
                'companyId' => $companyId,
                'company' => $company,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'route'   => $route,
                'companyId' => $companyId,
                'company' => $company,
            ]);
        }
    }

    /**
     * Updates an existing CompanyKitchen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'companyAdmin', ['modelId' => $id])
        )) $this->goBack();

        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $company = Company::findOne($companyId);
        if ($model->load(Yii::$app->request->post()) && @$model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route'   => $route,
                'companyId' => $companyId,
                'company' => $company,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'route'   => $route,
                'companyId' => $companyId,
                'company' => $company,
            ]);
        }
    }

    /**
     * Deletes an existing CompanyKitchen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'companyAdmin', ['modelId' => $id])
        )) $this->goBack();

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyKitchen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CompanyKitchen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyKitchen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
