<?php

namespace app\modules\admin\controllers;

use app\models\CompanyKitchen;
use app\models\ProductKitchen;
use Yii;
use app\models\Kitchen;
use app\modules\admin\models\KitchenSearch;
use app\modules\admin\controllers\AdminBaseController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KitchenController implements the CRUD actions for Kitchen model.
 */
class KitchenController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kitchen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KitchenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kitchen model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route' => $route,
            'companyId' => $companyId,
            'productId' => $productId,
        ]);
    }

    /**
     * Creates a new Kitchen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kitchen();
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                if($companyId != 0){
                    $ac = new CompanyKitchen();
                    $ac->company_id = $companyId;
                    $ac->kitchen_id = $model->id;
                    $ac->save();
                } elseif ($productId != 0){
                    $ap = new ProductKitchen();
                    $ap->product_id = $productId;
                    $ap->kitchen_id = $model->id;
                }
                return $this->redirect([
                    'view',
                    'id' => $model->id,
                    'route' => $route,
                    //'company' => $company,
                    'companyId' => $companyId,
                    'productId' => $productId,
                ]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            //'company' => $company,
            'companyId' => $companyId,
            'productId' => $productId,
        ]);

    }

    /**
     * Updates an existing Kitchen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'companyId' => $companyId,
                'productId' => $productId,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'route' => $route,
                'companyId' => $companyId,
                'productId' => $productId,
            ]);
        }
    }

    /**
     * Deletes an existing Kitchen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Kitchen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Kitchen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kitchen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
