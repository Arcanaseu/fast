<?php

namespace app\modules\admin\controllers;

use app\models\Company;
use app\models\User;
use Yii;
use yii\web\Controller;

class AdminBaseController extends Controller
{
    public $layout = 'main';

    protected function checkAccess($rule, $params = null)
    {
        if(\Yii::$app->user->isGuest)
            return false;
        else
            return  \Yii::$app->authManager->checkAccess(\Yii::$app->user->id, $rule, $params);
    }

    public static function getUsersCompany()
    {
        if(!Yii::$app->user->isGuest){
            $userId = \Yii::$app->user->id;
            if(\Yii::$app->authManager->checkAccess($userId, 'admin')){
                $user = User::findOne($userId);
                if($user instanceof User){
                    $company = $user->getCompany();
                    if($company instanceof Company){
                        return $company;
                    }
                }
            }

        }

        return null;
    }


}
