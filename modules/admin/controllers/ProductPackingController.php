<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\ProductPacking;
use app\modules\admin\models\ProductPackingSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductPackingController implements the CRUD actions for ProductPacking model.
 */
class ProductPackingController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductPacking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductPackingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductPacking model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route' => $route,
            'back'   => $back,
        ]);
    }

    /**
     * Creates a new ProductPacking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductPacking();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'back'   => $back,

            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'route' => $route,
                'back'   => $back,
            ]);
        }
    }

    /**
     * Updates an existing ProductPacking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'back'   => $back,

            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'route' => $route,
                'back'   => $back,
            ]);
        }
    }

    /**
     * Deletes an existing ProductPacking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the ProductPacking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ProductPacking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductPacking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
