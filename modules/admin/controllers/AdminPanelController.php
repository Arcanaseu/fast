<?php

namespace app\modules\admin\controllers;

use app\models\BaseModel;
use app\models\City;
use app\models\Company;
use app\models\Country;
use app\models\Currency;
use app\models\Group;
use app\models\MenuCategory;
use app\models\Meta;
use app\models\Metro;
use app\models\Phone;
use app\models\Product;
use app\models\ProductBrand;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;

class AdminPanelController extends AdminBaseController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ]
        ];
    }

    public $layout = 'admin';

    protected $display = [
        ['Company', 'title'],
        ['User', 'surname'],
        ['Group', 'title'],
        ['Events', 'title'],
        ['City', 'city'],
        ['Country', 'title'],
        ['Currency', 'title'],
        ['Metro', 'metro'],
        ['Meta', 'title'],
        ['Phone', 'number'],
        ['Product', 'title'],
        ['ProductBrand', 'title'],
        ['ProductCategory', 'title'],
        ['ProductType', 'title'],
        ['ProductPacking', 'title'],
        ['ProductSort', 'title'],
        ['ProductFortress', 'title'],
        ['ProductBrewery', 'title'],
    ];

    public function actionIndex()
    {

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);


        $res = [];
        foreach($this->display as $item){
            $pc = $this->getPaginated($item[0], $item[1]);
            $res = array_merge($res, $pc);
        }

        return $this->render('index', $res

        );

    }

    public function actionDelete(){

        if($this->checkAccess('superAdmin')){
            $params = \Yii::$app->getRequest()->getQueryParams();
            $id = isset($params['id'])?intval($params['id']):0;
            $class = isset($params['class'])?$params['class']:'';
            //$relId = isset($params['relId'])?intval($params['relId']):0;
            //$relClass = isset($params['relClass'])?$params['relClass']:'';
            /** @var ActiveRecord $class */
            $class = new $class();
            $model = $class::findOne($id);
            /** @var ActiveRecord $relClass */
            //$relClass = new $relClass();
            //$relModel = $relClass::findOne($relId);
            if($model instanceof ActiveRecord){

                try{
                    $model->delete();
                } catch(Exception $e){
                    Yii::$app->getSession()->setFlash('not_deleted', 'You can`t delete this model without removing related models');
                }

            }

            $this->redirect(['admin-panel/index']);




        }
    }

    protected function getPaginated($modelClass, $field){
        $modelName = 'app\models\\'.$modelClass;
        $prefix = strtolower($modelClass);
        /** @var BaseModel $model */
        $model = new $modelName();
        $query = $model::find();
        $post = Yii::$app->request->post();

        if(
            $post &&
            isset($post[$modelClass]) &&
            is_array($post[$modelClass]) &&
            count($post[$modelClass]) > 0
        ){
            $i = 0;
            foreach($model->attributes as $attrName => $value){
                if(array_key_exists($attrName, $post[$modelClass]) && $post[$modelClass][$attrName] != ''){
                    if($i == 0){
                        $query->where(['like', $attrName, $post[$modelClass][$attrName]]);
                    }else{
                        $query->andFilterWhere(['like', $attrName, $post[$modelClass][$attrName]]);
                    }

                    $i++;
                }
            }
            unset($attrName, $value);

        }


        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        $pages->pageSizeParam = 'per-page-'.$prefix;
        $pages->pageParam = 'page-'.$prefix;
        $paginator = 'paginator'.$modelClass;
        $pref = $prefix.'s';

        $res = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy($field)
            ->all();

        return [$pref => $res, $paginator => $pages];
    }

}
