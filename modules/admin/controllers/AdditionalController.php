<?php

namespace app\modules\admin\controllers;

use app\models\AdditionalCompany;
use app\models\AdditionalProduct;
use app\models\Company;
use app\models\Image;
use Yii;
use app\models\Additional;
use app\modules\admin\models\AdditionalSearch;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdditionalController implements the CRUD actions for Additional model.
 */
class AdditionalController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view'],
                        'roles' => [ 'admin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Additional models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdditionalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Additional model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {


        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route' => $route,
            'companyId' => $companyId,
            'productId' => $productId,
        ]);
    }

    /**
     * Creates a new Additional model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Additional();
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        //$company = Company::findOne($companyId);
        if(isset($_FILES['Additional']) && $_FILES['Additional']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Additional Image  - '.$model->title;
                $image->description = $model->description;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->image_id = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {
                    if($companyId != 0){
                        $ac = new AdditionalCompany();
                        $ac->company_id = $companyId;
                        $ac->additional_id = $model->id;
                        $ac->save();
                    } elseif ($productId != 0){
                        $ap = new AdditionalProduct();
                        $ap->product_id = $productId;
                        $ap->additional_id = $model->id;
                    }
                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        //'company' => $company,
                        'companyId' => $companyId,
                        'productId' => $productId,
                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post())) {

                if ($model->save()) {
                    if($companyId != 0){
                        $ac = new AdditionalCompany();
                        $ac->company_id = $companyId;
                        $ac->additional_id = $model->id;
                        $ac->save();
                    } elseif ($productId != 0){
                        $ap = new AdditionalProduct();
                        $ap->product_id = $productId;
                        $ap->additional_id = $model->id;
                        $ap->save();
                    }
                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        //'company' => $company,
                        'companyId' => $companyId,
                        'productId' => $productId,
                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            //'company' => $company,
            'companyId' => $companyId,
            'productId' => $productId,
        ]);

    }

    /**
     * Updates an existing Additional model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);

        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        $company = Company::findOne($companyId);
        if(isset($_FILES['Additional']) && $_FILES['Additional']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Additional Image  - '.$model->title;
                $image->description = $model->description;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->image_id = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->validate() && $model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'company' => $company,
                        'companyId' => $companyId,
                        'productId' => $productId,
                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect([
                    'view',
                    'id' => $model->id,
                    'route' => $route,
                    'company' => $company,
                    'companyId' => $companyId,
                    'productId' => $productId,
                ]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'route' => $route,
            'company' => $company,
            'companyId' => $companyId,
            'productId' => $productId,
        ]);
    }

    /**
     * Deletes an existing Additional model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /** @var Additional $model */
        $model = $this->findModel($id);
        $model->delete();
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Additional model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Additional the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Additional::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
