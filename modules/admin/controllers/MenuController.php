<?php

namespace app\modules\admin\controllers;

use app\models\Company;
use app\models\User;
use app\modules\admin\AdminHelper;
use Yii;
use app\models\Menu;
use app\modules\admin\models\MenuSearch;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'menuAdmin', ['modelId' => $id])
        )) $this->goBack();
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);
        $model = $this->findModel($id);


        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId;
        $images = AdminHelper::getImages('Menu', $model->id, $url);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route' => $route,
            'companyId' => $companyId,
            'images' => $images,
            'back'  => $back,
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):null;
        $company = Company::findOne($companyId);
        if($company instanceof Company)
            $modelId = $company->menu->id;
        else {
            /** @var User $user */
            $user  = User::findOne(Yii::$app->user->id);
            $company = $user->getCompany();
            $modelId = $company->menu->id;
        }
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(
                Yii::$app->user->id,
                'menuAdmin',
                ['modelId' => $modelId, 'class' => 'Menu'])
        )) $this->goBack();

        $back = (isset($params['back']))?$params['back']:'';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'companyId' => $companyId,
                'back'   => $back,

            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            'companyId' => $companyId,
            'back'   => $back,

        ]);
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'menuAdmin', ['modelId' => $id])
        )) $this->goBack();
        $model = $this->findModel($id);

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):null;
        $url = Yii::$app->controller->id.'/view/?id='.$id.'&companyId='.$companyId;
        $back = (isset($params['back']))?$params['back']:'';
        $images = AdminHelper::getImages('Menu', $model->id, $url);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'companyId' => $companyId,
                'images' => $images,
                'back'   => $back,
            ]);
        }
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId;
        $images = AdminHelper::getImages('Menu', $model->id, $url);
        return $this->render('update', [
            'model' => $model,
            'route' => $route,
            'companyId' => $companyId,
            'images' => $images,
            'back'   => $back,
        ]);
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'menuAdmin', ['modelId' => $id])
        )) $this->goBack();
        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
