<?php

namespace app\modules\admin\controllers;

use app\models\Image;
use app\models\Menu;
use app\models\MenuCategory;
use app\models\MenuCategoryItem;
use app\models\User;
use app\modules\admin\AdminHelper;
use Yii;
use app\models\Company;
use app\modules\admin\models\CompanySearch;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class PlaceController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [ 'update', 'view'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create','index', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        if(!Yii::$app->authManager->checkAccess($user->id, 'superAdmin')){
            $company = $user->getCompany();
            $modelId = null;
            if($company  instanceof Company)
                $modelId = $company->id;
            else
                $this->goBack();
            if(!(
                !Yii::$app->user->isGuest &&
                Yii::$app->authManager->checkAccess(
                    Yii::$app->user->id,
                    'companyAdmin',
                    ['modelId' => $modelId, 'class' => 'Company'])
            )) $this->goBack();
        }


        $model = $this->findModel($id);
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id;
        $images = AdminHelper::getImages('Company', $model->id, $url);

        return $this->render('view', [
            'model' => $model,
            'route' => $route,
            'images' => $images,
            'back'   => $back,
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Company();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);



        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        if(isset($_FILES['Company']) && $_FILES['Company']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
//                    $model->lat = $coordinates[0];
//                    $model->lon = $coordinates[1];
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Company Image  - '.$model->title;
                $image->description = $model->description;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {
                    $menu = new Menu();
                    $menu->company_id = $model->id;
                    $menu->save();
                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'back'   => $back,

                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
//                    $model->lat = $coordinates[0];
//                    $model->lon = $coordinates[1];
                }

                if ($model->save()) {
                    $menu = new Menu();
                    $menu->company_id = $model->id;
                    $menu->save();

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'back'   => $back,

                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            'back'   => $back,
        ]);

    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        if(!Yii::$app->authManager->checkAccess($user->id, 'superAdmin')){
            $company = $user->getCompany();
            $modelId = null;
            if($company  instanceof Company)
                $modelId = $company->id;
            else
                $this->goBack();
            if(!(
                !Yii::$app->user->isGuest &&
                Yii::$app->authManager->checkAccess(
                    Yii::$app->user->id,
                    'companyAdmin',
                    ['modelId' => $modelId, 'class' => 'Company'])
            )) $this->goBack();
        }

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $model = $this->findModel($id);


        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/view/?id='.$id;
        $images = AdminHelper::getImages('Company', $model->id, $url);
        if(isset($_FILES['Company']) && $_FILES['Company']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
//                    $model->lat = $coordinates[0];
//                    $model->lon = $coordinates[1];
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Company Image  - '.$model->title;
                $image->description = $model->description;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'images' => $images,
                        'back'   => $back,

                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
//                    $model->lat = $coordinates[0];
//                    $model->lon = $coordinates[1];
                }

                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'images' => $images,
                        'back'   => $back,

                    ]);
                }
            }
        }

        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id;
        $images = AdminHelper::getImages('Company', $model->id, $url);
        return $this->render('update', [
            'model' => $model,
            'route' => $route,
            'images' => $images,
            'back'   => $back,
        ]);




    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->checkAccess('superAdmin')){
            $model = $this->findModel($id);
            $menu = $model->menu;
            if($model->delete()){
                $categories = $menu->menuCategories;
                /** @var MenuCategory $cat */
                foreach($categories as $cat){
                    /** @var MenuCategoryItem $item */
                    foreach($cat->menuCategoryItems as $item){
                        $item->delete();
                    }
                    $cat->delete();
                }
                $menu->delete();
            }
        }



        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
