<?php

namespace app\modules\admin\controllers;

use app\models\Company;
use app\models\MenuCategory;
use app\models\Product;
use Yii;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;

class AdminFullPanelController extends AdminBaseController
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
        ];
    }

    public $layout = 'admin';


    public function actionIndex()
    {
        $class = 'Product';
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);


            return $this->render('index');



    }

}
