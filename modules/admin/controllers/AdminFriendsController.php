<?php

namespace app\modules\admin\controllers;


use app\models\Friends;
use app\models\FriendsRequestForm;
use app\models\HandleRequestForm;
use app\models\PeopleSearchForm;
use app\models\Request;
use app\models\UnfriendForm;
use app\models\User;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;

class AdminFriendsController extends AdminBaseController
{

    public $layout = 'admin';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'send', 'unfriend','handle'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','send', 'unfriend','handle'],
                        'roles' => [ '@'],
                    ],

                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $model = new PeopleSearchForm();
        $ids = static::getFriendsId();
        $friends = User::find()->where(['in', 'id',$ids]);
        $countFriends = clone $friends;
        $friends = $friends->all();
        $pages = new Pagination(['totalCount' => $countFriends->count(), 'pageSize' => 10]);
        $pages->pageSizeParam = 'per-page-friends';
        $pages->pageParam = 'page-friends';
        $requests = Request::find()->where(['acceptor_id' => Yii::$app->user->id])->all();


        if ($model->load(Yii::$app->request->post())) {
            $query = $model->getQuery();
            $countQuery = clone $query;
            $userPages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
            $userPages->pageSizeParam = 'per-page-user';
            $userPages->pageParam = 'page-user';
            $users = $query->all();
            return $this->render('index',
                [
                    'model'        => $model,
                    'dataProvider' => $model->search(),
                    'users'        => $users,
                    'friends'      => $friends,
                    'userPages'    => $userPages,
                    'pages'        => $pages,
                    'requests'     => $requests,


                ]
            );
        }
        return $this->render('index',
            [
                'model'        => $model,
                'dataProvider' => false,
                'userPages'    => false,
                'friends'      => $friends,
                'pages'        => $pages,
                'requests'     => $requests,

            ]
        );
    }

    public function actionSend(){
        $model = new FriendsRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->send();

        }
        $this->redirect('/admin/admin-friends');

    }

    public function actionUnfriend(){
        $model = new UnfriendForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->unfriend();

        }
        $this->redirect('/admin/admin-friends');
    }

    public function actionHandle(){
        $model = new HandleRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->handle();

        }
        $this->redirect('/admin/admin-friends');
    }


    public static function getFriendsId(){
        if(Yii::$app->user->isGuest){
            return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
        }

        $ids = [];
        $friends  = Friends::find()
            ->where(['initiator_id' => Yii::$app->user->id])
            ->orFilterWhere(['acceptor_id' => Yii::$app->user->id])
            ->all();
        /** @var Friends $friend */
        foreach($friends as $friend){
            if($friend->initiator_id != Yii::$app->user->id){
                $ids[] = $friend->initiator_id;
            }elseif($friend->acceptor_id != Yii::$app->user->id){
                $ids[] = $friend->acceptor_id;
            }
        }
        return $ids;
    }

}
