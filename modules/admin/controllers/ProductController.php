<?php

namespace app\modules\admin\controllers;

use app\models\Image;
use app\models\MenuCategoryItem;
use app\modules\admin\AdminHelper;
use Yii;
use app\models\Product;
use app\modules\admin\models\ProductSearch;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $categoryId = (isset($params['categoryId']))?intval($params['categoryId']):0;
        $back = (isset($params['back']))?$params['back']:null;
        $backRoute = (isset($params['backRoute']))?$params['backRoute']:'index';
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId.'&categoryId='.$categoryId.'&back='.$back.'&backroute='.$backRoute;
        $images = AdminHelper::getImages('Product', $model->id, $url);
        return $this->render('view', [
            'model' => $model,
            'id' => $model->id,
            'backRoute' => $backRoute,
            'route' => $route,
            'companyId' => $companyId,
            'categoryId' => $categoryId,
            'back' => $back,
            'productId' => $model->id,
            'images' => $images,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $categoryId = (isset($params['categoryId']))?intval($params['categoryId']):0;
        $back = (isset($params['back']))?$params['back']:null;
        $backRoute = (isset($params['backRoute']))?$params['backRoute']:'index';
        if(isset($_FILES['Product']) && $_FILES['Product']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Product Avatar Image  - '.$model->title;
                $image->description = $model->description;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {

                    return $this->redirect([
                        $backRoute,
                        'route' => $route,
                        'companyId' => $companyId,
                        'categoryId' => $categoryId,
                        'back' => $back,
                        'productId' => $model->id
                    ]);
                }
            }
        } elseif ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                $backRoute,
                'route' => $route,
                'companyId' => $companyId,
                'categoryId' => $categoryId,
                'back' => $back,
                'productId' => $model->id
            ]);
        }

            return $this->render('create', [
                'model' => $model,
                'backRoute' => $backRoute,
                'route' => $route,
                'companyId' => $companyId,
                'categoryId' => $categoryId,
                'back' => $back,


            ]);

    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);

        $model = $this->findModel($id);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $categoryId = (isset($params['categoryId']))?intval($params['categoryId']):0;
        $back = (isset($params['back']))?$params['back']:null;
        $backRoute = (isset($params['backRoute']))?$params['backRoute']:'index';
        $url = Yii::$app->controller->id.'/view/?id='.$id.'&companyId='.$companyId.'&categoryId='.$categoryId.'&back='.$back.'&backroute='.$backRoute;
        $images = AdminHelper::getImages('Product', $model->id, $url);
        if(isset($_FILES['Product']) && $_FILES['Product']['size']['file'] > 0){

            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Product Avatar Image  - '.$model->title;
                $image->description = $model->description;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                if($image->save()){
                    /*
                    $oldImage = Image::findOne($model->avatar);
                    $path = $oldImage->path;
                    if($oldImage->delete())
                        @unlink($path);
                    */
                    $model->avatar = $image->id;
                }


                $model->validate();
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'backRoute' => $backRoute,
                        'route' => $route,
                        'companyId' => $companyId,
                        'categoryId' => $categoryId,
                        'back' => $back,
                        'productId' => $model->id,
                        'images' => $images
                    ]);
                }
            }
        } elseif ($post = Yii::$app->request->post()) {
            //$avatar = $model->avatar;
            if($model->load($post)){
                /*
                $newImage = Image::findOne($model->avatar);
                if($newImage && $avatar != $model->avatar){
                    /** @var Image $oldImage *//*
                    $oldImage = Image::findOne($avatar);
                    $path = $oldImage->path;
                    if($oldImage->delete())
                        @unlink($path);
                }
                */
                if($model->save())
                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'backRoute' => $backRoute,
                        'route' => $route,
                        'companyId' => $companyId,
                        'categoryId' => $categoryId,
                        'back' => $back,
                        'productId' => $model->id,
                        'images' => $images,
                    ]);
            }

        }
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId.'&categoryId='.$categoryId.'&back='.$back.'&backroute='.$backRoute;
        $images = AdminHelper::getImages('Product', $model->id, $url);
        return $this->render('update', [
            'model' => $model,
            'id' => $model->id,
            'backRoute' => $backRoute,
            'route' => $route,
            'companyId' => $companyId,
            'categoryId' => $categoryId,
            'back' => $back,
            'images' => $images,


        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //todo only for administrators
        $model = $this->findModel($id);
        $mcis = $model->menuCategoryItems;
        if($model->delete()){
            /** @var MenuCategoryItem $mci */
            foreach($mcis as $mci){
                $mci->delete();
            }
        }

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
