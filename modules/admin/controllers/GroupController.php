<?php

namespace app\modules\admin\controllers;

use app\modules\admin\AdminHelper;
use Yii;
use app\models\Group;
use app\modules\admin\models\GroupSearch;
use app\modules\admin\controllers\AdminBaseController;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'groupAdmin', ['modelId' => $id])
        )) $this->goBack();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);
        $model = $this->findModel($id);


        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $userId = (isset($params['userId']))?intval($params['userId']):0;
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId.'&userId='.$userId;
        $images = AdminHelper::getImages('Group', $model->id, $url);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route' => $route,
            'companyId' => $companyId,
            'userId' => $userId,
            'images' => $images,
            'back'   => $back,
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Group();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):null;
        $userId = (isset($params['userId']))?intval($params['userId']):0;
        $back = (isset($params['back']))?$params['back']:'';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'companyId' => $companyId,
                'userId' => $userId,
                'back'   => $back,

            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            'companyId' => $companyId,
            'userId' => $userId,
            'back'   => $back,

        ]);
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'groupAdmin', ['modelId' => $id])
        )) $this->goBack();


        $model = $this->findModel($id);

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):null;
        $userId = (isset($params['userId']))?intval($params['userId']):0;
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/view/?id='.$id.'&companyId='.$companyId.'&userId='.$userId;
        $images = AdminHelper::getImages('Group', $model->id, $url);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'companyId' => $companyId,
                'images' => $images,
                'userId' => $userId,
                'back'   => $back,
            ]);
        }
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId.'&userId='.$userId;
        $images = AdminHelper::getImages('Group', $model->id, $url);
        return $this->render('update', [
            'model' => $model,
            'route' => $route,
            'companyId' => $companyId,
            'images' => $images,
            'userId' => $userId,
            'back'   => $back,
        ]);
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'groupAdmin', ['modelId' => $id])
        )) $this->goBack();

        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
