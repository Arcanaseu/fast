<?php

namespace app\modules\admin\controllers;

use app\models\Company;
use app\models\MenuCategory;
use app\models\Product;
use app\models\User;
use Yii;
use app\models\MenuCategoryItem;
use app\modules\admin\models\MenuCategoryItemSearch;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuCategoryItemController implements the CRUD actions for MenuCategoryItem model.
 */
class MenuCategoryItemController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MenuCategoryItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuCategoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MenuCategoryItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        $company = $user->getCompany();
        $modelId = $company->menu->id;
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(
                Yii::$app->user->id,
                'menuAdmin',
                ['modelId' => $modelId, 'class' => 'Menu'])
        )) $this->goBack();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MenuCategoryItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $categoryId = (isset($params['categoryId']))?intval($params['categoryId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;

        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        if(isset($params['companyId']))
            $company = Company::findOne($companyId);
        else
            $company = $user->getCompany();
        $modelId = $company->menu->id;
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(
                Yii::$app->user->id,
                'menuAdmin',
                ['modelId' => $modelId, 'class' => 'Menu'])
        )) $this->goBack();

        $model = new MenuCategoryItem();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);

        $back = (isset($params['back']))?intval($params['back']):0;
        $company = Company::findOne($companyId);
        $category = MenuCategory::findOne($categoryId);
        $product = Product::findOne($productId);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                $route, 'back' => $back
            ]);
        }
        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            'company' => $company,
            'companyId' => $companyId,
            'categoryId' => $categoryId,
            'back'  => $back,
            'category' => $category,
            'product'  => $product

        ]);
    }

    /**
     * Updates an existing MenuCategoryItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        $company = $user->getCompany();
        $modelId = $company->menu->id;
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(
                Yii::$app->user->id,
                'menuAdmin',
                ['modelId' => $modelId, 'class' => 'Menu'])
        )) $this->goBack();

        $model = $this->findModel($id);

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $categoryId = (isset($params['categoryId']))?intval($params['categoryId']):0;
        $productId = (isset($params['productId']))?intval($params['productId']):0;
        $back = (isset($params['back']))?intval($params['back']):0;
        $company = Company::findOne($companyId);
        $category = MenuCategory::findOne($categoryId);
        $product = $model->product;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                $route, 'back' => $back
            ]);
        }
        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            'company' => $company,
            'companyId' => $companyId,
            'categoryId' => $categoryId,
            'back'  => $back,
            'category' => $category,
            'product'  => $product

        ]);
    }

    /**
     * Deletes an existing MenuCategoryItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        $company = $user->getCompany();
        $modelId = $company->menu->id;
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(
                Yii::$app->user->id,
                'menuAdmin',
                ['modelId' => $modelId, 'class' => 'Menu'])
        )) $this->goBack();

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MenuCategoryItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MenuCategoryItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MenuCategoryItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
