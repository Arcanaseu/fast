<?php

namespace app\modules\admin\controllers;

use yii\filters\AccessControl;

class ApiDocController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'developer'],
                    ],

                ],
            ]
        ];
    }

    public $layout = 'admin';


    public function actionIndex()
    {
        return $this->render('index');
    }

}
