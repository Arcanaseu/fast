<?php

namespace app\modules\admin\controllers;

use app\models\Company;
use app\models\User;
use app\modules\admin\models\UserSearch;
use Yii;
use app\models\UserCompany;
use app\modules\admin\models\UserCompanySearch;
use app\modules\admin\controllers\AdminBaseController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserCompanyController implements the CRUD actions for UserCompany model.
 */
class UserCompanyController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserCompany models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserCompany model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {


        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        if(!Yii::$app->authManager->checkAccess($user->id, 'superAdmin')){
            $company = $user->getCompany();
            $modelId = null;
            if($company  instanceof Company)
                $modelId = $company->id;
            else
                $this->goBack();
            if(!(
                !Yii::$app->user->isGuest &&
                Yii::$app->authManager->checkAccess(
                    $this->findModel($id)->user->id,
                    'companyAdmin',
                    ['modelId' => $modelId, 'class' => 'Company'])
            )) $this->goBack();
        }

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $back = (isset($params['back']))?$params['back']:'';
        $company = Company::findOne($companyId);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'company' => $company,
            'route'   => $route,
            'companyId' => $companyId,
            'back'     => $back,
        ]);
    }

    /**
     * Creates a new UserCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserCompany();

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $back = (isset($params['back']))?$params['back']:'';
        $company = Company::findOne($companyId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'company' => $company,
                'route'   => $route,
                'companyId' => $companyId,
                'back'     => $back,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'company' => $company,
                'route'   => $route,
                'companyId' => $companyId,
                'back'     => $back,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing UserCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        if(!Yii::$app->authManager->checkAccess($user->id, 'superAdmin')){
            $company = $user->getCompany();
            $modelId = null;
            if($company  instanceof Company)
                $modelId = $company->id;
            else
                $this->goBack();
            if(!(
                !Yii::$app->user->isGuest &&
                Yii::$app->authManager->checkAccess(
                    $this->findModel($id)->user->id,
                    'companyAdmin',
                    ['modelId' => $modelId, 'class' => 'Company'])
            )) $this->goBack();
        }

        $params = \Yii::$app->getRequest()->getQueryParams();
        $model = $this->findModel($id);
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $back = (isset($params['back']))?$params['back']:'';
        $company = Company::findOne($companyId);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'company' => $company,
                'route'   => $route,
                'companyId' => $companyId,
                'back'     => $back,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'company' => $company,
                'route'   => $route,
                'companyId' => $companyId,
                'back'     => $back
            ]);
        }
    }

    /**
     * Deletes an existing UserCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {


        /** @var User $user */
        $user  = User::findOne(Yii::$app->user->id);
        if(!Yii::$app->authManager->checkAccess($user->id, 'superAdmin')){
            $company = $user->getCompany();
            $modelId = null;
            if($company  instanceof Company)
                $modelId = $company->id;
            else
                $this->goBack();
            if(!(
                !Yii::$app->user->isGuest &&
                Yii::$app->authManager->checkAccess(
                    $this->findModel($id)->user->id,
                    'companyAdmin',
                    ['modelId' => $modelId, 'class' => 'Company'])
            )) $this->goBack();
        }

        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        return $this->redirect([$route, 'back' => $back]);
    }

    /**
     * Finds the UserCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return UserCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserCompany::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
