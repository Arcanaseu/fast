<?php

namespace app\modules\admin\controllers;

use app\models\Company;
use Yii;
use app\models\CompanyMetro;
use app\modules\admin\models\CompanyMetroSearch;
use app\modules\admin\controllers\AdminBaseController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyMetroController implements the CRUD actions for CompanyMetro model.
 */
class CompanyMetroController extends AdminBaseController
{
    public function behaviors()
    {
        return [//todo close update and view - only administrator!!!
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ]

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyMetro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyMetroSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanyMetro model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CompanyMetro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyMetro();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route'])) ? $params['route'] : 'index';
        $companyId = (isset($params['companyId'])) ? intval($params['companyId']) : 0;
        $company = Company::findOne($companyId);
        if ($model->load(Yii::$app->request->post()) && @$model->save()) {
            return $this->redirect([$route]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'route' => $route,
                'companyId' => $companyId,
                'company' => $company,
            ]);
        }
    }

    /**
     * Updates an existing CompanyMetro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CompanyMetro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyMetro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CompanyMetro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyMetro::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
