<?php

namespace app\modules\admin\controllers;


use app\models\Additional;
use app\models\Company;
use app\models\CompanyKitchen;
use app\models\CompanyImage;
use app\models\Events;
use app\models\EventsImage;
use app\models\Group;
use app\models\GroupCompany;
use app\models\GroupImage;
use app\models\Image;
use app\models\Menu;
use app\models\MenuImage;
use app\models\ProductImage;
use app\models\UserImage;
use app\sms\Transport;
use yii\base\Exception;
use yii\db\ActiveRecord;
use app\modules\admin\AdminHelper;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\JqueryAsset;
use yii\web\UploadedFile;

class PlaceEditController extends AdminBaseController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','delete'],
                        'roles' => [ 'admin'],
                    ],


                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
//            ],
        ];
    }


    public $layout = 'admin';


    /**
     * @return string
     */
    public function actionIndex()
    {


        //(new Transport())->send(['text' => 'test next from sms', 'source' => 'beermap'], ['+79199804190']);
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);

        $cp = self::getUsersCompany();
        $_SESSION['company'] = ($cp instanceof Company)?$cp->id:null;

        $params = \Yii::$app->getRequest()->getQueryParams();
        $placeId = isset($params['placeId'])?intval($params['placeId']):1;

        if($this->checkAccess('superAdmin')){
        $id = $placeId;
        } elseif ($this->checkAccess('companyAdmin', ['class' => 'Company'])){
            $company = self::getUsersCompany();
            if($company instanceof Company){
                $id = $company->id;
            } else {
                $this->redirect('/admin/');
                exit;
            }
        } elseif(!Yii::$app->user->isGuest) {
            $this->redirect('/admin/');
            exit;
        }else{
            $this->redirect('/');
            exit;
        }


        $class = 'Company';

        $backRoute = 'place-edit/index';
        $route = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
        $images = AdminHelper::getImages($class, $id, $route);

        /** @var Company $model */
        $model = Company::findOne($id);
        if(isset($_FILES['Company']) && $_FILES['Company']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Company Image  - '.$model->title;
                $image->description = $model->description;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->save();

            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }
                $model->save();
            }
        }




        /** @var array $additional */
        $additional = AdminHelper::getModelsByPrefixAndHyphen('Additional', $id, $class, true);
        $kitchens = AdminHelper::getModelsByPrefixAndHyphen($class, $id, 'Kitchen');
        $metro = AdminHelper::getModelsByPrefixAndHyphen($class, $id, 'Metro');
        $phone = AdminHelper::getModelsByPrefixAndHyphen($class, $id, 'Phone');
        $events = Events::find()->where(['creator_company_id' => $id])->all();
        $groups = GroupCompany::find()->where(['company_id' => $id])->all();
        $menu = Menu::find()->where(['company_id' =>$id])->one();
        /** @var Company $company */
        $company = Company::findOne($id);



        return $this->render('index', [
            'class' => $class,
            'id' => $id,
            'backRoute' => $backRoute,
            'additional' => $additional,
            'images' => $images,
            'kitchens' => $kitchens,
            'metro'  => $metro,
            'phone'  => $phone,
            'events' => $events,
            'menu'   =>$menu,
            'company' => $company,
            'groups'  => $groups

        ]);

    }

    public function actionDelete(){

        $companyCheck = false;
        if(isset($_SESSION['company'])){
            $cp = self::getUsersCompany();
            if($cp && $cp->id == $_SESSION['company']){
                $companyCheck = true;
            }
        }

        if(Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'superAdmin') || $companyCheck){
            $params = \Yii::$app->getRequest()->getQueryParams();
            $id = isset($params['id'])?intval($params['id']):0;
            $class = isset($params['class'])?$params['class']:'';
            //$relId = isset($params['relId'])?intval($params['relId']):0;
            //$relClass = isset($params['relClass'])?$params['relClass']:'';
            /** @var ActiveRecord $class */
            $class = new $class();
            $model = $class::findOne($id);
            /** @var ActiveRecord $relClass */
            //$relClass = new $relClass();
            //$relModel = $relClass::findOne($relId);
            if($model instanceof ActiveRecord){

                try{
                    $model->delete();
                } catch(Exception $e){
                    Yii::$app->getSession()->setFlash('not_deleted', 'You can`t delete this model without removing related models');
                }

            }

            $this->redirect(['/admin/place-edit/']);
//            $this->refresh();




        }
//        $this->redirect(['place-edit']);
        $this->redirect(['/admin/place-edit/']);
    }




}
