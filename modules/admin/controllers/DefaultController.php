<?php

namespace app\modules\admin\controllers;

use app\models\CompanyImage;
use app\models\EventsImage;
use app\models\GroupImage;
use app\models\Image;
use app\models\MenuImage;
use app\models\Phone;
use app\models\ProductImage;
use app\models\User;
use app\models\UserImage;
use app\modules\admin\AdminHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\JqueryAsset;
use yii\web\UploadedFile;

class DefaultController extends AdminBaseController
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'delete'],
                'rules' => [

                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ '@'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [ 'admin'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public $layout = 'admin';


    public function actionIndex()
    {
        $id= Yii::$app->user->id;
        /** @var User $model */
        $model = User::findOne($id);
        $flag = ($model->access_token)?false:true;
        $assignments = Yii::$app->authManager->getAssignments($id);
        if(isset($assignments['developer'])){
            $access = 'developer';
        } elseif(isset($assignments['superAdmin'])){
            $access = 'superAdmin';
        } elseif(isset($assignments['admin'])){
            $access = 'admin';
        }  else {
            $access = 'default';
        }
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id;
        $images = AdminHelper::getImages('User', $model->id, $url);


        if(isset($_FILES['User']) && $_FILES['User']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'User Image  - '.$model->name;
                $image->description = $model->surname;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;
                $model->save($flag);
                if($model->phone0 instanceof Phone && !$model->phone0->country_id){
                    $model->phone0->country_id = Yii::$app->request->post('country');
                    $model->phone0->save();

                }

            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }
                if ($model->save($flag)) {
                    if($model->phone0 instanceof Phone && !$model->phone0->country_id){
                        $model->phone0->country_id = Yii::$app->request->post('country');
                        $model->phone0->save();

                    }

                }
            }
        }



        $model = User::findOne($id);

        return $this->render('index', [
            'model' => $model,
            'route' => $route,
            'images' => $images,
            'back'   => $back,
            'access' => $access,
        ]);

    }

    public function actionDeleteImage(){


        if (Yii::$app->request->isAjax) {
            $html = '';
            $post = Yii::$app->request->post();
            $modelId = (isset($post['modelId']))?intval($post['modelId']):null;
            $modelClass = (isset($post['modelClass']))?$post['modelClass']:'';
            $backRoute = (isset($post['backRoute']))?$post['backRoute']:'index';

            $imageId = (isset($post['imageId']))?intval($post['imageId']):null;
            /** @var Image $image */
            $image = Image::find()->where(['id' => $imageId])->one();

            if($image){
                $item = 'app\models\\'.$modelClass.'Image';
                $item = new $item();
                if( $item instanceof CompanyImage ||
                    $item instanceof EventsImage  ||
                    $item instanceof GroupImage   ||
                    $item instanceof MenuImage    ||
                    $item instanceof ProductImage ||
                    $item instanceof UserImage){
                    $prefix = strtolower($modelClass);
                    $field = $prefix.'_id';
                    $relModel = $item::find()->where([$field => $modelId, 'image_id' => $imageId])->one();
                    if($relModel instanceof ActiveRecord){
                        $relModel->delete();
                    }
                }
                $path = $image->path;
                if($image->delete())
                     @unlink($path);
            }

            $html .= AdminHelper::getImages($modelClass, $modelId, $backRoute);
            echo json_encode($html);
        }
    }

    public function actionImageCreate()
    {
        if (Yii::$app->request->isAjax) {
            $html = '';

            if(isset($_FILES) && count($_FILES) > 0){


                $modelClass = isset($_POST['class'])?$_POST['class']:'';
                $modelId =  isset($_POST['classId'])?intval($_POST['classId']):null;
                $backRoute = (isset($_POST['backRoute']))?$_POST['backRoute']:'index';
                $prefix = strtolower($modelClass);
                $field = $prefix.'_id';
                $item = 'app\models\\'.$modelClass.'Image';


                $model = new Image();
                $model->title = isset($_POST['title'])?$_POST['title']:'';
                $model->description = isset($_POST['description'])?$_POST['description']:'';

                $uf = new UploadedFile();
                $uf->name = $_FILES['img']['name'];
                $uf->tempName = $_FILES['img']['tmp_name'];
                $uf->type = $_FILES['img']['type'];
                $uf->size = $_FILES['img']['size'];
                $uf->error = $_FILES['img']['error'];


                $model->file = $uf;
                $model->created =date('Y-m-d H:i:s');
                $time = time();
                $fileName = 'uploads/' . $time . '.' . $model->file->extension;
                $fileName1 = 'uploads-middle/' . $time . '.' . $model->file->extension;
                $fileName2 = 'thumbnail/' . $time . '.' . $model->file->extension;
                $model->validate();
                if ($model->validate()) {
                    $model->file->saveAs($fileName,false);
                    $model->file->saveAs($fileName1,false);
                    $model->file->saveAs($fileName2,false);
                    $model->path = $fileName;
                    if($model->save()){
                        if($modelId){
                            $item = new $item();
                            if( $item instanceof CompanyImage ||
                                $item instanceof EventsImage  ||
                                $item instanceof GroupImage   ||
                                $item instanceof MenuImage    ||
                                $item instanceof ProductImage ||
                                $item instanceof UserImage){
                                $item->$field = $modelId;
                                $item->image_id = $model->primaryKey;
                                $item->save();
                            }

                        }

                    }


                }
                $html .= AdminHelper::getImages($modelClass, $modelId, $backRoute);

            }


            echo $html;
        }
    }

    public function actionImage(){
        if (Yii::$app->request->isAjax) {
            $id =  isset($_GET['id'])?intval($_GET['id']):null;
            $html = '';
            /** @var Image $model */
            $model = Image::findOne($id);
            if($model){
                $html .= '<img src="/'.$model->path.'" width="200" height="150">';
            }
            echo $html;

        }
    }




}
