<?php

namespace app\modules\admin\controllers;

use app\models\Image;
use app\modules\admin\AdminHelper;
use Yii;
use app\models\Events;
use app\modules\admin\models\EventsSearch;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'eventsAdmin', ['modelId' => $id])
        )) $this->goBack();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);
        $model = $this->findModel($id);
        $date = $model->date;
        if($date && is_string($date)){
            $date = (int) strtotime($date);
            if(time() > $date && $model->is_expired != 1){
                $model->is_expired = 1;
                $model->save();
            }
        }

        $params = \Yii::$app->getRequest()->getQueryParams();
        $back = (isset($params['back']))?$params['back']:'';
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):0;
        $userId = (isset($params['userId']))?intval($params['userId']):0;
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId.'&userId='.$userId;
        $images = AdminHelper::getImages('Events', $model->id, $url);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route' => $route,
            'companyId' => $companyId,
            'userId' => $userId,
            'images' => $images,
            'back'   => $back,
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Events();

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):null;
        $userId = (isset($params['userId']))?intval($params['userId']):null;
        $back = (isset($params['back']))?$params['back']:'';
        //$company = Company::findOne($companyId);
        if(isset($_FILES['Events']) && $_FILES['Events']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->created = date('Y-m-d H:i:s');
                if ($model->creator_company_id = $companyId){
                    $model->creator_type = 'company';
                } elseif ($model->creator_user_id = $userId){
                    $model->creator_type = 'user';
                } elseif ($model->creator_company_id = AdminHelper::getCreatorCompany()) {
                    $model->creator_type = 'company';
                } else {
                    $model->creator_user_id = AdminHelper::getCreatorUser();
                    $model->creator_type = 'user';
                }

                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Avatar Events Image  - '.$model->title;
                $image->description = '';
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        //'company' => $company,
                        'companyId' => $companyId,
                        'userId' => $userId,
                        'back'   => $back,
                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $model->created =date('Y-m-d H:i:s');
                if ($model->creator_company_id = $companyId){
                    $model->creator_type = 'company';
                } elseif ($model->creator_user_id = $userId){
                    $model->creator_type = 'user';
                } elseif ($model->creator_company_id = AdminHelper::getCreatorCompany()) {
                    $model->creator_type = 'company';
                } else {
                    $model->creator_user_id = AdminHelper::getCreatorUser();
                    $model->creator_type = 'user';
                }
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        //'company' => $company,
                        'companyId' => $companyId,
                        'userId' => $userId,
                        'back'   => $back,
                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            //'company' => $company,
            'companyId' => $companyId,
            'userId' => $userId,
            'back'   => $back,
        ]);

    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'eventsAdmin', ['modelId' => $id])
        )) $this->goBack();

        $model = $this->findModel($id);

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $companyId = (isset($params['companyId']))?intval($params['companyId']):null;
        $userId = (isset($params['userId']))?intval($params['userId']):null;
        $back = (isset($params['back']))?$params['back']:'';
        //$company = Company::findOne($companyId);
        $url = Yii::$app->controller->id.'/view/?id='.$id.'&companyId='.$companyId.'&userId='.$userId;
        $images = AdminHelper::getImages('Events', $model->id, $url);
        if(isset($_FILES['Events']) && $_FILES['Events']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->created = date('Y-m-d H:i:s');
                if ($model->creator_company_id = $companyId){
                    $model->creator_type = 'company';
                } elseif ($model->creator_user_id = $userId){
                    $model->creator_type = 'user';
                } elseif ($model->creator_company_id = AdminHelper::getCreatorCompany()) {
                    $model->creator_type = 'company';
                } else {
                    $model->creator_user_id = AdminHelper::getCreatorUser();
                    $model->creator_type = 'user';
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'Avatar Events Image  - '.$model->title;
                $image->description = '';
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        //'company' => $company,
                        'companyId' => $companyId,
                        'userId' => $userId,
                        'images' => $images,
                        'back'   => $back,
                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $model->created =date('Y-m-d H:i:s');
                if ($model->creator_company_id = $companyId){
                    $model->creator_type = 'company';
                } elseif ($model->creator_user_id = $userId){
                    $model->creator_type = 'user';
                } elseif ($model->creator_company_id = AdminHelper::getCreatorCompany()) {
                    $model->creator_type = 'company';
                } else {
                    $model->creator_user_id = AdminHelper::getCreatorUser();
                    $model->creator_type = 'user';
                }
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        //'company' => $company,
                        'companyId' => $companyId,
                        'userId' => $userId,
                        'images' => $images,
                        'back'   => $back,
                    ]);
                }
            }
        }
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id.'&companyId='.$companyId.'&userId='.$userId;
        $images = AdminHelper::getImages('Events', $model->id, $url);
        return $this->render('update', [
            'model' => $model,
            'route' => $route,
            //'company' => $company,
            'companyId' => $companyId,
            'userId' => $userId,
            'images' => $images,
            'back'   => $back,
        ]);
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if(!(
            !Yii::$app->user->isGuest &&
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'eventsAdmin', ['modelId' => $id])
        )) $this->goBack();

        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
