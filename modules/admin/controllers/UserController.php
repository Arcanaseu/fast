<?php

namespace app\modules\admin\controllers;

use app\models\Image;
use app\modules\admin\AdminHelper;
use Yii;
use app\models\User;
use app\modules\admin\models\UserSearch;
use yii\filters\AccessControl;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index',  'update', 'view', 'delete'],
                        'roles' => [ 'admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [ 'superAdmin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(!Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'superAdmin')){
            if(Yii::$app->user->id != $id)
                $this->goBack();
        }

        $model = $this->findModel($id);
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id;
        $images = AdminHelper::getImages('User', $model->id, $url);

        $assignments = Yii::$app->authManager->getAssignments($id);
        if(isset($assignments['developer'])){
            $access = 'developer';
        } elseif(isset($assignments['superAdmin'])){
            $access = 'superAdmin';
        } else {
            $access = 'admin';
        }



        return $this->render('view', [
            'model' => $model,
            'route' => $route,
            'images' => $images,
            'back'   => $back,
            'access' => $access,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);



        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        if(isset($_FILES['User']) && $_FILES['User']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'User Image  - '.$model->name;
                $image->description = $model->surname;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'back'   => $back,

                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }

                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'back'   => $back,

                    ]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'route' => $route,
            'back'   => $back,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'superAdmin')){
            if(Yii::$app->user->id != $id)
                $this->goBack();
        }

        $this->getView()->registerJsFile('@web/assets/js/angular.min.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-create.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/forms.js', ['depends' => [JqueryAsset::className()]]);
        $this->getView()->registerJsFile('@web/assets/js/image-resize.js', ['depends' => [JqueryAsset::className()]]);

        $model = $this->findModel($id);

        $assignments = Yii::$app->authManager->getAssignments($id);
        if(isset($assignments['developer'])){
            $access = 'developer';
        } elseif(isset($assignments['superAdmin'])){
            $access = 'superAdmin';
        } else {
            $access = 'admin';
        }


        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        $url = Yii::$app->controller->id.'/view/?id='.$id;
        $images = AdminHelper::getImages('User', $model->id, $url);
        if(isset($_FILES['User']) && $_FILES['User']['size']['file'] > 0){
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $image = new Image();
                $image->path = $fileName;
                $image->file = $model->file;
                $image->title = 'User Image  - '.$model->name;
                $image->description = $model->surname;
                if($image->validate())
                    $image->file->saveAs($fileName,false);
                $model->avatar = ($image->save())? $image->id:null;

                $model->validate();
                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'images' => $images,
                        'back'   => $back,
                        'access' => $access,

                    ]);
                }
            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $coordinates = AdminHelper::getCoordinates($model->address);
                if($coordinates && is_array($coordinates) && count($coordinates) == 2){
                    $model->lat = $coordinates[0];
                    $model->lon = $coordinates[1];
                }

                if ($model->save()) {

                    return $this->redirect([
                        'view',
                        'id' => $model->id,
                        'route' => $route,
                        'images' => $images,
                        'back'   => $back,
                        'access' => $access,

                    ]);
                }
            }
        }

        $url = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id.'/?id='.$id;
        $images = AdminHelper::getImages('User', $model->id, $url);
        return $this->render('update', [
            'model' => $model,
            'route' => $route,
            'images' => $images,
            'back'   => $back,
            'access' => $access,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if(!Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'superAdmin')){
            if(Yii::$app->user->id != $id)
                $this->goBack();
        }

        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
