<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Country;
use app\modules\admin\models\CountrySearch;
use app\modules\admin\controllers\AdminBaseController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'view', 'delete'],
                        'roles' => [ 'superAdmin'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Country models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Country model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        return $this->render('view', [
            'model' => $this->findModel($id),
            'route' => $route,
            'back'   => $back,
        ]);
    }

    /**
     * Creates a new Country model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Country();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'back'   => $back,

            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'route' => $route,
                'back'   => $back,
            ]);
        }
    }

    /**
     * Updates an existing Country model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        $back = (isset($params['back']))?$params['back']:'';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id,
                'route' => $route,
                'back'   => $back,

            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'route' => $route,
                'back'   => $back,
            ]);
        }
    }

    /**
     * Deletes an existing Country model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $params = \Yii::$app->getRequest()->getQueryParams();
        $route = (isset($params['route']))?$params['route']:'index';
        return $this->redirect([$route]);
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Country the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Country::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
