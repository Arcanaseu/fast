<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AcceptedCompany;

/**
 * AcceptedCompanySearch represents the model behind the search form about `app\models\AcceptedCompany`.
 */
class AcceptedCompanySearch extends AcceptedCompany
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'events_id', 'company_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AcceptedCompany::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'events_id' => $this->events_id,
            'company_id' => $this->company_id,
        ]);

        return $dataProvider;
    }
}
