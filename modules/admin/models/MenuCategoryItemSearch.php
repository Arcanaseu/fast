<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MenuCategoryItem;

/**
 * MenuCategoryItemSearch represents the model behind the search form about `app\models\MenuCategoryItem`.
 */
class MenuCategoryItemSearch extends MenuCategoryItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'category_id', 'price', 'discount'], 'integer'],
            [['discount_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MenuCategoryItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'discount' => $this->discount,
        ]);

        $query->andFilterWhere(['like', 'discount_type', $this->discount_type]);

        return $dataProvider;
    }
}
