<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventsVideo;

/**
 * EventsVideoSearch represents the model behind the search form about `app\models\EventsVideo`.
 */
class EventsVideoSearch extends EventsVideo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'events_id', 'video_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventsVideo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'events_id' => $this->events_id,
            'video_id' => $this->video_id,
        ]);

        return $dataProvider;
    }
}
