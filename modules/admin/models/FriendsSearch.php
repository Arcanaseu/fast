<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Friends;

/**
 * FriendsSearch represents the model behind the search form about `app\models\Friends`.
 */
class FriendsSearch extends Friends
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'initiator_id', 'acceptor_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Friends::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'initiator_id' => $this->initiator_id,
            'acceptor_id' => $this->acceptor_id,
        ]);

        return $dataProvider;
    }
}
