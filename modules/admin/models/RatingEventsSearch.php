<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RatingEvents;

/**
 * RatingEventsSearch represents the model behind the search form about `app\models\RatingEvents`.
 */
class RatingEventsSearch extends RatingEvents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'events_id', 'score', 'voted'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RatingEvents::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'events_id' => $this->events_id,
            'score' => $this->score,
            'voted' => $this->voted,
        ]);

        return $dataProvider;
    }
}
