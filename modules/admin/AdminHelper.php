<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 12.07.2015
 * Time: 12:34
 */

namespace app\modules\admin;


use app\models\Additional;
use app\models\City;
use app\models\Company;
use app\models\CompanyImage;
use app\models\Country;
use app\models\Kitchen;
use app\models\Currency;
use app\models\Events;
use app\models\EventsImage;
use app\models\Group;
use app\models\GroupImage;
use app\models\Hidden;
use app\models\HiddenCompany;
use app\models\HiddenGroup;
use app\models\HiddenUser;
use app\models\Image;
use app\models\MentionCompany;
use app\models\MentionEvents;
use app\models\Menu;
use app\models\MenuCategory;
use app\models\MenuImage;
use app\models\Metro;
use app\models\Phone;
use app\models\Product;
use app\models\ProductBrand;
use app\models\ProductBrewery;
use app\models\ProductCategory;
use app\models\ProductFortress;
use app\models\ProductImage;
use app\models\ProductPacking;
use app\models\ProductSort;
use app\models\ProductType;
use app\models\User;
use app\models\UserImage;
use app\models\Video;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


class AdminHelper
{
    public static $images = [
        'company-image',
        'events-image',
        'group-image',
        'menu-image',
        'product-image',
        'user-image'

    ];

    public static function getMetroList(){
        $models = Metro::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'metro');
    }

    public static function getCoordinates($name){

        $lat = $name;
        $lon = $name;

        return [0,0];
        /*
        return [
            'lat' => 0,
            'lon' => 0
        ];//stab

        return [
            'lat' => $lat,
            'lon' => $lon
        ];
        */
    }

    /**
     * @return bool|integer
     */
    public static function getCreatorUser(){
        if(!\Yii::$app->user->isGuest){
            return \Yii::$app->user->id;
        }
        return null;
    }

    /**
     * @return bool|integer
     */
    public static function getCreatorCompany(){
        if(!\Yii::$app->user->isGuest){
            $user = User::findOne(\Yii::$app->user->id);
            if($user instanceof User){
                $company = $user->getCompany();
                if($company instanceof Company)
                    return $company->id;
            }
        }
        return null;
    }


    /**
     * @return array
     */
    public static function getImagesList() {
        $models = Image::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return array
     */
    public static function getAdditionalList() {
        $models = Additional::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getCityList() {
        $models = City::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'city');
    }


    /**
     * @return array
     */
    public static function getKitchenList() {
        $models = Kitchen::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return array
     */
    public static function getCurrencyList() {
        $models = Currency::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }



    /**
     * @return array
     */
    public static function getPhoneList() {
        $models = Phone::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'number');
    }

    /**
     * @return array
     */
    public static function getCategoryList() {
        $models = ProductCategory::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }



    /**
     * @return array
     */
    public static function getTypeList() {
        $models = ProductType::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getBrandList() {
        $models = ProductBrand::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getSortList() {
        $models = ProductSort::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return array
     */
    public static function getPackingList() {
        $models = ProductPacking::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getBreweryList() {
        $models = ProductBrewery::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getFortressList() {
        $models = ProductFortress::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }



    /**
     * @return array
     */
    public static function getCountryList() {
        $models = Country::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return array
     */
    public static function getMentionCompanyList() {
        $models = MentionCompany::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }



    /**
     * @return array
     */
    public static function getMentionEventsList() {
        $models = MentionEvents::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return array
     */
    public static function getMentionProductList() {
        $models = MentionCompany::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getMentionUserList() {
        $models = MentionCompany::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }





    /**
     * @return array
     */
    public static function getCompanyList() {
        $models = Company::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getEventsList() {
        $models = Events::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getUsersList() {
        $models = User::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'name');
    }

    /**
     * @return array
     */
    public static function getVideoList() {
        $models = Video::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getGroupsList() {
        $models = Group::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getProductList() {
        $models = Product::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getMenuList() {
        $models = Menu::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getMenuCategoryList() {
        $models = MenuCategory::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }



    public static function getCreator(){
        $creatorType = 'user';
        if(!($creator = self::getCreatorUser())){
            if($creator = self::getCreatorCompany());
                $creatorType = 'company';
        }
        return [$creatorType, $creator];
    }

    public static function myEvents(){
        $creator = self::getCreator();
        $events = [];
        switch ($creator[0]){
            case 'company':
                /** @var Company $ownerCompany */
                $ownerCompany = $creator[1];
                $events = $ownerCompany->events;
                break;
            case 'user':
                /** @var User $ownerUser */
                $ownerUser = $creator[1];
                $events = $ownerUser->events;
                break;
        }
        return ArrayHelper::map($events, 'id', 'title');

    }

    public static function getOwner(){
        $creator = self::getCreator();
        $owner = null;

        switch ($creator[0]){
            case 'company':
                /** @var Company $ownerCompany */
                $ownerCompany = $creator[1];
                $owner = $ownerCompany;
                break;
            case 'user':
                /** @var User $ownerUser */
                $ownerUser = $creator[1];
                $owner = $ownerUser;
                break;
        }
        return $owner;
    }

    public static function getHiddenEvents(){

        $owner = self::getOwner();
        $hidden = ($owner instanceof Company || $owner instanceof User)?$owner->hiddens:[];
        $events = [];
        $hiddenList = [];
        /** @var Hidden $h */
        foreach($hidden as $h){
            $events[] = $h->events;
            $hiddenList[] = [$h->id => $h->events->title];
        }


        return [$events, $hiddenList];

    }


    public static function getHiddenCompanies(){

        $owner = self::getOwner();
        $hidden = ($owner instanceof Company || $owner instanceof User)?$owner->hiddenCompanies:[];
        $companies = [];
        $hiddenList = [];
        /** @var HiddenCompany $h */
        foreach($hidden as $h){
            $companies[] = $h->hiddenCompany;
            $hiddenCompaniesList[] = [$h->id => $h->hiddenCompany->title];
        }


        return [$companies, $hiddenList];

    }


    public static function getHiddenUsers(){

        $owner = self::getOwner();
        $hidden = ($owner instanceof Company || $owner instanceof User)?$owner->hiddenUsers:[];
        $users = [];
        $hiddenList = [];
        /** @var HiddenUser $h */
        foreach($hidden as $h){
            $users[] = $h->hiddenUser;
            $hiddenCompaniesList[] = [$h->id => $h->hiddenUser->name];
        }


        return [$users, $hiddenList];

    }


    public static function getHiddenGroups(){

        $owner = self::getOwner();
        $hidden = ($owner instanceof Company || $owner instanceof User)?$owner->hiddenGroups:[];
        $groups = [];
        $hiddenList = [];
        /** @var HiddenGroup $h */
        foreach($hidden as $h){
            $groups[] = $h->hiddenGroup;
            $hiddenCompaniesList[] = [$h->id => $h->hiddenGroup->title];
        }


        return [$groups, $hiddenList];

    }


    public static function getHiddenEventsList(){
        $res = self::getHiddenEvents();
        return $res[1];
    }

    public static function getHiddenCompaniesList(){
        $res = self::getHiddenCompanies();
        return $res[1];
    }

    public static function getHiddenUsersList(){
        $res = self::getHiddenUsers();
        return $res[1];
    }
    public static function getHiddenGroupsList(){
        $res = self::getHiddenGroups();
        return $res[1];
    }




    public static function getNotHiddenEventsList(){
        $hidden = self::getHiddenEvents();
        $hidden = $hidden[0];
        $events = self::getEventsList();
        $res = array_diff($events, $hidden);
        return ArrayHelper::map($res, 'id', 'title');
    }

    public static function getNotHiddenCompaniesList(){
        $hidden = self::getHiddenCompanies();
        $hidden = $hidden[0];
        $events = self::getCompanyList();
        $res = array_diff($events, $hidden);
        return ArrayHelper::map($res, 'id', 'title');
    }

    public static function getNotHiddenUsersList(){
        $hidden = self::getHiddenUsers();
        $hidden = $hidden[0];
        $events = self::getUsersList();
        $res = array_diff($events, $hidden);
        return ArrayHelper::map($res, 'id', 'title');
    }

    public static function getNotHiddenGroupsList(){
        $hidden = self::getHiddenGroups();
        $hidden = $hidden[0];
        $events = self::getGroupsList();
        $res = array_diff($events, $hidden);
        return ArrayHelper::map($res, 'id', 'title');
    }


    public static function getImages($modelClass, $id, $route ){

        $class = strtolower($modelClass);
        $link = $class.'-image';
        $field = $class.'_id';
        $html='';
        if(in_array($link, self::$images)){
            $model = 'app\models\\'.$modelClass.'Image';
            /** @var ActiveRecord $model */
            $model = new $model();
            $query = $model::find();
            $models = $query->where([$field => $id])->all();
            foreach($models as $item){
                if( $item instanceof CompanyImage ||
                    $item instanceof EventsImage  ||
                    $item instanceof GroupImage   ||
                    $item instanceof MenuImage    ||
                    $item instanceof ProductImage ||
                    $item instanceof UserImage){
                    $html .=  "<div style='display: inline !important;float:left;'>

<a href='/admin/image/update?id=".$item->image->id."&route=".$route."' target='_blank'><span class='glyphicon glyphicon-pencil' style='width:25px;float:right;margin-left:15px'></span></a>
                                  <a href='/admin/image/view?id=".$item->image->id."&route=".$route."' target='_blank'><span class='glyphicon glyphicon-eye-open' style='width:25px;float:right;margin-left:25px'></span></a>
                                  <div class='admin-image-delete' style='text-align: right;cursor:pointer; height:25px; color:red' onclick='conf(".$item->image->id.")'>X</div>
                                  <img class='admin-img' src='/".$item->image->path."'  width='250' height='200'>
                               </div>";
                }

            }

        }
        return $html;
    }

    public static function getModelsByPrefixAndHyphen($prefixModelClass, $modelId, $hyphenModelClass, $reverse = false){

        $modelClass =  'app\models\\'.$prefixModelClass.$hyphenModelClass;
        $class = ($reverse)?strtolower($hyphenModelClass):strtolower($prefixModelClass);
        $field = $class.'_id';
        /** @var ActiveRecord $model */
        $model = new $modelClass();
        if($model instanceof ActiveRecord){
            $models = $model::find()->where([$field => $modelId])->all();
            $models = (is_array($models))?$models:[];
        } else {
            $models = [];
        }
        return $models;


    }

    public static function getCountryCodeList()
    {
        $models = Country::find()->asArray()->all();
        return ArrayHelper::map($models, 'phone_code', 'title');
    }


}