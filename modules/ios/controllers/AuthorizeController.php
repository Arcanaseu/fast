<?php

namespace app\modules\ios\controllers;

use app\models\Country;
use app\modules\ios\models\User as RegUser;
use app\models\Image;
use app\models\LoginForm;
use app\models\Phone;
use app\models\RegistrationForm;
use app\models\User;
use app\modules\ios\models\ActiveMobileUser ;
use app\sms\Transport;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\GraphNodes\GraphPicture;
use Yii;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\Response;
use yii\web\UploadedFile;

class AuthorizeController extends Controller
{

    public $params;
    public $enableCsrfValidation = false;


    public static  $countries = [
        'n/a',
        3,//'Россия',
        5,//'Украина',
        6,//'Беларусь',
        1,//'Литва',

    ];

    private $countryPhoneLength = [
        7 => 10,
        370 => 8,
        371 => 8,
        38 => 10,
    ];


    /**
     * @return array
     */
    public function actionPlain()
    {

        //session_name('sid');
        session_start();
        $sid = session_id();
        Yii::$app->user->getIdentity();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $params = \Yii::$app->getRequest()->getQueryParams();
        if (!\Yii::$app->user->isGuest) {
            /** @var User $user */
            $user = User::findOne(Yii::$app->user->id);
            $active = ActiveMobileUser::find()->where(['access_token' => $sid])->one();
            if(!$active ||
               !($active instanceof ActiveMobileUser) ||
                Yii::$app->user->id  != $active->user_id
                //||  $active->expired_at < time()
            ){

                return [
                    'error' => 'authorization error'
                ];
            } else{

                return [
                    'user' => $user,
                    'PHPSESSID'  => $sid,
                    'not_guest'  =>true,
                ];
            }

        } else {
            if(!(array_key_exists('username', $params)) || !(array_key_exists('password', $params))){
                return [
                    'error' => 'authorization error'
                ];
            }
            $model = new LoginForm();
            $model->username =   $params['username'];
            $model->password =   $params['password'];
            if($model->login()){
                $sid = session_id();
                /** @var User $user */
                $user = User::findOne(Yii::$app->user->id);
                return $this->createActiveMobileUser($user, $sid);
            }

        }
        return [
            'error' => 'authorization error'
        ];

    }


    /**
     * @return array
     * @throws \Exception
     */
    public function actionLogout()
    {
        if(array_key_exists('PHPSESSID',$_GET)){
            $sid = $_GET['PHPSESSID'];

        }elseif(array_key_exists('PHPSESSID',$_POST)){
            $sid = $_POST['PHPSESSID'];
        }else{
            $sid = null;
        }
        if($sid){
            session_id($sid);
        }

        session_start();

        Yii::$app->user->getIdentity();
        $sid = session_id();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $active = ActiveMobileUser::find()->where(['access_token' => $sid])->one();
        if($active instanceof ActiveMobileUser){
            if($active->delete()){
                Yii::$app->user->logout();
                Yii::$app->response->cookies->remove('PHPSESSID');
                return ['logout' => 'success'];
            }
        }
        @session_unset();
        @session_destroy();
        Yii::$app->response->cookies->remove('PHPSESSID');
        return ['logout' => 'not logged'];

    }

    /**
     * @return array
     */
    public function actionSocial()
    {
        $params = \Yii::$app->getRequest()->getQueryParams();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = null;
        if(isset($params['provider'])){
            switch($params['provider']){
                case 'Facebook':
                    $user =  $this->fb($params['token']);
                    break;
                case 'Vkontakte':
                    $user =  $this->vk($params['token']);
                    break;
                case '|OK':
                    $user =  $this->ok($params['token']);
                    break;

            }
        }
        if($user instanceof User){
            $sid = session_id();
            return $this->createActiveMobileUser($user, $sid);
        }
        return ['error' =>'Bad request'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public  function actionUnlink(){
        $params = \Yii::$app->getRequest()->getQueryParams();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = null;
        if(array_key_exists('access_token',$params)){
            $user = User::find()->where(['access_token' => $params['access_token']])->one();

        }
        if($user instanceof User){
            $active = ActiveMobileUser::find()->where(['user_id' => $user->id])->one();
            if($active){
                $active->delete();
            }
            if($user->delete()){
                return ['unlink' => 'success'];
            }

        }
        return ['unlink' => 'error'];

    }


    public function actionSignupFirst(){

        session_start();
        if(array_key_exists('PHPSESSID',$_POST)){
            $sid = $_POST['PHPSESSID'];
        }elseif(array_key_exists('PHPSESSID',$_GET)){
            $sid = $_GET['PHPSESSID'];
        }else{
            $sid = null;
        }
        if($sid){
            session_id($sid);
        }

        Yii::$app->user->getIdentity();

        Yii::$app->response->format = Response::FORMAT_JSON;
        $errors = [];
        if (!\Yii::$app->user->isGuest) {
            $user = User::findOne(Yii::$app->user->id);
            return ['PHPSESSID' =>session_id(), 'user' =>$user, 'exist' => 'true'];
        }
        $this->cleanTemporaryTables();
        $model = new RegUser();
        if(array_key_exists('User', $_FILES) && $_FILES['User']['size']['photo'] > 0){

            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->phone = $this->checkPhone($model);
                $model->session = session_id();
                $model->expired_at = time() + 20*60;
                if(!$model->username || $model->username == ''){
                    $model->username = (string) $model->phone;
                }
                $model->file = UploadedFile::getInstance($model, 'photo');
                $fileName = 'uploads/' . time() . '.' . $model->file->extension;
                $model->path = $fileName;
                if($model->validate())
                    $model->file->saveAs($fileName,false);
                $model->validate();
                if ($model->save()) {
                    $res = $this->setCheck($model);

                    return  ['result' => $res, 'PHPSESSID' => session_id()];
                } else {
                    return  ['errors' => $model->errors];
                }

            }
        }else{
            if ($model->load(Yii::$app->request->post())) {
                $model->phone = $this->checkPhone($model);
                $model->expired_at = time() + 20*60;
                $model->session = session_id();
                if(!$model->username || $model->username == ''){
                    $model->username = (string)$model->phone;
                }
                $model->validate();
                if ($model->save()) {
                    $res = $this->setCheck($model);
                    return  ['result' => $res,  'PHPSESSID' => session_id()];
                } else {
                    return  ['errors' => $model->errors];
                }
            }
        }

        $errors[] = 'Bad request';
        return  ['errors' => $errors];





    }
    public function actionSignupSecond(){
        $this->enableCsrfValidation = false;

        session_start();
        if(array_key_exists('PHPSESSID',$_POST)){
            $sid = $_POST['PHPSESSID'];
        }elseif(array_key_exists('PHPSESSID',$_GET)){
            $sid = $_GET['PHPSESSID'];
        }else{
            $sid = null;
        }
        if($sid){
            session_id($sid);
        }

        Yii::$app->user->getIdentity();
        if (!\Yii::$app->user->isGuest) {

            $user = User::findOne(Yii::$app->user->id);
            return ['PHPSESSID' =>session_id(), 'user' =>$user, 'exist' => 'true'];
        }
        $post = Yii::$app->request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $errors = [];
        if(isset($post['User']['sms_code'])){
            $model = RegUser::find()->where(['session' => session_id()])->one();
            if($model && $model->checking == trim($post['User']['sms_code'])){
                return $this->registration($model);

            }
            if(!$model){
                $errors[] = 'Wrong access_token(PHPSESSID)';
            }elseif($model->checking != trim($post['User']['sms_code'])){
                $errors[] = 'Wrong sms_code';
            }

            return  ['errors' => $errors];
        }
        $errors[] = 'sms_code is absent in parameters';
        return  ['errors' => $errors];
    }
    public function actionSignupResend(){

        session_start();
        Yii::$app->user->getIdentity();
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!\Yii::$app->user->isGuest) {

            $user = User::findOne(Yii::$app->user->id);
            return ['PHPSESSID' =>session_id(), 'user' =>$user, 'exist' => 'true'];
        }
        $errors = [];
        if(array_key_exists('PHPSESSID',$_POST)){
            $sid = $_POST['PHPSESSID'];
        }elseif(array_key_exists('PHPSESSID',$_GET)){
            $sid = $_GET['PHPSESSID'];
        }else{
            $sid = null;
        }
        $model = RegUser::find()->where(['session' => $sid])->one();
        if(!$model) {
            $errors[] = 'Wrong access_token(PHPSESSID)'.session_id();
            return  ['errors' => $errors];
        }
        $res = $this->setCheck($model);
        return ['result' => $res,  'PHPSESSID' => session_id()];
    }




    /**
     * @param RegUser $model
     * @return array
     */
    protected function registration(RegUser $model){



        $user = new User();
        if($model->path && $model->path != ''){
            $image = new Image();
            $image->setAttribute('path',$model->path);
            $image->setAttribute('title','User Image  - '.$model->username);
            if($image->save())
                $user->setAttribute('avatar',$image->id);
        }

        $user->setAttribute('username',$model->username);
        $user->setAttribute('password', md5($model->password));
        $user->setAttribute('gender', $model->gender);

        $phone = new Phone();
        $ph =  (float)$model->phone;
        $phone->setAttribute('number', $ph);
        /** @var Country $country */
        $country = Country::find()->where(['phone_code' => $model->country])->one();
        $phone->setAttribute('country_id',$country->id);
        $phone->validate();
        $phone->save();
        $user->setAttribute('phone', $phone->id);
        $user->setAttribute('email',$model->email);
        $user->validate();
        if($user->save()){
            Yii::$app->user->login($user,  0);

            $num =  $model->getPhone();
            $model->delete();
            $text = 'You are registered at beermap';
            $res = (new Transport())->send(['text' => $text, 'source' => 'beermap'], [$num]);
            $this->createActiveMobileUser($user, session_id());
            return [
                'result' =>$res,
                'user' => $user,
                'PHPSESSID'  => session_id(),
            ];
        }
        return ['errors' => $user->errors];
    }

    private function cleanTemporaryTables()
    {
        $t = time();
        $criteria = "'expired_at' < {$t}";
        $users = RegUser::find()->where($criteria)->all();
        /** @var RegUser $user */
        foreach($users as $user){
            if($path = $user->path)
                unlink($path);
            $user->delete();
        }
    }

    private function setCheck($user)
    {

        $code = ($user instanceof RegUser && !$user->checking)?(substr(md5(time()),0,4)):$user->checking;
        $user->checking = $code;
        $user->save();
        $phone = $user->getPhone();
        $res = (new Transport())->send(['text' => $code, 'source' => 'beermap'], [$phone]);
        return $res;
    }

    private function checkPhone(RegUser $model)
    {
        $model->phone = preg_replace("/[^0-9]/", "", $model->phone );
        $len = $this->countryPhoneLength[$model->country];
        if(strlen($model->phone) > $len)
            $model->phone = substr($model->phone, -$len);
        elseif(strlen($model->phone) < $len)
            return null;

        return (float) $model->phone;
    }

    /**
     * @return array
     */
    public  function actionSignup(){


        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RegistrationForm();
        if ($model->load(Yii::$app->request->post())){
            $user = new User();
            $user->setAttribute('username',$model->username);
            $user->setAttribute('password', md5($model->password));
            $phone = new Phone();
            $ph =  $model->phone;
            $phone->setAttribute('number', $ph);
            $cntr = (int) $model->country;
            $phone->setAttribute('country_id',$cntr);
            $phone->validate();
            $phone->save();
            $user->setAttribute('phone', $phone->id);
            $user->setAttribute('email',$model->email);
            $user->validate();

            if($user->save()){

                Yii::$app->user->login($user,  0);
                $sid = session_id();
                $num =  $user->getPhone();
                $text = 'You are registered at beermap';
                (new Transport())->send(['text' => $text, 'source' => 'beermap'], [$num]);
                return  $this->createActiveMobileUser($user, $sid);
            }
            return ['error' => $user->errors];
        }
        return ['error' => 'Bad request'];
    }


    /**
     * @param $id
     * @throws \yii\db\Exception
     */
    private function cleanActive($id)
    {
        $check = intval($id) % 100;
        if($check == 0){
            $t = time();
            $sql = "DELETE FROM `active_mobile_user` WHERE 'expired_at' < {$t}";
            \Yii::$app->db->createCommand($sql)->execute();
        }
    }

    /**
     * @param $userInfo
     * @return User|null|\yii\web\IdentityInterface|static
     */
    private function okLogin($userInfo)
    {

        $name = (isset($userInfo['first_name']))?$userInfo['first_name']:null;
        $surname = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $uid = (isset($userInfo['uid']))?$userInfo['uid']:null;
        if(!$name && $surname)
            $name = $surname;
        elseif(!$name)
            $name = $uid;


        if(!$uid)
            $this->goHome();
        $access = md5(md5($uid).sha1($uid));
        $user = User::findIdentityByAccessToken($access);
        if($user){
            Yii::$app->user->login($user,  0);
            return $user;
        } else {
            $user = new User();
            $user->access_token = $access;
            $user->name = $name;
            $user->surname = $surname;
            if($user->save(false)){
                if(isset($userInfo['pic_2'])){
                    $url = $userInfo['pic_2'];
                    $this->createAvatar($user, $url);
                }elseif(isset($userInfo['pic_1'])){
                    $url = $userInfo['pic_1'];
                    $this->createAvatar($user, $url);
                }
                Yii::$app->user->login($user,  0);
                return $user;

            }
            return null;
        }



    }


    /**
     * @param $userInfo
     * @return User|bool|null|\yii\web\IdentityInterface|static
     */
    private function vkLogin($userInfo)
    {
        $name = (isset($userInfo['first_name']))?$userInfo['first_name']:null;
        $surname = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $uid = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $phone = (isset($userInfo['home_phone']))?$userInfo['home_phone']:null;
        $phone = ($phone)?preg_replace('/\D/','',$phone):null;
        if(!$name && $surname)
            $name = $surname;
        elseif(!$name)
            $name = $uid;


        if(!$uid)
            return false;
        $access = md5(md5($uid).sha1($uid));
        $user = User::findIdentityByAccessToken($access);
        if($user){
            Yii::$app->user->login($user,  0);
            return $user;
        } else {
            $user = new User();
            $user->access_token = $access;
            $user->name = $name;
            $user->surname = $surname;
            if($user->save(false)){
                if(isset($userInfo['photo_big'])){
                    $url = $userInfo['photo_big'];
                    $this->createAvatar($user, $url);
                }elseif(isset($userInfo['photo_50'])){
                    $url = $userInfo['photo_50'];
                    $this->createAvatar($user, $url);
                }
                if($phone){
                    $country = (isset($userInfo['country']))?self::$countries[$userInfo['country']]:null;
                    $ph = new Phone();
                    if($country && is_numeric($country))
                        $ph->country_id = $country;
                    $ph->number = $phone;
                    $ph->save();
                }
                Yii::$app->user->login($user,  0);
                return $user;


            }

        }
        return false;
    }


    /**
     * @param $url
     * @param $path
     * @return bool
     */
    private function imageFromUrl($url, $path)
    {
        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $resource = curl_exec($ch);
        curl_close ($ch);
        $img = imagecreatefromstring($resource);
        return imagepng($img, $path);

    }

    /**
     * @param User $user
     * @param $url
     */
    private function createAvatar(User $user, $url)
    {
        $path = 'uploads/' . time() . '.png';
        if($this->imageFromUrl($url, $path)){
            $image = new Image();
            $image->path = $path;
            $image->title = $user->name;
            $image->description = 'This is image from social website';
            if($image->save()){
                $user->avatar = $image->id;
                $user->save(false);
            }
        }
    }

    /**
     * @param $token
     * @return User|bool|null|\yii\web\IdentityInterface|static
     * @throws FacebookSDKException
     */
    private function fb($token)
    {
        $appId = '156629614671462';
        if(session_id() == '')
            session_start();
        $fb = new Facebook([
            'app_id' => '156629614671462',
            'app_secret' => '48d6b092bdd909e9228dc2f1f0fb0d01',
            'default_graph_version' => 'v2.2',
        ]);




// The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($token);


// Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($appId);
// If you know the user ID this access token belongs to, you can validate it here

        $tokenMetadata->validateExpiration();


        $fb->setDefaultAccessToken($token);


        $response = $fb->get('/me?fields=email,first_name,last_name,name,picture');
        $userNode = $response->getGraphUser();
        if($user = $this->fbLogin($userNode))
            return $user;
        else{
            return false;
        }


    }


    /**
     * @param $userInfo
     * @return User|bool|null|\yii\web\IdentityInterface|static
     */
    private function fbLogin($userInfo)
    {
        $name = (isset($userInfo['first_name']))?$userInfo['first_name']:null;
        $surname = (isset($userInfo['last_name']))?$userInfo['last_name']:null;
        $uid = (isset($userInfo['id']))?$userInfo['id']:null;
        $picture = (isset($userInfo['picture']))?$userInfo['picture']:null;

        if(!$name && $surname)
            $name = $surname;
        elseif(!$name)
            $name = $uid;


        if(!$uid)
            $this->goHome();
        $access = md5(md5($uid).sha1($uid));
        $user = User::findIdentityByAccessToken($access);
        if($user){
            Yii::$app->user->login($user,  0);
            return $user;
        } else {
            $user = new User();
            $user->access_token = $access;
            $user->name = $name;
            $user->surname = $surname;
            if($user->save(false)){
                if($picture  instanceof GraphPicture){
                    $url = $picture->getUrl();
                    $this->createAvatar($user, $url);
                }

                Yii::$app->user->login($user,  0);
                return $user;
            }

        }
        return false;
    }

    /**
     * @param $token
     * @return User|AuthorizeController|bool|null|\yii\web\IdentityInterface|static
     */
    private function vk($token)
    {
        $client_id = '5032216'; // ID
        $client_secret = 'gBgtLR7EzIqcmw5W0hqq'; //
        $redirect_uri = 'http://ms.arcanas.eu/auth/vk/'; //


        if ($token) {

            $this->params = array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'code' => $_GET['code'],
                'redirect_uri' => $redirect_uri
            );

            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($this->params))), true);

            if (isset($token['access_token'])) {
                $this->params = array(
                    'uids'         => $token['user_id'],
                    'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big,contacts',
                    'access_token' => $token['access_token']
                );

                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($this->params))), true);

                if (isset($userInfo['response'][0]['uid'])) {
                    $userInfo = $userInfo['response'][0];
                    return $this->vkLogin($userInfo);

                }
            }
        }

        return false;


    }

    /**
     * @param $token
     * @return User|AuthorizeController|bool|null|\yii\web\IdentityInterface|static
     */
    private function ok($token)
    {
//        $client_id = '1150074368'; // Application ID
        $public_key = 'CBAPLNIFEBABABABA'; //
        $client_secret = '56670772AA5458E8C742107B'; //
//        $redirect_uri = 'http://ms.arcanas.eu/auth/ok/'; //

        if (isset($token) && isset($public_key)) {
            $sign = md5("application_key={$public_key}format=jsonmethod=users.getCurrentUser" . md5("{$token}{$client_secret}"));

            $params = array(
                'method' => 'users.getCurrentUser',
                'access_token' => $token,
                'application_key' => $public_key,
                'format' => 'json',
                'sig' => $sign
            );

            $userInfo = json_decode(file_get_contents('http://api.odnoklassniki.ru/fb.do' . '?' . urldecode(http_build_query($params))), true);
            if (isset($userInfo['uid'])) {

                return $this->okLogin($userInfo);

            }
        }
        return false;

    }

    /**
     * @param $user
     * @param $sid
     * @return array
     */
    private function createActiveMobileUser($user, $sid)
    {

        $active = new ActiveMobileUser();
        $active->user_id = $user->id;
        $active->access_token = $sid;
        $active->expired_at = time() + 3600;
        $active->validate();
        if($active->save()){
            //$this->cleanActive($active->id);
            return [
                'user' => $user,
                'PHPSESSID'  => $sid,
                'active'   => 'created',
            ];

        }

        return ['error' => 'error'];

    }

}
