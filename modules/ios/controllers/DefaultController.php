<?php

namespace app\modules\ios\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $this->goHome();

    }

}
