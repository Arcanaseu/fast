<?php

namespace app\modules\ios\controllers;

use app\models\User;
use app\modules\ios\models\ActiveMobileUser;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class UserController extends Controller
{
    public function actionIndex()
    {
        session_start();
        Yii::$app->user->getIdentity();
        $sid = session_id();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $params = \Yii::$app->getRequest()->getQueryParams();
        if(array_key_exists('PHPSESSID', $params) && $params['PHPSESSID'] == $sid){
            $active = ActiveMobileUser::find()->where(['access_token' => $sid])->one();
            if($active){
                if($user = $active->user){
                    return [
                        'user' => $user,
                        'PHPSESSID'  => session_id(),
                    ];
                }else{
                    return ['errors' => 'User not found'];
                }

            }


        } elseif($params['id']){
            $user = User::findOne($params['id']);
            if($user){
                return [
                    'user' => $user,
                    'PHPSESSID'  => session_id(),
                ];
            }else{
                return ['errors' => 'User not found'];
            }
        }
        return ['errors' => 'Bad request'];
    }

    public function actionUpdatedevicetoken()
    {
        session_start();
        Yii::$app->user->getIdentity();
        $sid = session_id();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $params = \Yii::$app->getRequest()->getQueryParams();
        if(array_key_exists('PHPSESSID', $params) && $params['PHPSESSID'] == $sid){
            $active = ActiveMobileUser::find()->where(['access_token' => $sid])->one();
            if($active && array_key_exists('device_token', $params)){
                $active->device_token = $params['device_token'];
                if($active->save()){
                    return ['success'];
                }
            }
        }
        return ['error'];
    }

}
