<?php

namespace app\modules\ios\models;

use app\models\User as MUser;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "active_mobile_user".
 *
 * @property string $id
 * @property string $user_id
 * @property string $access_token
 * @property string $expired_at
 * @property string device_token
 *
 * @property User $user
 */
class ActiveMobileUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'active_mobile_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'access_token'], 'required'],
            [['user_id'], 'integer'],
            [['expired_at'], 'safe'],
            [['access_token','device_token' ], 'string', 'max' => 255],
            [['user_id', 'access_token'], 'unique', 'targetAttribute' => ['user_id', 'access_token'], 'message' => 'The combination of User ID and Access Token has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'access_token' => 'Access Token',
            'expired_at' => 'Expired At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ActiveMobileUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActiveMobileUserQuery(get_called_class());
    }
}
