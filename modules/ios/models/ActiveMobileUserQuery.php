<?php

namespace app\modules\ios\models;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[ActiveMobileUser]].
 *
 * @see ActiveMobileUser
 */
class ActiveMobileUserQuery extends ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ActiveMobileUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ActiveMobileUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}