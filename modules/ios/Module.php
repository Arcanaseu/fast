<?php

namespace app\modules\ios;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\ios\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
