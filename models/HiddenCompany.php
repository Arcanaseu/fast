<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "hidden_company".
 *
 * @property string $id
 * @property string $creator_type
 * @property string $user_id
 * @property string $company_id
 * @property string $created
 * @property string $hidden_company_id
 *
 * @property Company $company
 * @property Company $hiddenCompany
 * @property User $user
 */
class HiddenCompany extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hidden_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_type'], 'string'],
            [['user_id', 'company_id', 'hidden_company_id'], 'integer'],
            [['created'], 'safe'],
            [['hidden_company_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_type' => 'Creator Type',
            'user_id' => 'User',
            'company_id' => 'Company',
            'created' => 'Created',
            'hidden_company_id' => 'Hidden Company',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'hidden_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
