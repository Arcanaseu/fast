<?php

namespace app\models;

use app\sms\Transport;
use Yii;
use yii\base\Model;

/**
 * RemindForm is the model behind the login form.
 */
class RemindForm extends Model
{
    public $username;
    public $email;
    public $phone;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'email', 'phone'], 'safe'],

        ];
    }

    public function getMessage(){

        if (!($user = User::findByUsername($this->username))){
            if (!($user = User::findByEmail($this->email))){
                $user = User::findByPhone($this->phone);
            }
        }
        if($user instanceof User){
            $phone = $user->getPhone();
            $pass = substr(md5(time()),0,8);
            $user->password = md5($pass);
            $user->save(false);
            $text = 'Your username: '.$user->username.',  password: '.$pass;
            (new Transport())->send(['text' => $text, 'source' => 'beermap'], [$phone]);
            $message = 'Your data was sent to phone';

        } else {
            $message = 'User with this data not found';
        }
        return $message;

    }


}
