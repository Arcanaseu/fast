<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_video".
 *
 * @property string $id
 * @property string $product_id
 * @property string $video_id
 *
 * @property Product $product
 * @property Video $video
 */
class ProductVideo extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'video_id'], 'required'],
            [['product_id', 'video_id'], 'integer'],
            [['product_id', 'video_id'], 'unique', 'targetAttribute' => ['product_id', 'video_id'], 'message' => 'The combination of Product ID and Video ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product',
            'video_id' => 'Video',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }
}
