<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "hidden".
 *
 * @property string $id
 * @property string $creator_type
 * @property string $user_id
 * @property string $company_id
 * @property string $created
 * @property string $events_id
 *
 * @property Company $company
 * @property Events $events
 * @property User $user
 */
class Hidden extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hidden';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_type'], 'string'],
            [['user_id', 'company_id', 'events_id'], 'integer'],
            [['created'], 'safe'],
            [['events_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_type' => 'Creator Type',
            'user_id' => 'User',
            'company_id' => 'Company',
            'created' => 'Created',
            'events_id' => 'Events',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
