<?php

namespace app\models;


use Yii;
use yii\base\Model;



/**
 * UnfriendForm is the model behind the login form.
 */
class UnfriendForm extends Model
{
    public $friendId;
    public $userId;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['friendId','userId'], 'required'],
        ];
    }

    public function unfriend(){
        if(!(Yii::$app->user->id == intval($this->userId))){
            return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
        }
        $un = $friends  = Friends::find()
            ->where(['initiator_id' => Yii::$app->user->id, 'acceptor_id' => $this->friendId ])->one();
        if($un)
            $un->delete();
        $un = $friends  = Friends::find()
            ->where(['acceptor_id' => Yii::$app->user->id, 'initiator_id' => $this->friendId ])->one();
        if($un)
            $un->delete();

    }



}
