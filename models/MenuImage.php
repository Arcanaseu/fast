<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "menu_image".
 *
 * @property string $id
 * @property string $menu_id
 * @property string $image_id
 *
 * @property Image $image
 * @property Menu $menu
 */
class MenuImage extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'image_id'], 'required'],
            [['menu_id', 'image_id'], 'integer'],
            [['menu_id', 'image_id'], 'unique', 'targetAttribute' => ['menu_id', 'image_id'], 'message' => 'The combination of Menu ID and Image ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu',
            'image_id' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
