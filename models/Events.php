<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "events".
 *
 * @property string $id
 * @property string $type
 * @property string $avatar
 * @property string $title
 * @property string $description
 * @property string $text
 * @property string $created
 * @property integer $is_closed
 * @property integer $is_expired
 * @property string $date
 * @property string $creator_user_id
 * @property string $creator_company_id
 * @property string $creator_type
 * @property double $lat
 * @property double $lon
 *
 * @property AcceptedCompany[] $acceptedCompanies
 * @property AcceptedUser[] $acceptedUsers
 * @property Image $avatar0
 * @property Company $creatorCompany
 * @property User $creatorUser
 * @property EventsImage[] $eventsImages
 * @property EventsVideo[] $eventsVideos
 * @property GroupEvents[] $groupEvents
 * @property Hidden[] $hiddens
 * @property Like[] $likes
 * @property MentionEvents[] $mentionEvents
 * @property RatingEvents $ratingEvents
 * @property Repost[] $reposts
 */
class Events extends BaseModel
{

    /**
     * @var UploadedFile
     */
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'text', 'creator_type'], 'string'],
            [['avatar', 'is_closed', 'is_expired', 'creator_user_id', 'creator_company_id'], 'integer'],
            [['created', 'date'], 'safe'],
            [['lat', 'lon'], 'required'],
            [['lat', 'lon'], 'number'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024],
            [['file'], 'file',  'extensions' => 'png, jpg,jpeg,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'avatar' => 'Avatar',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
            'created' => 'Created',
            'is_closed' => 'Is Closed',
            'is_expired' => 'Is Expired',
            'date' => 'Date',
            'creator_user_id' => 'Creator User',
            'creator_company_id' => 'Creator Company',
            'creator_type' => 'Creator Type',
            'lat' => 'Lat',
            'lon' => 'Lon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedCompanies()
    {
        return $this->hasMany(AcceptedCompany::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedUsers()
    {
        return $this->hasMany(AcceptedUser::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar0()
    {
        return $this->hasOne(Image::className(), ['id' => 'avatar']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatorCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'creator_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatorUser()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsImages()
    {
        return $this->hasMany(EventsImage::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsVideos()
    {
        return $this->hasMany(EventsVideo::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupEvents()
    {
        return $this->hasMany(GroupEvents::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddens()
    {
        return $this->hasMany(Hidden::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Like::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionEvents()
    {
        return $this->hasMany(MentionEvents::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingEvents()
    {
        return $this->hasOne(RatingEvents::className(), ['events_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReposts()
    {
        return $this->hasMany(Repost::className(), ['events_id' => 'id']);
    }
}
