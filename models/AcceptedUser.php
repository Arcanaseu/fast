<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "accepted_user".
 *
 * @property string $id
 * @property string $events_id
 * @property string $user_id
 *
 * @property Events $events
 * @property User $user
 */
class AcceptedUser extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accepted_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'user_id'], 'required'],
            [['events_id', 'user_id'], 'integer'],
            [['events_id', 'user_id'], 'unique', 'targetAttribute' => ['events_id', 'user_id'], 'message' => 'The combination of Events ID and User ID has already been taken.']
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'events_id' => 'Events',
            'user_id' => 'User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
