<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "menu".
 *
 * @property string $id
 * @property string $company_id
 * @property string $title
 * @property string $description
 *
 * @property Company $company
 * @property MenuCategory[] $menuCategories
 * @property MenuImage[] $menuImages
 * @property MenuVideo[] $menuVideos
 */
class Menu extends BaseModel
{
    /**
     * @var UploadedFile
     */
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'title', 'description'], 'required'],
            [['company_id'], 'integer'],
            [['company_id'], 'unique'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024],
            [['file'], 'file',  'extensions' => 'png, jpg,jpeg,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuCategories()
    {
        return $this->hasMany(MenuCategory::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuImages()
    {
        return $this->hasMany(MenuImage::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuVideos()
    {
        return $this->hasMany(MenuVideo::className(), ['menu_id' => 'id']);
    }
}
