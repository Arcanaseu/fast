<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rating_mention_user".
 *
 * @property string $id
 * @property string $creator_id
 * @property string $target_id
 * @property integer $rate
 *
 * @property MentionUser $target
 * @property User $creator
 */
class RatingMentionUser extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating_mention_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'target_id', 'rate'], 'required'],
            [['creator_id', 'target_id', 'rate'], 'integer'],
            [['creator_id', 'target_id'], 'unique', 'targetAttribute' => ['creator_id', 'target_id'], 'message' => 'The combination of Creator ID and Target ID has already been taken.'],
            [['creator_id', 'target_id'], 'unique', 'targetAttribute' => ['creator_id', 'target_id'], 'message' => 'The combination of Creator ID and Target ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_id' => 'Creator',
            'target_id' => 'Target',
            'rate' => 'Rate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(MentionUser::className(), ['id' => 'target_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }
}
