<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company_image".
 *
 * @property string $id
 * @property string $company_id
 * @property string $image_id
 *
 * @property Company $company
 * @property Image $image
 */
class CompanyImage extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'image_id'], 'required'],
            [['company_id', 'image_id'], 'integer'],
            [['company_id', 'image_id'], 'unique', 'targetAttribute' => ['company_id', 'image_id'], 'message' => 'The combination of Company ID and Image ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'image_id' => 'Image',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }
}
