<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "hidden_group".
 *
 * @property string $id
 * @property string $creator_type
 * @property string $user_id
 * @property string $company_id
 * @property string $created
 * @property string $hidden_group_id
 *
 * @property Company $company
 * @property Group $hiddenGroup
 * @property User $user
 */
class HiddenGroup extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hidden_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_type'], 'string'],
            [['user_id', 'company_id', 'hidden_group_id'], 'integer'],
            [['created'], 'safe'],
            [['hidden_group_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_type' => 'Creator Type',
            'user_id' => 'User',
            'company_id' => 'Company',
            'created' => 'Created',
            'hidden_group_id' => 'Hidden Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'hidden_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
