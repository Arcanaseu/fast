<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "like_product".
 *
 * @property string $id
 * @property string $creator_type
 * @property string $user_id
 * @property string $company_id
 * @property string $created
 * @property string $target_id
 *
 * @property Company $company
 * @property Product $target
 * @property User $user
 */
class LikeProduct extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'like_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_type'], 'string'],
            [['user_id', 'company_id', 'target_id'], 'integer'],
            [['created'], 'safe'],
            [['target_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_type' => 'Creator Type',
            'user_id' => 'User',
            'company_id' => 'Company',
            'created' => 'Created',
            'target_id' => 'Target',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(Product::className(), ['id' => 'target_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
