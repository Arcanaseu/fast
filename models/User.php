<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use developeruz\db_rbac\interfaces\UserRbacInterface;
use yii\base\NotSupportedException;


/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $name
 * @property string $surname
 * @property string $password
 * @property string $access_token
 * @property string $city
 * @property string $address
 * @property string $avatar
 * @property string $phone
 * @property string $email
 * @property string $gender
 * @property double $lat
 * @property double $lon
 *
 * @property AcceptedUser[] $acceptedUsers
 * @property Events[] $events
 * @property Friends[] $friends
 * @property Friends[] $friends0
 * @property GroupAdministrator[] $groupAdministrators
 * @property GroupUser[] $groupUsers
 * @property Hidden[] $hiddens
 * @property HiddenCompany[] $hiddenCompanies
 * @property HiddenGroup[] $hiddenGroups
 * @property HiddenUser[] $hiddenUsers
 * @property HiddenUser[] $hiddenUsers0
 * @property Like[] $likes
 * @property LikeCompany[] $likeCompanies
 * @property LikeProduct[] $likeProducts
 * @property MentionCompany[] $mentionCompanies
 * @property MentionEvents[] $mentionEvents
 * @property MentionProduct[] $mentionProducts
 * @property MentionUser[] $mentionUsers
 * @property MentionUser[] $mentionUsers0
 * @property Message[] $messages
 * @property Message[] $messages0
 * @property Message[] $messages1
 * @property MessageModerator[] $messageModerators
 * @property RatingMentionCompany[] $ratingMentionCompanies
 * @property RatingMentionEvents[] $ratingMentionEvents
 * @property RatingMentionProduct[] $ratingMentionProducts
 * @property RatingMentionUser[] $ratingMentionUsers
 * @property RatingUser $ratingUser
 * @property Repost[] $reposts
 * @property Request[] $requests
 * @property Request[] $requests0
 * @property Subscribers[] $subscribers
 * @property Subscribers[] $subscribers0
 * @property SubscribersCompany[] $subscribersCompanies
 * @property SubscribersProduct[] $subscribersProducts
 * @property City $city0
 * @property Phone $phone0
 * @property Image $avatar0
 * @property UserCompany[] $userCompanies
 * @property UserImage[] $userImages
 * @property UserPhone[] $userPhones
 * @property UserVideo[] $userVideos
 * @property Company $company
 */
class User extends BaseModel implements IdentityInterface, UserRbacInterface
{



    public $authKey = 'a7656fafe94dae72b1e1487670148412';//secret key





    /**
     * @var UploadedFile
     */
    public $file;


    public function getUserName()
    {
        return $this->username;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }



    /**
     * Finds user by username
     *
     * @param  string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {

        return static::findOne(['username' => $username]);
    }

    /**
     * @param $email
     * @return null|static
     */
    public static function findByEmail($email){
        return static::findOne(['email' => $email]);
    }

    public static function findByPhone($phone){
        return static::findOne(['phone' => $phone]);
    }

    public  function getPhone(){
        $phone = $this->phone0->number;
        $countryCode = $this->phone0->country->phone_code;
        $phone = '+'.$countryCode.$phone;

        return $phone;
    }





    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['city', 'avatar', 'phone'], 'integer'],
            [['lat', 'lon'], 'number'],
            [[
                'name',
                'surname',
                'address',
                'username',
                'access_token',
                'password',
                'gender'
            ], 'string', 'max' => 255],
            [['email'], 'email'],
            [['username','email'], 'unique'],
            [['file'], 'file', 'extensions' => 'png, jpg,jpeg,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'city' => 'City',
            'address' => 'Address',
            'avatar' => 'Avatar',
            'phone' => 'Phone',
            'email' => 'Email',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'username' => 'Username',
            'access_token' => 'Access Token',
            'password' => 'Password',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedUsers()
    {
        return $this->hasMany(AcceptedUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['creator_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriends()
    {
        return $this->hasMany(Friends::className(), ['acceptor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriends0()
    {
        return $this->hasMany(Friends::className(), ['initiator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupAdministrators()
    {
        return $this->hasMany(GroupAdministrator::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupUsers()
    {
        return $this->hasMany(GroupUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddens()
    {
        return $this->hasMany(Hidden::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenCompanies()
    {
        return $this->hasMany(HiddenCompany::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenGroups()
    {
        return $this->hasMany(HiddenGroup::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenUsers()
    {
        return $this->hasMany(HiddenUser::className(), ['hidden_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenUsers0()
    {
        return $this->hasMany(HiddenUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Like::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeCompanies()
    {
        return $this->hasMany(LikeCompany::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeProducts()
    {
        return $this->hasMany(LikeProduct::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionCompanies()
    {
        return $this->hasMany(MentionCompany::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionEvents()
    {
        return $this->hasMany(MentionEvents::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionProducts()
    {
        return $this->hasMany(MentionProduct::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionUsers()
    {
        return $this->hasMany(MentionUser::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionUsers0()
    {
        return $this->hasMany(MentionUser::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages0()
    {
        return $this->hasMany(Message::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages1()
    {
        return $this->hasMany(Message::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageModerators()
    {
        return $this->hasMany(MessageModerator::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingMentionCompanies()
    {
        return $this->hasMany(RatingMentionCompany::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingMentionEvents()
    {
        return $this->hasMany(RatingMentionEvents::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingMentionProducts()
    {
        return $this->hasMany(RatingMentionProduct::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingMentionUsers()
    {
        return $this->hasMany(RatingMentionUser::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingUser()
    {
        return $this->hasOne(RatingUser::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReposts()
    {
        return $this->hasMany(Repost::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['acceptor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests0()
    {
        return $this->hasMany(Request::className(), ['initiator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribers()
    {
        return $this->hasMany(Subscribers::className(), ['subscriber_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribers0()
    {
        return $this->hasMany(Subscribers::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribersCompanies()
    {
        return $this->hasMany(SubscribersCompany::className(), ['subscriber_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribersProducts()
    {
        return $this->hasMany(SubscribersProduct::className(), ['subscriber_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity0()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhone0()
    {
        return $this->hasOne(Phone::className(), ['id' => 'phone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar0()
    {
        return $this->hasOne(Image::className(), ['id' => 'avatar']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanies()
    {
        return $this->hasMany(UserCompany::className(), ['user_id' => 'id']);
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        $uc = $this->getUserCompanies()->one();
        if($uc instanceof UserCompany){
            return $uc->company;
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserImages()
    {
        return $this->hasMany(UserImage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPhones()
    {
        return $this->hasMany(UserPhone::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVideos()
    {
        return $this->hasMany(UserVideo::className(), ['user_id' => 'id']);
    }

    public function getGroups()
    {
        $groups = [];
        /** @var GroupUser $gu */
        foreach ($this->groupUsers as $gu) {
            $groups[] = $gu->group;
        }

        return $groups;
    }
}
