<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "request".
 *
 * @property string $id
 * @property string $initiator_id
 * @property string $acceptor_id
 * @property string $created
 * @property string $type
 * @property integer $is_realized
 *
 * @property User $initiator
 * @property User $acceptor
 */
class Request extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initiator_id', 'acceptor_id', 'type'], 'required'],
            [['initiator_id', 'acceptor_id', 'is_realized'], 'integer'],
            [['created'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['initiator_id', 'acceptor_id'], 'unique', 'targetAttribute' => ['initiator_id', 'acceptor_id'], 'message' => 'The combination of Initiator ID and Acceptor ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'initiator_id' => 'Initiator',
            'acceptor_id' => 'Acceptor',
            'created' => 'Created',
            'type' => 'Type',
            'is_realized' => 'Is Realized',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInitiator()
    {
        return $this->hasOne(User::className(), ['id' => 'initiator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptor()
    {
        return $this->hasOne(User::className(), ['id' => 'acceptor_id']);
    }
}
