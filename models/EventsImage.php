<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "events_image".
 *
 * @property string $id
 * @property string $events_id
 * @property string $image_id
 *
 * @property Image $image
 * @property Events $events
 */
class EventsImage extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'image_id'], 'required'],
            [['events_id', 'image_id'], 'integer'],
            [['events_id', 'image_id'], 'unique', 'targetAttribute' => ['events_id', 'image_id'], 'message' => 'The combination of Events ID and Image ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'events_id' => 'Events',
            'image_id' => 'Image',
        ];
    }







    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }
}
