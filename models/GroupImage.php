<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "group_image".
 *
 * @property string $id
 * @property string $group_id
 * @property string $image_id
 *
 * @property Image $image
 * @property Group $group
 */
class GroupImage extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'image_id'], 'required'],
            [['group_id', 'image_id'], 'integer'],
            [['group_id', 'image_id'], 'unique', 'targetAttribute' => ['group_id', 'image_id'], 'message' => 'The combination of Group ID and Image ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group',
            'image_id' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }
}
