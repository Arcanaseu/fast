<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_kitchen".
 *
 * @property string $id
 * @property string $product_id
 * @property string $kitchen_id
 *
 * @property Product $product
 * @property Kitchen $kitchen
 */
class ProductKitchen extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_kitchen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'kitchen_id'], 'required'],
            [['product_id', 'kitchen_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product',
            'kitchen_id' => 'Kitchen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitchen()
    {
        return $this->hasOne(Kitchen::className(), ['id' => 'kitchen_id']);
    }
}
