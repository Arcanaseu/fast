<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "message_moderator".
 *
 * @property string $id
 * @property string $creator_id
 * @property string $created
 * @property string $text
 */
class MessageModerator extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_moderator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'text'], 'required'],
            [['creator_id'], 'integer'],
            [['created'], 'safe'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_id' => 'Creator',
            'created' => 'Created',
            'text' => 'Text',
        ];
    }
}
