<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_metro".
 *
 * @property string $id
 * @property string $company_id
 * @property string $metro_id
 *
 * @property Company $company
 * @property Metro $metro
 */
class CompanyMetro extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'metro_id'], 'required'],
            [['company_id', 'metro_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'metro_id' => 'Metro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetro()
    {
        return $this->hasOne(Metro::className(), ['id' => 'metro_id']);
    }
}
