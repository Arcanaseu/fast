<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rating_product".
 *
 * @property string $id
 * @property string $product_id
 * @property string $score
 * @property string $voted
 *
 * @property Product $product
 */
class RatingProduct extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'score', 'voted'], 'required'],
            [['product_id', 'score', 'voted'], 'integer'],
            [['product_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product',
            'score' => 'Score',
            'voted' => 'Voted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
