<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rating_events".
 *
 * @property string $id
 * @property string $events_id
 * @property string $score
 * @property string $voted
 *
 * @property Events $events
 */
class RatingEvents extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating_events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'score', 'voted'], 'required'],
            [['events_id', 'score', 'voted'], 'integer'],
            [['events_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'events_id' => 'Events',
            'score' => 'Score',
            'voted' => 'Voted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }
}
