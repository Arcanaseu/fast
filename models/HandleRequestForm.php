<?php

namespace app\models;


use Yii;
use yii\base\Model;



/**
 * HandleRequestForm is the model behind the login form.
 */
class HandleRequestForm extends Model
{
    public $requestId;
    public $userId;
    public $decision;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['requestId','decision', 'userId'], 'required'],
        ];
    }

    public function handle(){

        if(!(Yii::$app->user->id == intval($this->userId))){
            return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
        }
        $id = intval($this->requestId);
        /** @var Request $request */
        $request =  Request::findOne($id);
        if($request instanceof Request && $this->decision == 1){
            $friend = new Friends();
            $friend->initiator_id = $request->initiator_id;
            $friend->acceptor_id  = $request->acceptor_id;
            if($friend->save()){
                $request->delete();
                return true;
            }
        }elseif($request instanceof Request && $this->decision == 0){
            $request->delete();
        }
        return false;

    }



}
