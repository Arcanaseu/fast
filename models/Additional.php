<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "additional".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $image_id
 *
 * @property Image $image
 * @property AdditionalCompany[] $additionalCompanies
 * @property AdditionalProduct[] $additionalProducts
 */
class Additional extends BaseModel
{

    /**
     * @var UploadedFile
     */
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'additional';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024],
            [['file'], 'file',  'extensions' => 'png, jpg,jpeg,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image_id' => 'Image',
            'file'  => 'New Image'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalCompanies()
    {
        return $this->hasMany(AdditionalCompany::className(), ['additional_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalProducts()
    {
        return $this->hasMany(AdditionalProduct::className(), ['additional_id' => 'id']);
    }
}
