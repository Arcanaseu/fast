<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "reg_user".
 *
 * @property string $id
 * @property string $username
 * @property string $email
 * @property string $country
 * @property string $phone
 * @property string $gender
 * @property string $checking
 * @property string $session
 * @property string $password
 * @property string $expired_at
 * @property string $path
 * @property string $title
 * @property string $name
 * @property string $surname
 */
class RegUser extends BaseModel
{


    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var UploadedFile
     */
    public $photo;


    public $code;
    public $sms_code;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reg_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'country', 'phone',   'password'], 'required'],
            [['country', 'phone', 'expired_at'], 'integer'],
            [['email'], 'email'],
            [['gender','checking', 'path', 'title', 'session','username', 'sms_code'], 'safe'],
            [['username', 'email', 'gender', 'checking', 'session', 'password', 'path', 'title', 'name', 'surname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Login',
            'email' => 'Email',
            'country' => 'Country',
            'phone' => 'Phone',
            'gender' => 'Gender',
            'checking' => 'Check',
            'session' => 'Session',
            'password' => 'Password',
            'expired_at' => 'Expired At',
            'path' => 'Path',
            'title' => 'Title',
            'file'  => 'Avatar',
            'photo'  => 'Avatar',
            'name' => 'Name',
            'surname' => 'Surname',
        ];
    }

    /**
     * @inheritdoc
     * @return RegUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RegUserQuery(get_called_class());
    }



    public  function getPhone(){

        return '+'.$this->country.$this->phone;
    }
}
