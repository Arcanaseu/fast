<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property string $avatar
 * @property string $category_id
 * @property string $type_id
 * @property string $brand_id
 * @property string $sort_id
 * @property string $packing_id
 * @property string $country_id
 * @property string $fortress_id
 * @property string $brewery_id
 *
 * @property AdditionalProduct[] $additionalProducts
 * @property LikeProduct[] $likeProducts
 * @property MentionProduct[] $mentionProducts
 * @property MenuCategoryItem[] $menuCategoryItems
 * @property ProductBrewery $brewery
 * @property Country $country
 * @property ProductFortress $fortress
 * @property Image $avatar0
 * @property ProductBrand $brand
 * @property ProductCategory $category
 * @property ProductPacking $packing
 * @property ProductSort $sort
 * @property ProductType $type
 * @property ProductImage[] $productImages
 * @property ProductVideo[] $productVideos
 * @property RatingProduct $ratingProduct
 * @property SubscribersProduct[] $subscribersProducts
 */
class Product extends BaseModel
{
    /**
     * @var UploadedFile
     */
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['avatar', 'category_id', 'type_id', 'brand_id', 'sort_id', 'packing_id', 'country_id', 'fortress_id', 'brewery_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['text'], 'string'],
            [['description'], 'string', 'max' => 1024],
            [['file'], 'file',  'extensions' => 'png, jpg,jpeg,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
            'avatar' => 'Avatar',
            'category_id' => 'Category',
            'type_id' => 'Type',
            'brand_id' => 'Brand',
            'sort_id' => 'Sort',
            'packing_id' => 'Packing',
            'country_id' => 'Country',
            'fortress_id' => 'Fortress',
            'brewery_id' => 'Brewery',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalProducts()
    {
        return $this->hasMany(AdditionalProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeProducts()
    {
        return $this->hasMany(LikeProduct::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionProducts()
    {
        return $this->hasMany(MentionProduct::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuCategoryItems()
    {
        return $this->hasMany(MenuCategoryItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar0()
    {
        return $this->hasOne(Image::className(), ['id' => 'avatar']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(ProductBrand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacking()
    {
        return $this->hasOne(ProductPacking::className(), ['id' => 'packing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSort()
    {
        return $this->hasOne(ProductSort::className(), ['id' => 'sort_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVideos()
    {
        return $this->hasMany(ProductVideo::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingProduct()
    {
        return $this->hasOne(RatingProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribersProducts()
    {
        return $this->hasMany(SubscribersProduct::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFortress()
    {
        return $this->hasOne(ProductFortress::className(), ['id' => 'fortress_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrewery()
    {
        return $this->hasOne(ProductBrewery::className(), ['id' => 'brewery_id']);
    }

}
