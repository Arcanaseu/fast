<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "metro".
 *
 * @property string $id
 * @property string $metro
 * @property double $lat
 * @property double $lon
 *
 * @property CompanyMetro[] $companyMetros
 */
class Metro extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metro'], 'required'],
            [['lat', 'lon'], 'number'],
            [['metro'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'metro' => 'Metro',
            'lat' => 'Lat',
            'lon' => 'Lon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMetros()
    {
        return $this->hasMany(CompanyMetro::className(), ['metro_id' => 'id']);
    }
}
