<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company_video".
 *
 * @property string $id
 * @property string $company_id
 * @property string $video_id
 *
 * @property Company $company
 * @property Video $video
 */
class CompanyVideo extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'video_id'], 'required'],
            [['company_id', 'video_id'], 'integer'],
            [['company_id', 'video_id'], 'unique', 'targetAttribute' => ['company_id', 'video_id'], 'message' => 'The combination of Company ID and Video ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'video_id' => 'Video',
        ];
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }
}
