<?php

namespace app\models;

use Yii;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "accepted_company".
 *
 * @property string $id
 * @property string $events_id
 * @property string $company_id
 *
 * @property Events $events
 * @property Company $company
 */
class AcceptedCompany extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accepted_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'company_id'], 'required'],
            [['events_id', 'company_id'], 'integer'],
            [['events_id', 'company_id'], 'unique', 'targetAttribute' => ['events_id', 'company_id'], 'message' => 'The combination of Events ID and Company ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'events_id' => 'Events',
            'company_id' => 'Company',
        ];
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
