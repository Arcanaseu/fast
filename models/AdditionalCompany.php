<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "additional_company".
 *
 * @property string $id
 * @property string $additional_id
 * @property string $company_id
 *
 * @property Additional $additional
 * @property Company $company
 */
class AdditionalCompany extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'additional_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['additional_id', 'company_id'], 'required'],
            [['additional_id', 'company_id'], 'integer'],
            [['additional_id'],
                'unique',
                'targetAttribute' => ['additional_id', 'company_id'],
                'message' => 'Additional is already used by this company!!!!!',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'additional_id' => 'Additional',
            'company_id' => 'Company',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditional()
    {
        return $this->hasOne(Additional::className(), ['id' => 'additional_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
