<?php

namespace app\models;

use app\sms\Transport;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * RegistrationForm is the model behind the login form.
 */
class RegistrationForm extends Model
{
    public $username;
    public $password;
    public $email;
    public $country;
    public $phone;
    public $gender;
    public $check;
    public $path;
    public $title;
    /**
     * @var UploadedFile
     */
    public $file;






    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['password', 'email', 'phone', 'country'], 'required'],
            [['gender','check', 'path', 'title', 'username'], 'safe'],
            [['phone'], 'integer']
        ];
    }



}
