<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_phone".
 *
 * @property string $id
 * @property string $company_id
 * @property string $phone_id
 *
 * @property Company $company
 * @property Phone $phone
 */
class CompanyPhone extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'phone_id'], 'required'],
            [['company_id', 'phone_id'], 'integer'],
            [['company_id', 'phone_id'], 'unique', 'targetAttribute' => ['company_id', 'phone_id'], 'message' => 'The combination of Company ID and Phone ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'phone_id' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhone()
    {
        return $this->hasOne(Phone::className(), ['id' => 'phone_id']);
    }
}
