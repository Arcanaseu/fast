<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = false;


    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            ['check', 'safe'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if($log = Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0)){
            if(!Yii::$app->user->isGuest){
                $userId = \Yii::$app->user->id;
                if(\Yii::$app->authManager->checkAccess($userId, 'admin')){
                    $user = User::findOne($userId);
                    if($user instanceof User){
                        $company = $user->getCompany();
                        if($company instanceof Company){
                            Yii::$app->session->set('company', $company->id);

                        }
                    }
                }

            }
        }
        return $log;


    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }






    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    private function getUserFromSession()
    {
        $this->_user = User::findByUsername(Yii::$app->session->get('username'));
        $this->rememberMe = Yii::$app->session->get('remember_me');
        return $this->_user;

    }
}
