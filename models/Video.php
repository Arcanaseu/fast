<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "video".
 *
 * @property string $id
 * @property string $path
 * @property string $created
 * @property string $title
 * @property string $description
 *
 * @property CompanyVideo[] $companyVideos
 * @property EventsVideo[] $eventsVideos
 * @property GroupVideo[] $groupVideos
 * @property MenuVideo[] $menuVideos
 * @property ProductVideo[] $productVideos
 * @property UserVideo[] $userVideos
 */
class Video extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'title', 'description'], 'required'],
            [['created'], 'safe'],
            [['path', 'title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'created' => 'Created',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyVideos()
    {
        return $this->hasMany(CompanyVideo::className(), ['video_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsVideos()
    {
        return $this->hasMany(EventsVideo::className(), ['video_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupVideos()
    {
        return $this->hasMany(GroupVideo::className(), ['video_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuVideos()
    {
        return $this->hasMany(MenuVideo::className(), ['video_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVideos()
    {
        return $this->hasMany(ProductVideo::className(), ['video_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVideos()
    {
        return $this->hasMany(UserVideo::className(), ['video_id' => 'id']);
    }
}
