<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_brewery".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 *
 * @property Product[] $products
 */
class ProductBrewery extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_brewery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['brewery_id' => 'id']);
    }
}
