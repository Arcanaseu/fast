<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_phone".
 *
 * @property string $id
 * @property string $user_id
 * @property string $phone_id
 *
 * @property User $user
 * @property Phone $phone
 */
class UserPhone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'phone_id'], 'required'],
            [['user_id', 'phone_id'], 'integer'],
            [['user_id', 'phone_id'], 'unique', 'targetAttribute' => ['user_id', 'phone_id'], 'message' => 'The combination of User ID and Phone ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'phone_id' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhone()
    {
        return $this->hasOne(Phone::className(), ['id' => 'phone_id']);
    }
}
