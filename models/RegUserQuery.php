<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RegUser]].
 *
 * @see RegUser
 */
class RegUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RegUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RegUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}