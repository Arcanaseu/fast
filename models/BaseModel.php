<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 22.07.2015
 * Time: 18:17
 */

namespace app\models;



use ReflectionClass;
use yii\db\ActiveRecord;

class BaseModel  extends ActiveRecord
{
    protected $shortClassName;

    /**
     * @return mixed
     */
    public function getShortClassName(){
        return (new ReflectionClass($this))->getShortName();
    }
}