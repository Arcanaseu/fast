<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "meta".
 *
 * @property string $id
 * @property string $title
 * @property string $key
 * @property string $description
 */
class Meta extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'key', 'description'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['key', 'description'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'key' => 'Key',
            'description' => 'Description',
        ];
    }
}
