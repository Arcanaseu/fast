<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "menu_video".
 *
 * @property string $id
 * @property string $menu_id
 * @property string $video_id
 *
 * @property Video $video
 * @property Menu $menu
 */
class MenuVideo extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'menu_id', 'video_id'], 'required'],
            [['id', 'menu_id', 'video_id'], 'integer'],
            [['menu_id', 'video_id'], 'unique', 'targetAttribute' => ['menu_id', 'video_id'], 'message' => 'The combination of Menu ID and Video ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu',
            'video_id' => 'Video',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }
}
