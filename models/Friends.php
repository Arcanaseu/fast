<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "friends".
 *
 * @property string $id
 * @property string $initiator_id
 * @property string $acceptor_id
 *
 * @property User $initiator
 * @property User $acceptor
 */
class Friends extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friends';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initiator_id', 'acceptor_id'], 'required'],
            [['initiator_id', 'acceptor_id'], 'integer'],
            [['initiator_id', 'acceptor_id'], 'unique', 'targetAttribute' => ['initiator_id', 'acceptor_id'], 'message' => 'The combination of Initiator ID and Acceptor ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'initiator_id' => 'Initiator',
            'acceptor_id' => 'Acceptor',
        ];
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInitiator()
    {
        return $this->hasOne(User::className(), ['id' => 'initiator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptor()
    {
        return $this->hasOne(User::className(), ['id' => 'acceptor_id']);
    }
}
