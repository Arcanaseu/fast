<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "menu_category_item".
 *
 * @property string $id
 * @property string $product_id
 * @property string $category_id
 * @property string $price
 * @property string $discount
 * @property string $discount_type
 *
 * @property MenuCategory $category
 * @property Product $product
 */
class MenuCategoryItem extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_category_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id', 'price'], 'required'],
            [['product_id', 'category_id', 'price', 'discount'], 'integer'],
            [['discount_type'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product',
            'category_id' => 'Category',
            'price' => 'Price',
            'discount' => 'Discount',
            'discount_type' => 'Discount Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(MenuCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
