<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "events_video".
 *
 * @property string $id
 * @property string $events_id
 * @property string $video_id
 *
 * @property Video $video
 * @property Events $events
 */
class EventsVideo extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['events_id', 'video_id'], 'required'],
            [['events_id', 'video_id'], 'integer'],
            [['events_id', 'video_id'], 'unique', 'targetAttribute' => ['events_id', 'video_id'], 'message' => 'The combination of Events ID and Video ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'events_id' => 'Events',
            'video_id' => 'Video',
        ];
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }
}
