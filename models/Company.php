<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "company".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property string $avatar
 * @property string $address
 * @property string $opened
 * @property string $city_id
 * @property string $average_score
 * @property string $email
 * @property string $site
 * @property double $lat
 * @property double $lon
 *
 * @property AcceptedCompany[] $acceptedCompanies
 * @property AdditionalCompany[] $additionalCompanies
 * @property Image $avatar0
 * @property City $city
 * @property CompanyKitchen[] $companyKitchens
 * @property CompanyImage[] $companyImages
 * @property CompanyMetro[] $companyMetros
 * @property CompanyPhone[] $companyPhones
 * @property CompanyVideo[] $companyVideos
 * @property Events[] $events
 * @property GroupCompany[] $groupCompanies
 * @property Hidden[] $hiddens
 * @property HiddenCompany[] $hiddenCompanies
 * @property HiddenCompany[] $hiddenCompanies0
 * @property HiddenGroup[] $hiddenGroups
 * @property HiddenUser[] $hiddenUsers
 * @property Like[] $likes
 * @property LikeCompany[] $likeCompanies
 * @property LikeCompany[] $likeCompanies0
 * @property LikeProduct[] $likeProducts
 * @property MentionCompany[] $mentionCompanies
 * @property Menu $menu
 * @property RatingCompany $ratingCompany
 * @property Repost[] $reposts
 * @property SubscribersCompany[] $subscribersCompanies
 * @property UserCompany[] $userCompanies
 * @property User[] $users
 */
class Company extends BaseModel
{


    /**
     * @var UploadedFile
     */
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'safe', 'on' => 'search'],
            [['text'], 'string'],
            [['avatar', 'city_id', 'average_score'], 'integer'],
            [['lat', 'lon'], 'number'],
            [['title', 'address', 'email', 'site', 'opened'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024],
            [['file'], 'file',  'extensions' => 'png, jpg,jpeg,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
            'avatar' => 'Avatar',
            'address' => 'Address',
            'opened' => 'Opened',
            'city_id' => 'City',
            'average_score' => 'Average Score',
            'email' => 'Email',
            'site' => 'Site',
            'lat' => 'Lat',
            'lon' => 'Lon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedCompanies()
    {
        return $this->hasMany(AcceptedCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalCompanies()
    {
        return $this->hasMany(AdditionalCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar0()
    {
        return $this->hasOne(Image::className(), ['id' => 'avatar']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyKitchens()
    {
        return $this->hasMany(CompanyKitchen::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyImages()
    {
        return $this->hasMany(CompanyImage::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMetros()
    {
        return $this->hasMany(CompanyMetro::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyPhones()
    {
        return $this->hasMany(CompanyPhone::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyVideos()
    {
        return $this->hasMany(CompanyVideo::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['creator_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupCompanies()
    {
        return $this->hasMany(GroupCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddens()
    {
        return $this->hasMany(Hidden::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenCompanies()
    {
        return $this->hasMany(HiddenCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenCompanies0()
    {
        return $this->hasMany(HiddenCompany::className(), ['hidden_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenGroups()
    {
        return $this->hasMany(HiddenGroup::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenUsers()
    {
        return $this->hasMany(HiddenUser::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Like::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeCompanies()
    {
        return $this->hasMany(LikeCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeCompanies0()
    {
        return $this->hasMany(LikeCompany::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikeProducts()
    {
        return $this->hasMany(LikeProduct::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMentionCompanies()
    {
        return $this->hasMany(MentionCompany::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingCompany()
    {
        return $this->hasOne(RatingCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReposts()
    {
        return $this->hasMany(Repost::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribersCompanies()
    {
        return $this->hasMany(SubscribersCompany::className(), ['target_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanies()
    {
        return $this->hasMany(UserCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $ucs = $this->getUserCompanies()->all();
        $res = [];
        /** @var UserCompany $uc */
        foreach($ucs as $uc){
            if(($user = $uc->user) instanceof User){
                $res[] = $user;
            }
        }
        return $res;
    }
}
