<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "group".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property integer $is_open
 * @property string $created
 *
 * @property GroupAdministrator[] $groupAdministrators
 * @property GroupCompany[] $groupCompanies
 * @property GroupEvents[] $groupEvents
 * @property GroupImage[] $groupImages
 * @property GroupUser[] $groupUsers
 * @property GroupVideo[] $groupVideos
 * @property HiddenGroup[] $hiddenGroups
 */
class Group extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text'], 'string'],
            [['is_open'], 'integer'],
            [['created'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
            'is_open' => 'Is Open',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupAdministrators()
    {
        return $this->hasMany(GroupAdministrator::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupCompanies()
    {
        return $this->hasMany(GroupCompany::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupEvents()
    {
        return $this->hasMany(GroupEvents::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupImages()
    {
        return $this->hasMany(GroupImage::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupUsers()
    {
        return $this->hasMany(GroupUser::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupVideos()
    {
        return $this->hasMany(GroupVideo::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHiddenGroups()
    {
        return $this->hasMany(HiddenGroup::className(), ['hidden_group_id' => 'id']);
    }
}
