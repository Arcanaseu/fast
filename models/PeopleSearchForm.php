<?php

namespace app\models;


use app\modules\admin\controllers\AdminFriendsController;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * PeopleSearchForm is the model behind the login form.
 */
class PeopleSearchForm extends Model
{
    public $pattern;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['pattern'], 'required'],
        ];
    }

    public function search(){

        $query = $this->getQuery();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

    }

    public function getQuery()
    {
        $parts = explode(' ',$this->pattern);
        $ids = AdminFriendsController::getFriendsId();
        $ids[] = Yii::$app->user->id;


        $query = User::find();
        foreach($parts as $pattern){
            $pattern = trim($pattern);
            $query->orFilterWhere(['like', 'name', $pattern])
                ->orFilterWhere(['like', 'surname', $pattern])
                ->orFilterWhere(['like', 'username', $pattern])
                ->andFilterWhere(['not in', 'id',$ids]);

        }
        return $query;
    }


}
