<?php

namespace app\models;


use Yii;
use yii\base\Model;



/**
 * FriendsRequestForm is the model behind the login form.
 */
class FriendsRequestForm extends Model
{
    public $friendId;
    public $userId;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['friendId','userId'], 'required'],
        ];
    }

    public function send(){
        if(!(Yii::$app->user->id == intval($this->userId))){
            return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
        }
        if($this->validate()){
            $req = Request::find()->where([
                'initiator_id'=> $this->userId,
                'acceptor_id'=>$this->friendId
                ])->one();
            if(!$req){
                $request = new Request();
                $request->initiator_id = $this->userId;
                $request->acceptor_id = $this->friendId;
                $request->type = 'friend request';
                $request->save();
            }
        }
    }



}
