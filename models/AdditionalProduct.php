<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "additional_product".
 *
 * @property string $id
 * @property string $additional_id
 * @property string $product_id
 *
 * @property Product $product
 * @property Additional $additional
 */
class AdditionalProduct extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'additional_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['additional_id', 'product_id'], 'required'],
            [['additional_id', 'product_id'], 'integer'],
            [['additional_id', 'product_id'], 'unique', 'targetAttribute' => ['additional_id', 'product_id'], 'message' => 'The combination of Additional ID and Product ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'additional_id' => 'Additional',
            'product_id' => 'Product',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditional()
    {
        return $this->hasOne(Additional::className(), ['id' => 'additional_id']);
    }
}
