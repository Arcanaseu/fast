<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "kitchen".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 *
 * @property CompanyKitchen[] $companyKitchens
 */
class Kitchen extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitchen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyKitchens()
    {
        return $this->hasMany(CompanyKitchen::className(), ['kitchen_id' => 'id']);
    }
}
