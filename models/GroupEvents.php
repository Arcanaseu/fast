<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "group_events".
 *
 * @property string $id
 * @property string $group_id
 * @property string $events_id
 *
 * @property Events $events
 * @property Group $group
 */
class GroupEvents extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'events_id'], 'required'],
            [['group_id', 'events_id'], 'integer'],
            [['group_id', 'events_id'], 'unique', 'targetAttribute' => ['group_id', 'events_id'], 'message' => 'The combination of Group ID and Events ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group',
            'events_id' => 'Events',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'events_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }
}
