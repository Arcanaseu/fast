<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "image".
 *
 * @property string $id
 * @property string $path
 * @property string $created
 * @property string $title
 * @property string $description
 *
 * @property Company[] $companies
 * @property CompanyImage[] $companyImages
 * @property Events[] $events
 * @property EventsImage[] $eventsImages
 * @property GroupImage[] $groupImages
 * @property MenuImage[] $menuImages
 * @property Product[] $products
 * @property ProductImage[] $productImages
 * @property User[] $users
 * @property UserImage[] $userImages
 */
class Image extends BaseModel
{

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['path', 'created', 'title','description'], 'safe'],
            [['path', 'title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024],
            [['file'], 'file',  'extensions' => 'png,jpg,jpeg,gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'created' => 'Created',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['avatar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyImages()
    {
        return $this->hasMany(CompanyImage::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['avatar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsImages()
    {
        return $this->hasMany(EventsImage::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupImages()
    {
        return $this->hasMany(GroupImage::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuImages()
    {
        return $this->hasMany(MenuImage::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['avatar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['avatar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserImages()
    {
        return $this->hasMany(UserImage::className(), ['image_id' => 'id']);
    }
}
