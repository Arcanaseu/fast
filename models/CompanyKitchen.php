<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_kitchen".
 *
 * @property string $id
 * @property string $company_id
 * @property string $kitchen_id
 *
 * @property Company $company
 * @property Kitchen $kitchen
 */
class CompanyKitchen extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_kitchen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'kitchen_id'], 'required'],
            [['company_id', 'kitchen_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company',
            'kitchen_id' => 'Kitchen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitchen()
    {
        return $this->hasOne(Kitchen::className(), ['id' => 'kitchen_id']);
    }
}
