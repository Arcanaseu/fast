<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "subscribers_company".
 *
 * @property string $id
 * @property string $subscriber_id
 * @property string $target_id
 *
 * @property User $subscriber
 * @property Company $target
 */
class SubscribersCompany extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscribers_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscriber_id', 'target_id'], 'required'],
            [['subscriber_id', 'target_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscriber_id' => 'Subscriber',
            'target_id' => 'Target',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(User::className(), ['id' => 'subscriber_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(Company::className(), ['id' => 'target_id']);
    }
}
