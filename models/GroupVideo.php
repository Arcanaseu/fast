<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "group_video".
 *
 * @property string $id
 * @property string $group_id
 * @property string $video_id
 *
 * @property Video $video
 * @property Group $group
 */
class GroupVideo extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'video_id'], 'required'],
            [['group_id', 'video_id'], 'integer'],
            [['group_id', 'video_id'], 'unique', 'targetAttribute' => ['group_id', 'video_id'], 'message' => 'The combination of Group ID and Video ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group',
            'video_id' => 'Video',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }
}
