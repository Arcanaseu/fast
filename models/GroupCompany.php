<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "group_company".
 *
 * @property string $id
 * @property string $group_id
 * @property string $company_id
 *
 * @property Company $company
 * @property Group $group
 */
class GroupCompany extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'company_id'], 'required'],
            [['group_id', 'company_id'], 'integer'],
            [['group_id', 'company_id'], 'unique', 'targetAttribute' => ['group_id', 'company_id'], 'message' => 'The combination of Group ID and Company ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group',
            'company_id' => 'Company',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }
}
