<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mention_product".
 *
 * @property string $id
 * @property string $creator_id
 * @property string $target_id
 * @property string $created
 * @property integer $rate
 * @property string $text
 *
 * @property Product $target
 * @property User $creator
 * @property RatingMentionProduct[] $ratingMentionProducts
 */
class MentionProduct extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mention_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator_id', 'target_id', 'rate'], 'required'],
            [['creator_id', 'target_id', 'rate'], 'integer'],
            [['created'], 'safe'],
            [['text'], 'string', 'max' => 1024],
            [['creator_id', 'target_id'], 'unique', 'targetAttribute' => ['creator_id', 'target_id'], 'message' => 'The combination of Creator ID and Target ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator_id' => 'Creator',
            'target_id' => 'Target',
            'created' => 'Created',
            'rate' => 'Rate',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(Product::className(), ['id' => 'target_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingMentionProducts()
    {
        return $this->hasMany(RatingMentionProduct::className(), ['target_id' => 'id']);
    }
}
