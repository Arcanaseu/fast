<?php

namespace app\controllers;

use app\models\CompanyImage;
use app\models\EventsImage;
use app\models\GroupImage;
use app\models\Image;
use app\models\MenuImage;
use app\models\ProductImage;
use app\models\UserImage;
use yii\db\ActiveRecord;
use app\modules\admin\AdminHelper;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\JqueryAsset;
use yii\web\UploadedFile;

class SiteController extends Controller
{


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {


        return $this->render('index');
    }




    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionModels()
    {
        return $this->render('models');
    }
}
